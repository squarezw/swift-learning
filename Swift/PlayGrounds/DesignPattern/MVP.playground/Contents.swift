import UIKit

UIImage(named: "MVP2.png")

struct Person { // Model 
    let firstName: String
    let lastName: String
}

protocol GreetingView: class {  // 通过协议暴露接口
    func setGreeting(greeting: String)
}

protocol GreetingViewPresenter { // 通过协议暴露接口
    init(view: GreetingView, person: Person)
    func showGreeting()
}

class GreetingPresenter : GreetingViewPresenter {
    unowned let view: GreetingView
    let person: Person
    required init(view: GreetingView, person: Person) {
        self.view = view
        self.person = person
    }
    func showGreeting() {
        let greeting = "Hello" + " " + self.person.firstName + " " + self.person.lastName
        self.view.setGreeting(greeting: greeting)
    }
}

class GreetingViewController : UIViewController, GreetingView {
    var presenter: GreetingViewPresenter!
    let showGreetingButton = UIButton()
    let greetingLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showGreetingButton.addTarget(self, action: Selector(("didTapButton:")), for: .touchUpInside)
    }
    
    func didTapButton(button: UIButton) {
        self.presenter.showGreeting()   // 向 Presenter 发起业务调用
    }
    
    // 根据协议约定，实现协议方法，有数据返回时回调，并将处理后的结果一并返回
    func setGreeting(greeting: String) {
        self.greetingLabel.text = greeting
    }
}
// Assembling of MVP
let model = Person(firstName: "Jesse", lastName: "Zhao")
let view = GreetingViewController()
let presenter = GreetingPresenter(view: view, person: model)
view.presenter = presenter
