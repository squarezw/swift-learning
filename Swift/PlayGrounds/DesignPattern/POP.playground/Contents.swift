//: Playground - noun: a place where people can play

import UIKit

/**
 Object-Oriented Programming (OOP) 是近年軟體開發的主流設計模式，但在 2015 WWDC，Apple 對這個稱霸數十載的設計模式提出挑戰。
 
 Apple 認為，在 OOP 的架構中，假如 super class 定義過於狹窄，能繼承於此 super class 的 sub class 就會較少；為了讓 super class 較「通用」、能被較多 sub classes 繼承，開發者往往需要在 super class 中加入許多 properties，但這些 properties 卻未必是每個 sub class 都需要的。於是，OOP 為主的專案容易龐雜、且程式碼容易缺乏彈性。
 
 於是，Apple 提出 Protocol Oriented Programming（POP），主張應該用 protocol 當作設計程式架構的基礎，以解決 OOP 容易龐雜、缺乏彈性的缺點。
 */

