//: Playground - noun: a place where people can play

import UIKit


/**
 假定我们要设计一个媒体播放器。该媒体播放器目前只支持音频文件mp3和wav。如果不谈设计，设计出来的播放器可能很简单：
 */

class MessageBox {
    static func show(message: String) {
        
    }
}

class MediaPlayer_ {
    func playMp3() {
        MessageBox.show(message: "Play the mp3 file.")
    }
    
    func playWav() {
        MessageBox.show(message: "Play the wav file.")
    }
    
    func play(audioType: String) {
        switch audioType {
        case "mp3":
            playMp3()
        case "wav":
            playWav()
        default:
            break
        }
    }
}

/**
 仔细分析这段代码，它其实是一种最古老的面向结构的设计。如果你要播放的不仅仅是 mp3和wav,你会不断地增加相应地播放方法，然后让switch子句越来越长，直至达到你视线看不到的地步。
 
 好吧，我们先来体验对象的精神。根据OOP的思想，我们应该把mp3和wav看作是一个独立的对象。那么是这样吗？
 */

class MP3_ {
    func play() {
        MessageBox.show(message: "Play the mp3 file.")
    }
}

class WAV_ {
    func play() {
        MessageBox.show(message: "Play the wav file.")
    }
}

/**
 好样的，你已经知道怎么建立对象了。更可喜的是，你在不知不觉中应用了重构的方法，把原来那个垃圾设计中的方法名字改为了统一的Play()方法。你在后面的设计中，会发现这样改名是多么的关键！但似乎你并没有击中要害，以现在的方式去更改MediaPlayer的代码，实质并没有多大的变化。
 
 既然mp3和wav都属于音频文件，他们都具有音频文件的共性，为什么不为它们建立一个共同的父类呢？
 */

class AudioMedia_ {
    func play() {
        MessageBox.show(message: "Play the AudioMedia file.")
    }
}

/**
 其实在现实生活中，我们播放的只会是某种具体类型的音频文件，因此这个AudioMedia类并没有实际使用的情况。对应在设计中，就是：这个类永远不会被实例化。所以，还得动一下手术，将其改为抽象类。好了，现在的代码有点OOP的感觉了：
 */

protocol AudioMediaProtocol {
    func play()
}

class Mp3__: AudioMediaProtocol {
    func play() {
        MessageBox.show(message: "Play the mp3 file.")
    }
}

class WAV__: AudioMediaProtocol {
    func play() {
        MessageBox.show(message: "Play the wav file.")
    }
}

class MediaPlayer__ {
    func play(media: AudioMedia) {
        media.play()
    }
}

/**
 看看现在的设计，即满足了类之间的层次关系，同时又保证了类的最小化原则，更利于扩展（到这里，你会发现play方法名改得多有必要）。即使你现在又增加了对WMA文件的播放，只需要设计WMA类，并继承AudioMedia，重写Play方法就可以了，MediaPlayer类对象的Play方法根本不用改变。
 
 是不是到此就该画上圆满的句号呢？然后刁钻的客户是永远不会满足的，他们在抱怨这个媒体播放器了。因为他们不想在看足球比赛的时候，只听到主持人的解说，他们更渴望看到足球明星在球场奔跑的英姿。也就是说，他们希望你的媒体播放器能够支持视频文件。你又该痛苦了，因为在更改硬件设计的同时，原来的软件设计结构似乎出了问题。因为视频文件和音频文件有很多不同的地方，你可不能偷懒，让视频文件对象认音频文件作父亲啊。你需要为视频文件设计另外的类对象了，假设我们支持RM和MPEG格式的视频：
 */


protocol VideoMediaProtocol {
    func play()
}

class RM:VideoMediaProtocol
{
    func play() {
        MessageBox.show(message: "Play the rm file.")
    }
}

class MPEG:VideoMediaProtocol {
    func play() {
        MessageBox.show(message: "Play the mpeg file.")
    }
}


/**
 糟糕的是，你不能一劳永逸地享受原有的MediaPlayer类了。因为你要播放的RM文件并不是AudioMedia的子类。
 
 不过不用着急，因为接口这个利器你还没有用上（虽然你也可以用抽象类，但在C#里只支持类的单继承）。虽然视频和音频格式不同，别忘了，他们都是媒体中的一种，很多时候，他们有许多相似的功能，比如播放。根据接口的定义，你完全可以将相同功能的一系列对象实现同一个接口：
 */

protocol IMedia {
    func play()
}

protocol AudioMedia:IMedia {
    
}

protocol VideoMedia:IMedia {
    
}

class MediaPlayer {
    func play(media: IMedia) {
        media.play()
    }
}

class Mp3:AudioMedia
{
    func play() {
        
    }
}

class Wav:AudioMedia
{
    func play() {
        
    }
}

/**
 现在可以总结一下，从MediaPlayer类的演变，我们可以得出这样一个结论：在调用类对象的属性和方法时，尽量避免将具体类对象作为传递参数，而应传递其抽象对象，更好地是传递接口，将实际的调用和具体对象完全剥离开，这样可以提高代码的灵活性。
 
 不过，事情并没有完。虽然一切看起来都很完美了，但我们忽略了这个事实，就是忘记了MediaPlayer的调用者。还记得文章最开始的switch语句吗？看起来我们已经非常漂亮地除掉了这个烦恼。事实上，我在这里玩了一个诡计，将switch语句延后了。虽然在MediaPlayer中，代码显得干净利落，其实烦恼只不过是转嫁到了MediaPlayer的调用者那里。例如，在主程序界面中：
 */

func btnPlay_(type: String) {
    var media: IMedia
    
    switch type {
    case "mp3":
        media = Mp3()
    case "wav":
        media = Wav()
    default:
        media = Mp3()
    }
    
    let player = MediaPlayer()
    player.play(media: media)
}

/**
 用户通过选择cbbMediaType组合框的选项，决定播放哪一种文件，然后单击Play按钮执行。
 
 现在该设计模式粉墨登场了，这种根据不同情况创建不同类型的方式，工厂模式是最拿手的。先看看我们的工厂需要生产哪些产品呢？虽然这里有两种不同类型的媒体 AudioMedia和VideoMedia（以后可能更多），但它们同时又都实现IMedia接口，所以我们可以将其视为一种产品，用工厂方法模式就可以了。首先是工厂接口：
 */

protocol IMediaFactory {
    func createMedia() -> IMedia
}

/**
 然后为每种媒体文件对象搭建一个工厂，并统一实现工厂接口：
 */

class MP3MediaFactory:IMediaFactory {
    func createMedia() -> IMedia {
        return Mp3()
    }
}

class WAVMediaFactory:IMediaFactory {
    func createMedia() -> IMedia {
        return Wav()
    }
}

/**
 //其它工厂略；
 
 写到这里，也许有人会问，为什么不直接给AudioMedia和VideoMedia类搭建工厂呢？很简单，因为在AudioMedia和 VideoMedia中，分别还有不同的类型派生，如果为它们搭建工厂，则在CreateMedia()方法中，仍然要使用Switch语句。而且既然这两个类都实现了IMedia接口，可以认为是一种类型，为什么还要那么麻烦去请动抽象工厂模式，来生成两类产品呢？
 
 可能还会有人问，即使你使用这种方式，那么在判断具体创建哪个工厂的时候，不是也要用到switch语句吗？我承认这种看法是对的。不过使用工厂模式，其直接好处并非是要解决 switch语句的难题，而是要延迟对象的生成，以保证的代码的灵活性。当然，我还有最后一招杀手锏没有使出来，到后面你会发现，switch语句其实会完全消失。
 
 还有一个问题，就是真的有必要实现AudioMedia和VideoMedia两个抽象类吗？让其子类直接实现接口不更简单？对于本文提到的需求，我想你是对的，但不排除AudioMedia和VideoMedia它们还会存在区别。例如音频文件只需要提供给声卡的接口，而视频文件还需要提供给显卡的接口。如果让MP3、WAV、RM、MPEG直接实现IMedia接口，而不通过AudioMedia和VideoMedia，在满足其它需求的设计上也是不合理的。当然这已经不包括在本文的范畴了。
 
 现在主程序界面发生了稍许的改变：
 */

func btnPlay__(string: String) {
    let factory: IMediaFactory
    
    switch string {
    case "mp3":
        factory = MP3MediaFactory()
    case "wav":
        factory = WAVMediaFactory()
    default:
        factory = MP3MediaFactory()
    }
    
    let player = MediaPlayer()
    player.play(media: factory.createMedia())
    
}

/**
 写到这里，我们再回过头来看MediaPlayer类。这个类中，实现了Play方法，并根据传递的参数，调用相应媒体文件的Play方法。在没有工厂对象的时候，看起来这个类对象运行得很好。如果是作为一个类库或组件设计者来看，他提供了这样一个接口，供主界面程序员调用。然而在引入工厂模式后，在里面使用MediaPlayer类已经多余了。所以，我们要记住的是，重构并不仅仅是往原来的代码添加新的内容。当我们发现一些不必要的设计时，还需要果断地删掉这些冗余代码。
 */

func btnPlay(string: String) {
    let factory: IMediaFactory
    
    switch string {
    case "mp3":
        factory = MP3MediaFactory()
    case "wav":
        factory = WAVMediaFactory()
    default:
        factory = MP3MediaFactory()
    }
    
    let media = factory.createMedia()
    media.play()
}

/**
 如果你在最开始没有体会到IMedia接口的好处，在这里你应该已经明白了。我们在工厂中用到了该接口；而在主程序中，仍然要使用该接口。使用接口有什么好处？那就是你的主程序可以在没有具体业务类的时候，同样可以编译通过。因此，即使你增加了新的业务，你的主程序是不用改动的。
 
 不过，现在看起来，这个不用改动主程序的理想，依然没有完成。看到了吗？在BtnPlay_Click()中，依然用new创建了一些具体类的实例。如果没有完全和具体类分开，一旦更改了具体类的业务，例如增加了新的工厂类，仍然需要改变主程序，何况讨厌的switch语句仍然存在，它好像是翅膀上滋生的毒瘤，提示我们，虽然翅膀已经从僵冷的世界里复活，但这双翅膀还是有病的，并不能正常地飞翔。
 
 是使用配置文件的时候了。我们可以把每种媒体文件类类型的相应信息放在配置文件中，然后根据配置文件来选择创建具体的对象。并且，这种创建对象的方法将使用反射来完成。首先，创建配置文件
 
 然后，在主程序界面的Form_Load事件中，读取配置文件的所有key值，填充cbbMediaType组合框控件：
 */

func formLoad(config: [String: String]) {
//    mediaType.items.clear()
//    for key in config.allkeys {
//        mediaType.item.add(key)
//    }
}

/**
 最后，更改主程序的Play按钮单击事件：
 */
func btnPlayClick(string: String) {
//    string mediaType = cbbMediaType.SelectItem.ToString().ToLower();
//    string factoryDllName = ConfigurationSettings.AppSettings[mediaType].ToString();
//    IMediaFactory factory = (IMediaFactory)Activator.CreateInstance("MediaLibrary",factoryDllName).Unwrap ();//MediaLibray为引用的媒体文件及工厂的程序集;
//    IMedia media = factory.CreateMedia();
//    media.Play();
}

/**
 现在鸟儿的翅膀不仅仅复活，有了可以飞的能力；同时我们还赋予这双翅膀更强的功能，它可以飞得更高，飞得更远！
 
 享受自由飞翔的惬意吧。设想一下，如果我们要增加某种媒体文件的播放功能，如AVI文件。那么，我们只需要在原来的业务程序集中创建AVI类，并实现 IMedia接口，同时继承VideoMedia类。另外在工厂业务中创建AVIMediaFactory类，并实现IMediaFactory接口。假设这个新的工厂类型为WingProject.AVIFactory，则在配置文件中添加如下一行：
 */
