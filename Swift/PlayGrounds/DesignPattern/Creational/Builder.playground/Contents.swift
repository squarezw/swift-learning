//: Playground - noun: a place where people can play

import UIKit

UIImage(named: "structure-builder")

/**
 生成器 - Builder
 
 将一个复杂对象的构建与它的表现分离，使得同样的构建过程可以创建不同的表现。
 
 生成器可以将构建对象的过程分为，客户 - 指导者 - 生成器 的关系
 
 characterBuilder 就是生成器
 game 就是指导者
 
 指导者里声明了创建不同表现的对象的方法。
 而方法里由生成器 characterBuilder 来构建不同的 Character 类型的对象。
 */
class CBuilder {
    let character: Character
    
    init(c: Character) {
        self.character = c
    }
}

class StandarCharacterBuilder: CBuilder {
    
    func buildCharacter() {
        buildProtection(value: 1)
    }
    
    func buildProtection(value: Float) -> Character {
        self.character.protection *= value
        return self.character
    }
}

class Character {
    var protection: Float = 1
}

class ChasingGame {
    func createPlayer(builder: StandarCharacterBuilder) -> Character {
        builder.buildCharacter()
        builder.buildProtection(value: 10)
        return builder.character
    }
    func createEnemy(builder: StandarCharacterBuilder) -> Character {
        builder.buildCharacter()
        builder.buildProtection(value: 100)
        return builder.character
    }
}

let character = Character()
let characterBuilder = StandarCharacterBuilder(c: character)
let chasingGame = ChasingGame()

let player = chasingGame.createPlayer(builder: characterBuilder)
let enemy = chasingGame.createEnemy(builder: characterBuilder)



/**
 Example 2
 */

// Builder can create different products using the same building process.

/// Can have GPS, trip computer and various numbers of seats.
/// Can be a city car, a sports car, or a cabriolet.
class Car {}

/// Textual users's manual that corresponds to a particular car configuration.
class Manual {}

/// Builder interface defines all possible ways to configure a product.
protocol Builder {
    func reset()
    func setSeats(_: Int)
    func setEngine(_: Engine)
    func setTripComputer(_: Bool)
    func setGPS(_: Bool)
}

class CarBuilder: Builder {
    private let car: Car = Car()
    
    func reset() {
        // Put a new Car instance into the "car" field.
    }
    
    func setSeats(_: Int) {
        // Set the number of seats in car.
    }
    
    func setEngine(_: Engine) {
        // Install a given engine.
    }
    
    func setTripComputer(_: Bool) {
        // Install a trip computer.
    }
    
    func setGPS(_: Bool) {
        // Install a global positioning system.
    }
    
    func getResult() -> Car {
        return car
    }
}

class CarManualBuilder: Builder {
    private let manual: Manual = Manual()
    
    func reset() {
        // Put a new Manual instance into the "manual" field.
    }
    
    func setSeats(_: Int) {
        // Document car seats features.
    }
    
    func setEngine(_: Engine) {
        // Add an engine instruction.
    }
    
    func setTripComputer(_: Bool) {
        // Add a trip computer instruction.
    }
    
    func setGPS(_: Bool) {
        // Add GPS instruction.
    }
    
    func getResult() -> Manual {
        return manual
    }
}

protocol Engine {}
class SportEngine: Engine {}


/// Director defines the order of building steps. It works with a builder object through
/// the common builder interface. Therefore it may not know what product is being built.
class Director {
    func constructSportsCar(builder: Builder) {
        builder.reset()
        builder.setSeats(2)
        builder.setEngine(SportEngine())
        builder.setTripComputer(true)
        builder.setGPS(true)
    }
}

class Application {
    func makeCar() {
        let director = Director()
        
        let carBuilder = CarBuilder()
        director.constructSportsCar(builder: carBuilder)
        let _ = carBuilder.getResult()
        
        let carManualBuilder = CarManualBuilder()
        director.constructSportsCar(builder: carManualBuilder)
        // The final product is often retrieved from a builder
        // object, since Director is not aware and not dependent
        // on concrete builders and products.
        let _ = carManualBuilder.getResult()
        
        // Remember, the Builder pattern can be used without a Director class.
        // the client code uses the builder object directly.
        let builder = CarBuilder()
        builder.setGPS(true)
        builder.getResult()
    }
}


/**
 if some builder is very similar to other builder, we could extend it from the common builder, while overriding some of the building steps
 */
class SuperCarBuilder: CarBuilder {
    override func setGPS(_: Bool) {
        // custom codes
    }
}


/**
 Sample:

 Sequence Builder
 */
protocol SQLQueryBuilder {
    func select(table: String) -> SQLQueryBuilder
    func `where`(field: String) -> SQLQueryBuilder
    func limit(start: Int, offset: Int) -> SQLQueryBuilder
    func getSQL() -> String
}

class ConSql: SQLQueryBuilder {
    private var query: String = ""
    
    func select(table: String) -> SQLQueryBuilder {
        query += table
        return self
    }
    func `where`(field: String) -> SQLQueryBuilder {
        query += field
        return self
    }
    func limit(start: Int, offset: Int) -> SQLQueryBuilder {
        query += "\(start) \(offset)"
        return self
    }
    func getSQL() -> String {
        return query
    }
}

let sql = ConSql().select(table: "aha").where(field: "name").limit(start: 1, offset: 2).getSQL()

print(sql)

