//: Playground - noun: a place where people can play

import UIKit

class TestDisplay {
    var name: String?
    let font: String
    
    init(font: String) {
        self.font = font
    }
    
    func clone() -> TestDisplay {
        return TestDisplay(font: self.font)
    }
}

let prototype = TestDisplay(font: "GotanProject")

let phi = prototype.clone()
phi.name = "Phi"

let chr = prototype.clone()
chr.name = "Chr"



