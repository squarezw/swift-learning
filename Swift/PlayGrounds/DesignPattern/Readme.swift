/**
 实战 Swift 设计模式
 https://wildenchen.gitbooks.io/swift-design-patterns/content/index.html

 https://github.com/ochococo/Design-Patterns-In-Swift (3.0)
 https://github.com/kingreza/Design-Patterns-In-Swift (2.0)
 
 系统设计的几个特性需要考虑
 
 Scalability
 
 
 
 MVC, MVVM, MVP, VIPER 结构解析
 https://techblog.badoo.com/blog/2016/03/21/ios-architecture-patterns/
 
 Comparing MVVM and Viper architectures: When to use one or the other
 https://auth0.com/blog/compare-mvvm-and-viper-architectures/
 
 自动生成 VIPER 结构项目
 https://github.com/rambler-digital-solutions/Generamba
 
 VIPER 详情介绍
 https://objccn.io/issue-13-5/
 */

/*
 面向对象程序设计(Object Oriented Programming)
 
 Object Oriented Programming is a lot like creating virtual models of a real element, in fact we often do just that. So, it stands to reason that Object Oriented Design might sound like it’s got a place in the physical world.
 
 protocol-oriented-programming
 https://www.raywenderlich.com/148448/introducing-protocol-oriented-programming
 
 OOP和设计模式原则简介
 1.封装
 2.继承
 3.多态

 */

/**
 https://www.jianshu.com/p/dc5052da82fa
 
 ----------------
 常用设计模式
 ----------------
 工厂方法 - Factory Method
 
 抽象工厂 - Abstract Factory
 
 生成器（Builder）
 
 装饰者模式 - Decorator
 Extension 和 Delegation 即是使用的这种
 
 适配器模式 - Adapter
 NSCopying
 
 观察者模式 - Observer
 Notification , KVO
 
 备忘录模式 - Memento
 Archiving
 
 桥接（Bridge）
 
 -------
 对象去耦
 -------
 中介者（Mediator）
 UINavigationController
 
 -------
 抽象集合
 -------
 组合（Composite）
 UIView
 
 迭代器（Iterator）
 NSEnumerator
 
 -------
 行为扩展
 -------
 访问者（Visitor）
 
 
 责任链（Chain of Responsibility）
 hit-test view
 
 -------
 算法封装
 -------
 模板方法（Template Method）
 drawRect
 - (void)drawRect:(CGRect)rect 就是模板方法，默认什么都不做或者只是做了部分操作，缺少特性操作，用来给子类选择重载与实现的方法。
 
 策略（Strategy）
 定义一系列算法，把它们一个个封装起来，并且使它们可相互替换。本模式使得算法可独立于使用它的客户而变化。
 举一个常见的例子，验证 UITextField 输入是否有效。有两个算法分别是验证邮箱的和验证电话号码的。可以通过 if else 这样的判断代码来决定执行哪个算法。也可以通过策略模式，将算法封装起来，如下图
 
 
 命令（Command）
 将请求封装为一个对象，从而可用不同的请求对客户进行参数化，对请求排队或记录请求日志，以及支持可撤销的操作。
 NSInvocation
 
 
 性能与对象访问
 
 享元（Flyweight）
 利用共享技术有效地支持大量细粒度的对象
 tableViewCell
 
 代理（Proxy）
 为其它对象提供一种代理以控制对这个对象的访问。
 代理设计模式的英文名是 Proxy pattern，和我们常见的 delegate（委托） 没关系。
 
 iOS 中常见的代理模式例子为引用计数，当一个复杂对象的多份副本须存在时，代理模式可以结合享元模式以减少内存用量。典型做法是创建一个复杂对象及多个代理者，每个代理者会引用到原本的复杂对象。而作用在代理者的运算会转送到原本对象。一旦所有的代理者都不存在时，复杂对象会被移除。
 
 http://www.jianshu.com/p/6b302c7fe987
 */
