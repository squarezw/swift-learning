//: Playground - noun: a place where people can play

import UIKit

UIImage(named: "structure-indexed-2x")


// The common interface for all components.
protocol DataSource {
    func writeData(data: String)
    func readData() -> String
}


class FileDataSource: DataSource {
    private let filename: String
    private var data: String?
    
    init(_ filename: String) {
        self.filename = filename
    }
    func writeData(data: String) {
        self.data = self.filename
        print("write data: \(data)")
    }
    func readData() -> String {
        return data ?? ""
    }
}

// The base decorator contains wrapping behavior
class DataSourceDecorator: DataSource {
    fileprivate let wrapee: DataSource
    
    init(_ source: DataSource) {
        self.wrapee = source
    }
    func writeData(data: String) {
        wrapee.writeData(data: data)
    }
    
    func readData() -> String {
        return wrapee.readData()
    }
}

class EncryptionDecorator: DataSourceDecorator {
    override func writeData(data: String) {
        // 1. Encrypt passed data
        // 2. Pass encrypted data to wrapee's writeData method.
        wrapee.writeData(data: "encrypted: \(data)")
    }
    
    override func readData() -> String {
        // 1. Get data from wrapee's readData method.
        // 2. Try to decrypt it if it's encrypted.
        // 3. Return the result.
        return ""
    }
}

class CompressionDecorator: DataSourceDecorator {
    override func writeData(data: String) {
        // 1. Compress passed data
        // 2. Pass compressed data to wrappee's writeData method.
        wrapee.writeData(data: "compressed: \(data)")
    }
    
    override func readData() -> String {
        // 1. Get data from wrappee's readData method.
        // 2. Try to decompress it if it's compressed.
        // 3. Return the result.
        return ""
    }
}

class Application {
    func dumbUsageExample() {
        var source: DataSource = FileDataSource("somefile.dat")
        source.writeData(data: "Hello World!")
        // The target file has been written with plain data.
        
        source = CompressionDecorator(source)
        
        source = EncryptionDecorator(source)
        source.writeData(data: "Hello World!")
        
    }
}

class TestDataSource: DataSource {
    func readData() -> String {
        return ""
    }
    
    func writeData(data: String) {
        print(data)
    }
}

let app = Application()
app.dumbUsageExample()


// Option 2. The client code that uses an external data source.
// SalaryManager objects neither know nor care about data
// storage specifics. They work with pre-configured data source,
// received from the app configurator.

class SalaryManager {
    let source: DataSource
    
    init(source: DataSource) {
        self.source = source
    }
    
    func load() {
        source.readData()
    }
    
    func save() {
        source.writeData(data: "Hello")
    }
    
     // ...Other useful methods...
}

// The app is able to assemble different stacks of decorators at
// run time, depending on the configuration or environment.
class ApplicationConfigurator {
    let enabledEncryption = true
    let enabledCompression = true
    
    func configurationExample() {
        var source: DataSource = FileDataSource("salary.dat")
        
        if (enabledEncryption) {
            source = EncryptionDecorator(source)
        }
        if (enabledCompression) {
            source = CompressionDecorator(source)
        }
        
        let logger = SalaryManager(source: source)
        logger.load()
    }
}

/// Sample 3

protocol Coffee {
    func getCost() -> Double
    func getIngredients() -> String
}

class SimpleCoffee: Coffee {
    func getCost() -> Double {
        return 1.0
    }
    func getIngredients() -> String {
        return "Coffee"
    }
}

class CoffeeDecorator: Coffee {
    private let decoratedCoffee: Coffee
    fileprivate let ingredientSeparator: String = ","
    
    required init(decoratedCoffee: Coffee) {
        self.decoratedCoffee = decoratedCoffee
    }
    
    func getCost() -> Double {
        return decoratedCoffee.getCost()
    }
    
    func getIngredients() -> String {
        return decoratedCoffee.getIngredients()
    }
}

final class Milk: CoffeeDecorator {
    required init(decoratedCoffee: Coffee) {
        super.init(decoratedCoffee: decoratedCoffee)
    }
    
    override func getCost() -> Double {
        return super.getCost() + 0.5
    }
    
    override func getIngredients() -> String {
        return super.getIngredients() + ingredientSeparator + "Milk"
    }
}

final class WhipCoffee: CoffeeDecorator {
    required init(decoratedCoffee: Coffee) {
        super.init(decoratedCoffee: decoratedCoffee)
    }
    
    override func getCost() -> Double {
        return super.getCost() + 0.7
    }
    
    override func getIngredients() -> String {
        return super.getIngredients() + ingredientSeparator + "Whip"
    }
}

var someCoffee: Coffee = SimpleCoffee()
print("Cost: \(someCoffee.getCost()); Ingredients: \(someCoffee.getIngredients())")
someCoffee = Milk(decoratedCoffee: someCoffee)
print("Cost: \(someCoffee.getCost()); Ingredients: \(someCoffee.getIngredients())")
someCoffee = WhipCoffee(decoratedCoffee: someCoffee)
print("Cost: \(someCoffee.getCost()); Ingredients: \(someCoffee.getIngredients())")
