//: Playground - noun: a place where people can play

import UIKit

// Client Interface
protocol OldTarget {
    var x: Int {get}
    var y: Int {get}
}

// Adaptee
class Service {
    var x: String
    var y: String
    
    init(x: String, y: String) {
        self.x = x
        self.y = y
    }
}

// Adapter
class OldTargetAdapter: OldTarget {
    var adaptee: Service
    
    init(adaptee: Service) {
        self.adaptee = adaptee
    }
    
    var x: Int {
        return Int(self.adaptee.x)!
    }
    
    var y: Int {
        return Int(self.adaptee.y)!
    }
}

let adaptee = Service(x: "10", y: "20")
let client = OldTargetAdapter(adaptee: adaptee)
client.x
client.y


/// Class Adapter

protocol Jumping {
    func jmp()
}

class Dog: Jumping {
    func jmp() {
        print("Jumps Excitedly")
    }
}

class Cat: Jumping {
    func jmp() {
        print("Pounce")
    }
}

class Frog {
    func leap() {
        print("Leaps")
    }
}

extension Frog : Jumping {
    func jmp() {
        leap()
    }
}

Frog().jmp()

/// Sample 2

protocol OlderDeathStarSuperLaserAiming {
    var angleV: NSNumber {get}
    var angleH: NSNumber {get}
}

// Adaptee
struct DeathStarSuperlaserTarget {
    let angleHorizontal: Double
    let angleVertical: Double
    
    init(angleHorizontal:Double, angleVertical:Double) {
        self.angleHorizontal = angleHorizontal
        self.angleVertical = angleVertical
    }
}

// Adapter
struct OldDeathStarSuperlaserTarget : OlderDeathStarSuperLaserAiming {
    private let target : DeathStarSuperlaserTarget
    
    var angleV:NSNumber {
        return NSNumber(value: target.angleVertical)
    }
    
    var angleH:NSNumber {
        return NSNumber(value: target.angleHorizontal)
    }
    
    init(_ target:DeathStarSuperlaserTarget) {
        self.target = target
    }
}

let target = DeathStarSuperlaserTarget(angleHorizontal: 14.0, angleVertical: 12.0)
let oldFormat = OldDeathStarSuperlaserTarget(target)

oldFormat.angleH
oldFormat.angleV
