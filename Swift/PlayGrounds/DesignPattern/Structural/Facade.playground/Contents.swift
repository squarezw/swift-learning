//: Playground - noun: a place where people can play

import UIKit
import Foundation

UIImage(named: "structure-facade")

// The facade pattern is used to define a simplified interface to a more complex subsystem.

// Some classes of a complex 3rd-party video conversion
// framework. We do not control that code, therefore can't
// simplify it.

class VideoFile {
    init(_ filename: String) {
        
    }
}
// ...

protocol Codec {}

class OggCompressionCodec: Codec {}
// ...

class MPEG4CompressionCodec: Codec {}
// ...

class CodecFactory {
    static func extract(_ file: VideoFile) -> Codec {
        return OggCompressionCodec()
    }
}
// ...

class BitrateReader {
    static func read(filename: String, sourceCodec: Codec) -> BitrateReader {
        return BitrateReader()
    }
    
    static func convert(buffer: BitrateReader, distinationCodec: Codec) -> String {
        return "Test"
    }
}
// ...

class AudioMixer {
    func fix(_ result: String) -> String { return "Result" }
}
// ...

// To defeat the complexity, we create a Facade class, which
// hides all of the framework's complexity behind a simple
// interface. It is a trade-off between functionality and
// simplicity.
class VideoConverter {
    func convert(filename: String, format: String) -> String {
        let file = VideoFile(filename)
        let sourceCodec = CodecFactory.extract(file)
        
        var distinationCodec: Codec
        
        if format == "mp4" {
            distinationCodec = MPEG4CompressionCodec()
        } else {
            distinationCodec = OggCompressionCodec()
        }
        
        let buffer = BitrateReader.read(filename: filename, sourceCodec: sourceCodec)
        var result = BitrateReader.convert(buffer: buffer, distinationCodec: distinationCodec)
        result = AudioMixer().fix(result)
        
        return result
    }
}

// Application classes do not depend on a billion classes
// provided by the complex framework. Also, if you happen to
// decide to switch framework, you will only need to rewrite the
// facade class.
class Application {
    func main() {
        let convertor = VideoConverter()
        convertor.convert(filename: "youtubevideo.ogg", format: "mp4")
    }
}


// Sample 2

enum Eternal {
    
    static func set(_ object: Any, forKey defaultName: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(object, forKey:defaultName)
        defaults.synchronize()
    }
    
    static func object(forKey key: String) -> Any {
        let defaults: UserDefaults = UserDefaults.standard
        return defaults.object(forKey: key) as Any
    }
    
}

Eternal.set("Disconnect me. I’d rather be nothing", forKey:"Bishop")
Eternal.object(forKey: "Bishop")
