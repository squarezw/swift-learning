//: Playground - noun: a place where people can play

import UIKit
import Foundation

UIImage(named: "structure-flyweight")

// This Flyweight class contains a portion of a state of a tree.
// These field store values that hardly unique for each
// particular tree. For instance, you will not find here the
// tree coordinates. But the texture and colors shared between
// many trees are here. Since this data is usually BIG, you
// would waste a lot of memory by keeping it in the each tree
// object. Instead, we can extract texture, color and other
// repeating data into a separate object, which can be
// referenced from lots of individual tree objects.

class TreeType {
    let name: String
    let color: String
    let texture: String
    
    init(name: String, color: String, texture: String) {
        self.name = name
        self.color = color
        self.texture = texture
    }
    
    func draw(canvas: Any, x: Int, y: Int) {
        print("Draw the bitmap on the \(canvas) at x:\(x) y:\(y)!")
        // 1. Create a bitmap of a given type, color and
        // texture.
        // 2. Draw the bitmap on the canvas at X and Y coord.
    }
}

// Flyweight factory decides whether to re-use existing
// flyweight or to create a new object.
class TreeFactory {
    static var treeTypes: [TreeType] = []
    static func getTreeType(name: String, color: String, texture: String) -> TreeType {
        let tree = treeTypes.find(name: name, color: color, texture: texture)
        
        if let type = tree.first {
            print("The tree is already existing: \(type.name)!")
            return type
        } else {
            let type = TreeType(name: name, color: color, texture: texture)
            treeTypes.append(type)
            print("Add a tree: \(type.name)!")
            return type
        }
    }
}

extension Collection where Element: TreeType {
    func find(name: String, color: String, texture: String) -> [Element] {
        return self.filter { $0.name == name && $0.color == color && $0.texture == texture }
    }
}


// Contextual object contains extrinsic part of tree state.
// Application can create billions of these since they are
// pretty small: just two integer coordinates and one reference
// field.
class Tree {
    let x,y:Int
    let type: TreeType
    init(x: Int, y:Int, type: TreeType) {
        self.x = x
        self.y = y
        self.type = type
    }
    
    func draw(canvas: Any) {
        type.draw(canvas: canvas, x: x, y: y)
    }
}


// The Tree and the Forest classes are the Flyweight's clients.
// You can merge them if you do not plan to develop the Tree
// class any further.
class Forest {
    var trees: [Tree] = []
    
    func plantTree(x: Int, y:Int, name: String, color: String, texture: String) {
        let type = TreeFactory.getTreeType(name: name, color: color, texture: texture)
        let tree = Tree(x: x, y: y, type: type)
        trees.append(tree)
    }
    
    func draw(canvans: Any) {
        for tree in trees {
            tree.draw(canvas: canvans)
        }
    }
}

let forest = Forest()

forest.plantTree(x: 10, y: 10, name: "1", color: "2", texture: "33")
forest.plantTree(x: 10, y: 20, name: "1", color: "2", texture: "33")
forest.plantTree(x: 10, y: 30, name: "Two", color: "Gray", texture: "Texture")

forest.draw(canvans: "A canvans")
