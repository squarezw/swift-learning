//: Playground - noun: a place where people can play

import UIKit
import Foundation

UIImage(named: "structure-proxy")

// The interface of a remote service.
protocol ThirdPartyYoutubeLib {
    func listVideos() -> Data
    func getVideoInfo(id: Int) -> Data
    func downloadVideo(id: Int)
}

// Concrete implementation of a service connector. Methods of
// this class can request various info from youtube. Request
// speed depends on a user's internet connection as wells as
// Youtube's. Application will slow down if a lot of requests
// will be fired at the same time, even if they are requesting
// the same info.
class ThirdPartyYoutubeClass: ThirdPartyYoutubeLib {
    func listVideos() -> Data {
        print("list videos...")
        return Data()
        // Send API request to Youtube.
    }
    
    func getVideoInfo(id: Int) -> Data {
        print("get video...")
        return Data()
        // Get a meta information about some video.
    }
    
    func downloadVideo(id: Int) {
        // Download video file from Youtube.
    }
}

// On the other hand, to save some bandwidth, we can cache
// request results and keep them for some time. But it may be
// impossible to put such code directly to the service class.
// For example, it could have been provided by third party
// library or/and defined as `final`. That is why we put the
// caching code to a new proxy class which implements the same
// interface as a service class. It is going to delegate to the
// service object only when the real requests have to be sent.
class CachedYoutubeClass: ThirdPartyYoutubeLib {
    let service: ThirdPartyYoutubeClass
    var listCache: Data?
    var videoCache: Data?
    let needReset: Bool = false
    
    init(service: ThirdPartyYoutubeClass) {
        self.service = service
    }
    
    func listVideos() -> Data {
        print("from proxy: ")
        if listCache == nil || needReset {
            listCache = service.listVideos()
        }
        return listCache!
    }
    
    func getVideoInfo(id: Int) -> Data {
        if videoCache == nil || needReset {
            videoCache = service.getVideoInfo(id: id)
        }
        return videoCache!
    }
    
    func downloadVideo(id: Int) {
        if !downloadExists(id) || needReset {
            service.downloadVideo(id: id)
        }
    }
    
    func downloadExists(_ id: Int) -> Bool {
        // determine return status of existing download data
        return true
    }
}


// the GUI
class YoutubeManager {
    let service: ThirdPartyYoutubeLib
    
    init(service: ThirdPartyYoutubeLib) {
        self.service = service
    }
    
    func renderVideoPage(id: Int) {
        let info = service.getVideoInfo(id: id)
        print(info)
        // Render the video page.
    }
    
    func renderListPanel() {
        let list = service.listVideos()
        print(list)
        // Render the list of video thumbnails.
    }
    
    func reactOnUserInput() {
        renderVideoPage(id: Int.random(in: 0 ... 10))
        renderListPanel()
    }
}

let service = ThirdPartyYoutubeClass()
let proxy = CachedYoutubeClass(service: service)

let manager = YoutubeManager(service: proxy)
manager.renderListPanel()
manager.reactOnUserInput()


// Sample 2: Protected

protocol DoorOperator {
    func open(door: String) -> String
}

class HAL9000: DoorOperator {
    func open(door: String) -> String {
        return ("HAL9000: Affirmative, Dave. I read you. Opened \(door).")
    }
}

class CurrentComputer: DoorOperator {
    var computer: DoorOperator? = nil
    
    func authenticate(password: String) -> Bool {
        guard password == "Password" else {
            return false
        }
        
        computer = HAL9000()
        return true
    }
    
    func open(door: String) -> String {
        guard computer != nil else {
            return "Access Denied. I'm afraid I can't do that."
        }
        
        return computer!.open(door: door)
    }
}



let computer = CurrentComputer()
computer.open(door: "A door")
computer.authenticate(password: "Password")
computer.open(door: "A door")

