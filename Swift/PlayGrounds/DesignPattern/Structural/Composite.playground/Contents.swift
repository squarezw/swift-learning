//: Playground - noun: a place where people can play

import UIKit
import Foundation

UIImage(named: "structure-composite")

// Common interface for all components.
protocol Graphic {
    func move(x: Int, y: Int)
    func draw()
}

// Simple component.
class Dot: Graphic {
    let x,y: Int
    
    init(x:Int, y:Int) {
        self.x = x
        self.y = y
    }
    
    func move(x: Int, y: Int) {
        
    }
    
    func draw() {
        
    }
}

// Components can extend other components.
class Circle: Dot {
    let radius: Int
    
    init(x: Int, y: Int, radius: Int) {
        self.radius = radius
        super.init(x: x, y: y)
    }
    
    override func draw() {
        // Draw a circle at X and Y and radius R.
    }
}


// The composite component includes methods to add/remove child
// components. It attempts to delegate all operations defined in
// the component interface to its child components.
class CompoundGraphic: Graphic {
    let children: [Graphic]? = nil
    
    func add(child: Graphic) {
        // Add a child to the array of children.
    }
    
    func remove(child: Graphic) {
        // Remove a child from the array of children.
    }
    
    func move(x: Int, y: Int) {
        if let children = self.children {
            for child in children {
                child.move(x: x, y: y)
            }
        }
    }
    
    func draw() {
        // 1. For each child component:
        //     - Draw the component.
        //     - Update the bounding rectangle.
        // 2. Draw the dashed rectangle using bounding coords.
    }
}


// Application can operate uniformly with a specific components
// or with a whole group of components.
class ImageEditor {
    func load() {
        let all = CompoundGraphic()
        all.move(x: 30, y: 20)
    }
}
