//: Playground - noun: a place where people can play

import UIKit

///     Client
///       |
///       |           Bridge
///                              <<Interface>>
///      GUI           <->      Implementation   (API/Service layer)
///
/// Refined Abstractions
///

UIImage(named: "structure-en-indexed-2x")

UIImage(named: "example-en-2x")

/// All devices follow the common interface. It makes them
/// compatible with all types of remotes.
protocol Device {
    var volume: Int {get set}
    var isEnabled: Bool {get}
    func enable()
    func disable()
}

/// Remotes contain references to the device they control.
/// Remotes delegate most of the work to their device objects.
class Remote {
    var device: Device
    
    func togglePower() {
        if device.isEnabled {
            device.disable()
        } else {
            device.enable()
        }
        print(device.isEnabled)
    }
    func volumeDown() {
        device.volume -= 10
        print(device.volume)
    }
    func volumeUp() {
        device.volume += 10
        print(device.volume)
    }
    
    init(device: Device) {
        self.device = device
    }
}

// You can extend remotes' class hierarchy independently from devices' classes.
class AdvancedRemote: Remote {
    func mute() {
        device.volume = 0
        print(device.volume)
    }
}

class Tv: Device {
    var volume: Int = 0
    var isEnabled: Bool = false
    
    func enable() {
        isEnabled = true
    }
    
    func disable() {
        isEnabled = false
    }
}

class Radio: Device {
    var volume: Int = 0 {
        didSet {
            volume += 10
        }
    }
    var isEnabled: Bool = false
    
    func enable() {
        isEnabled = true
    }
    
    func disable() {
        isEnabled = false
    }
}

let tv = Tv()
let remote = Remote(device: tv)
remote.togglePower()
remote.togglePower()
remote.volumeUp()
remote.volumeDown()

let advRemote = AdvancedRemote(device: tv)
advRemote.mute()

let newRemote = Remote(device: Radio())
newRemote.volumeUp()



// ---------- Sample 2 -----------

protocol Switch {
    var appliance: Appliance {get set}
    func turnOn()
}

protocol Appliance {
    func run()
}

class RemoteControl: Switch {
    var appliance: Appliance
    
    func turnOn() {
        self.appliance.run()
    }
    
    init(appliance: Appliance) {
        self.appliance = appliance
    }
}

class TV: Appliance {
    func run() {
        print("tv turned on");
    }
}

class VacuumCleaner: Appliance {
    func run() {
        print("vacuum cleaner turned on")
    }
}

var tvRemoteControl = RemoteControl(appliance: TV())
tvRemoteControl.turnOn()

var fancyVacuumCleanerRemoteControl = RemoteControl(appliance: VacuumCleaner())
fancyVacuumCleanerRemoteControl.turnOn()
