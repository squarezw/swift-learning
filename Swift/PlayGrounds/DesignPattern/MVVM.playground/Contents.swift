//: Playground - noun: a place where people can play

import UIKit

UIImage(named: "MVVM2.png")

struct Person { // Model
    let firstName: String
    let lastName: String
}

protocol GreetingViewModelProtocol: class {
    var greeting: String? { get }
    var greetingDidChange: ((GreetingViewModelProtocol) -> ())? { get set } // 2. 当有数据改变时的回调
    func showGreeting() // 1. 数据请求发起
}

class GreetingViewModel : GreetingViewModelProtocol {
    let person: Person
    var greeting: String? {
        didSet {
            self.greetingDidChange?(self)
        }
    }
    var greetingDidChange: ((GreetingViewModelProtocol) -> ())?
    required init(person: Person) {
        self.person = person
    }
    func showGreeting() {
        self.greeting = "Hello" + " " + self.person.firstName + " " + self.person.lastName
    }
}

class GreetingViewController : UIViewController {
    var viewModel: GreetingViewModelProtocol! {
        didSet {
            self.viewModel.greetingDidChange = { [unowned self] viewModel in
                self.greetingLabel.text = viewModel.greeting
            }
        }
    }
    let showGreetingButton = UIButton()
    let greetingLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showGreetingButton.addTarget(self.viewModel, action: Selector(("showGreeting")), for: .touchUpInside)
    }
    // layout code goes here
}
// Assembling of MVVM
let model = Person(firstName: "David", lastName: "Blaine")
let view = GreetingViewController()
let viewModel = GreetingViewModel(person: model)
view.viewModel = viewModel



// ------ 如何处理 TableViewDatasource 问题

// 去掉 public 试试，会不会报错?  -> 因为下面的 func 用的是 public ，需要开放协议保持一致
public struct ContactViewModel {
    var fullName: String!
}

public func <(lhs: ContactViewModel, rhs: ContactViewModel) -> Bool {
    return lhs.fullName.lowercased() < rhs.fullName.lowercased()
}

public func >(lhs: ContactViewModel, rhs: ContactViewModel) -> Bool {
    return lhs.fullName.lowercased() > rhs.fullName.lowercased()
}

class ContactViewModelController {
    
    fileprivate var contactViewModelList: [ContactViewModel] = []
//    fileprivate var dataManager = ContactLocalDataManager()

    var contactsCount: Int {
        return contactViewModelList.count
    }
    
    func retrieveContacts(_ success: (() -> Void)?, failure: (() -> Void)?) {
//        do {
//            let contacts = try dataManager.retrieveContactList()
//            contactViewModelList = contacts.map() { ContactViewModel(fullName: $0.fullName) }
//            success?()
//        } catch {
//            failure?()
//        }
    }
    
    func viewModel(at index: Int) -> ContactViewModel {
        return contactViewModelList[index]
    }
    
    func createContact(firstName: String, lastName: String,
                       success: ((ContactViewModel, Int) -> Void)?,
                       failure: (() -> Void)?) {
//        do {
//            let contact = try dataManager.createContact(firstName: firstName, lastName: lastName)
//            let contactViewModel = ContactViewModel(fullName: contact.fullName)
//            let insertionIndex = contactViewModelList.insertionIndex(of: contactViewModel) { $0 < $1 }
//            contactViewModelList.insert(contactViewModel, at: insertionIndex)
//            success?(contactViewModel, insertionIndex)
//        } catch {
//            failure?()
//        }
    }
}

class AddContactViewController: UIViewController {
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    
    var contactsViewModelController: ContactViewModelController?
    var didAddContact: ((ContactViewModel, Int) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTextField.becomeFirstResponder()
    }
    
    func didClickOnDoneButton(_ sender: UIBarButtonItem) {
        guard let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text
            else {
                return
        }
        
        if firstName.isEmpty || lastName.isEmpty {
            showEmptyNameAlert()
            return
        }
        
        dismiss(animated: true) { [unowned self] in
            self.contactsViewModelController?.createContact(firstName: firstName, lastName: lastName,
                                                            success: self.didAddContact, failure: nil)
        }
        
    }
    
    func didClickOnCancelButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func showEmptyNameAlert() {
        showMessage(title: "Error", message: "A contact must have first and last names")
    }
    
    fileprivate func showMessage(title: String, message: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
        present(alertView, animated: true, completion: nil)
    }
}

class ContactsTableViewCell: UITableViewCell {
    var cellModel: ContactViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    func bindViewModel() {
        textLabel?.text = cellModel?.fullName
    }
}


// Main
class ContactsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    let contactViewModelController = ContactViewModelController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        contactViewModelController.retrieveContacts({ [unowned self] in
            self.tableView.reloadData()
            }, failure: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let addContactNavigationController = segue.destination as? UINavigationController
        let addContactVC = addContactNavigationController?.viewControllers[0] as? AddContactViewController
        
        addContactVC?.contactsViewModelController = contactViewModelController
        addContactVC?.didAddContact = { [unowned self] (contactViewModel, index) in
            let indexPath = IndexPath(row: index, section: 0)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .left)
            self.tableView.endUpdates()
        }
    }
    
}

extension ContactsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as? ContactsTableViewCell
        guard let contactsCell = cell else {
            return UITableViewCell()
        }
        
        contactsCell.cellModel = contactViewModelController.viewModel(at: (indexPath as NSIndexPath).row)
        return contactsCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactViewModelController.contactsCount
    }
    
}