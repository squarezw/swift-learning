//: 什么是依赖注入
//: What is dependency injection?
//:
//: Basically, instead of having your objects creating a dependency or asking a factory object to make one for them, you pass the needed dependencies in to the object externally, and you make it somebody else's problem. This "someone" is either an object further up the dependency graph, or a dependency injector (framework) that builds the dependency graph. A dependency as I'm using it here is any other object the current object needs to hold a reference to.

//: 主要是方便测试中 Mock 数据
//:
//: https://en.wikipedia.org/wiki/Dependency_injection
//: https://www.bignerdranch.com/blog/dependency-injection-ios/
//: https://runningyoung.github.io/2015/06/30/2015-08-04-dependency-injection/
//:
//: 
//: 如何向一个5岁的小孩解释依赖注入
//: “When you go and get things out of the refrigerator for yourself, you can cause problems. You might leave the door open, you might get something Mommy or Daddy doesn’t want you to have. You might even be looking for something we don’t even have or which has expired.

//: What you should be doing is stating a need, “I need something to drink with lunch,” and then we will make sure you have something when you sit down to eat.”

//: 映射到面向对象程序开发中就是：高层类(5岁小孩)应该依赖底层基础设施(家长)来提供必要的服务。

//: 相关开源框架
//: objection 和 Typhoon

import UIKit

// :One of the major advantages of dependency injection is that it can make testing lots easier. Suppose you have an object which in its constructor does something like:

class MyClass {
    
}

let myObject: Any

class Factory {
    static func getObject() -> MyClass {
        return MyClass()
    }
}

class OldPattern {
    var myObj: MyClass?
    
    func someClass() {
        myObj = Factory.getObject()
    }
}


// This can be troublesome when all you want to do is run some unit tests on SomeClass, especially if myObject is something that does complex disk or network access. So now you're looking at mocking myObject but also somehow intercepting the factory call. Hard. Instead, pass the object in as an argument to the constructor. Now you've moved the problem elsewhere, but testing can become lots easier. Just make a dummy myObject and pass that in. The constructor would now look a bit like:

class NewPattern {
    var myObject: MyClass?
    
    func someClass(myObject: MyClass) {
        self.myObject = myObject;
    }
}

// This is one style of dependency injection - via the constructor. Several mechanisms are possible. As noted in the comments, one common alternative is to define a do-nothing constructor, and have the dependencies injected via property setters (h/t @MikeVella). Martin Fowler documents a third alternative (h/t @MarcDix), where classes explicitly implement an interface for the dependencies they wish injected.

// When not using dependency injection (such as in classes that do too much work in their constructors etc.), it tends to become much harder to isolate components in unit testing. Back in 2013 when I wrote this answer, this was a major theme on the Google Testing Blog. This remains the biggest advantage to me, as you may not always need the extra flexibility in your run-time design (for instance, for service locator or similar patterns), but you do often need to be able to isolate your classes during testing.

