import UIKit
import PlaygroundSupport

// SegmentedViewController is declared in SegmentedViewController.swift
// It's just a parent class to provide the basic UI (segmented control + tableView)

class DemoViewController: SegmentedViewController {
  let products: [Product] = loadProducts()

  var currentDataSource: (UITableViewDataSource & UITableViewDelegate)? {
    didSet {
      self.tableView.dataSource = currentDataSource
      self.tableView.delegate = currentDataSource
      self.tableView.reloadData()
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.setDataSource(type: .flat)
  }

  // This method is also called when the UISegmentedControl value changes
  // (see SegmentedViewController.swift)
  override func setDataSource(type: SourceType) {
    switch type {
    case .flat:
      let ds = ListDataSource<Product>(
        items: products,
        cellFactory: { [unowned self] in self.cell(for: $0) }
      )
      ds.onItemSelected = { print("Selected \($0) in flat list") }
      self.currentDataSource = ds
    case .categories:
      let ds = SectionsDataSource<Product>(
        items: products,
        categorizer: { (product: Product) in String(product.name.first ?? "_") },
        cellFactory: { [unowned self] in self.cell(for: $0) }
      )
      ds.onItemSelected = { print("Selected \($0) in sectionned list") }
      self.currentDataSource = ds
    }
  }

  private func cell(for product: Product) -> UITableViewCell {
    // Here ideally we'd have some custom cell specific for this UIViewController and dequeue it from the tableView
    let cell = UITableViewCell()
    cell.textLabel?.text = product.name
    cell.detailTextLabel?.text = product.category
    return cell
  }
}

// Present the view controller in the Live View window
let demoVC = DemoViewController()
PlaygroundPage.current.liveView = demoVC
