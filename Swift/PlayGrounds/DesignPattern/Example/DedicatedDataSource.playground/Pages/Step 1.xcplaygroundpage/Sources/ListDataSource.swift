import UIKit

public class ListDataSource: NSObject {
  let products: [Product]

  public init(products: [Product]) {
    self.products = products
  }

  func product(at indexPath: IndexPath) -> Product {
    return self.products[indexPath.row]
  }
}

extension ListDataSource: UITableViewDataSource {
  public func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.products.count
  }
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return cell(for: product(at: indexPath))
  }

  func cell(for product: Product) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = product.name
    return cell
  }
}
