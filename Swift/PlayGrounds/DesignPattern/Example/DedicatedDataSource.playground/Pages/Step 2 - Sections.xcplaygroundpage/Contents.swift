import UIKit
import PlaygroundSupport

class DemoViewController : UIViewController {
  var tableView: UITableView!

  var currentDataSource: UITableViewDataSource? {
    didSet {
      self.tableView.dataSource = currentDataSource
      self.tableView.reloadData()
    }
  }

  override func loadView() {
    self.tableView = UITableView()
    self.view = self.tableView
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let products = loadProducts()
    self.currentDataSource = SectionsDataSource(products: products)
  }
}

// Present the view controller in the Live View window
let demoVC = DemoViewController()
PlaygroundPage.current.liveView = demoVC


