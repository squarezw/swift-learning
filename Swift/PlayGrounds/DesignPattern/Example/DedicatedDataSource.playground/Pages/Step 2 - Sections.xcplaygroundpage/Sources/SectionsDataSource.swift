import UIKit

public class SectionsDataSource: NSObject {
  struct Section {
    let title: String
    let items: [Product]
  }
  let sections: [Section]

  public init(products: [Product]) {
    // Dispatch products into a dictionary according to their category
    let groups = Dictionary(grouping: products, by: { product in product.category })
    // Convert that dictionary into an array of Sections, then sort it by section title
    self.sections = groups.map(Section.init).sorted(by: { $0.title < $1.title })
  }

  func product(at indexPath: IndexPath) -> Product {
    return self.sections[indexPath.section].items[indexPath.row]
  }
}

extension SectionsDataSource: UITableViewDataSource {
  public func numberOfSections(in tableView: UITableView) -> Int {
    return self.sections.count
  }
  public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return self.sections[section].title
  }
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.sections[section].items.count
  }
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return cell(for: product(at: indexPath))
  }

  func cell(for product: Product) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = product.name
    return cell
  }
}
