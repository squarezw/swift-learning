import UIKit
import PlaygroundSupport

// SegmentedViewController is declared in SegmentedViewController.swift
// It's just a parent class to provide the basic UI (segmented control + tableView)

class DemoViewController: SegmentedViewController {
  let products: [Product] = loadProducts()

  var currentDataSource: (UITableViewDataSource & UITableViewDelegate)? {
    didSet {
      self.tableView.dataSource = currentDataSource
      self.tableView.delegate = currentDataSource
      self.tableView.reloadData()
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.setDataSource(type: .flat)
  }

  // This method is also called when the UISegmentedControl value changes
  // (see SegmentedViewController.swift)
  override func setDataSource(type: SourceType) {
    // Note: of course for long lists of products we could optimize that,
    // e.g. caching the DataSources once they have been created for the first time
    // to avoid re-creating every time the user changes the selected segment
    switch type {
    case .flat:
      let ds = ListDataSource(products: products)
      ds.onProductSelected = { print("Selected \($0) in flat list") }
      self.currentDataSource = ds
    case .categories:
      let ds = SectionsDataSource(products: products)
      ds.onProductSelected = { print("Selected \($0) in sectionned list") }
      self.currentDataSource = ds
    }
  }
}

// Present the view controller in the Live View window
let demoVC = DemoViewController()
PlaygroundPage.current.liveView = demoVC
