import UIKit

open class SegmentedViewController: UIViewController {
  public var tableView: UITableView!

  public enum SourceType: Int {
    case flat, categories
  }

  open override func loadView() {
    let segment = UISegmentedControl(items: ["Flat List", "Grouped List"])
    segment.backgroundColor = .white
    segment.tintColor = .orange
    segment.selectedSegmentIndex = 0
    segment.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)

    self.tableView = UITableView()

    let view = UIStackView(arrangedSubviews: [segment, tableView])
    view.backgroundColor = .gray
    view.axis = .vertical
    view.alignment = .fill
    view.spacing = 5.0

    self.view = view
  }

  @objc func segmentChanged(_ sender: UISegmentedControl) {
    let type = SourceType(rawValue: sender.selectedSegmentIndex) ?? .flat
    self.setDataSource(type: type)
  }

  open func setDataSource(type: SourceType) {
    fatalError("You must override this to switch over the SourceType and set currentDataSource appropriately")
  }
}
