import UIKit

public struct Product {
  public let name: String
  public let category: String
  public let price: Double
}

extension Product: CustomStringConvertible {
  public var description: String {
    return "\(name) (\(category)) @ \(price)€"
  }
}
