import UIKit

public func loadProducts() -> [Product] {
  return [
    Product(
      name: "Tomatoes",
      category: "Vegetables",
      price: 8.00
    ),
    Product(
      name: "Cod",
      category: "Fish",
      price: 12.00
    ),
    Product(
      name: "Potatoes",
      category: "Vegetables",
      price: 7.00
    ),
    Product(
      name: "Strawberries",
      category: "Fruits",
      price: 12.00
    ),
    Product(
      name: "Apples",
      category: "Fruits",
      price: 10.00
    ),
    Product(
      name: "Carrots",
      category: "Vegetables",
      price: 6.00
    ),
    Product(
      name: "Tuna",
      category: "Fish",
      price: 17.00
    ),
    Product(
      name: "Salmon",
      category: "Fish",
      price: 25.00
    ),
  ]
    .sorted(by: { $0.name < $1.name })
}
