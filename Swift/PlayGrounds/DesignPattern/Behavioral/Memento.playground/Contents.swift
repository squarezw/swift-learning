
import UIKit

UIImage(named: "structure-memento-with-nested-classes")

UIImage(named: "structure-memento-with-the-indermediate-interface")

UIImage(named: "structure-memento-even-stricter-encapsulation")

// Some tips
// A Caretaker like a Command object, which knows all related stuff.

// Originator class should have a special method, which captures
// originator's state inside a new memento object.
class Editor {
    fileprivate var x,y: Int
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
    // Not sure that it is putted either here or inside of Caretaker?
    func createSnapshot() -> Snapshot {
        // Memento is immutable object; that is why originator
        // passes its state to memento's constructor parameters.
        return Snapshot(editor: self, x: x, y: y)
    }
    
    func restore(memento: Snapshot) {
        self.x = memento.x
        self.y = memento.y
    }
    
    func doSomething() {
        self.x = Int.random(in: 0..<100)
        self.y = Int.random(in: 0..<100)
        print(self)
    }
}

extension Editor: CustomStringConvertible {
    var description: String {
        return "Hello: \(x) \(y)"
    }
}

// Memento stores past state of the editor
struct Snapshot {
    let editor: Editor
    let x,y: Int
}

// Command object can act as a caretaker. In such case, command
// gets a memento just before it changes the originator's state.
// When undo is requested, it restores originator's state with a
// memento.
class Command {
    private var history: [Snapshot] = []
    let editor: Editor
    
    init(editor: Editor) {
        self.editor = editor
    }
    
    func makeBackup() {
        history.append(editor.createSnapshot())
    }
    
    func undo() {
        guard let backup = history.popLast() else {
            return
        }
        editor.restore(memento: backup)
    }
}


let editor = Editor(x: 10, y: 20)
let caretaker = Command(editor: editor)
caretaker.makeBackup()

print(editor)

editor.doSomething()

caretaker.undo()

print(editor)


/// Sample 2

typealias Memento = NSDictionary

// Originator
protocol MementoConvertible {
    var memento: Memento { get }
    init?(memento: Memento)
}

struct GameState: MementoConvertible {
    
    private enum Keys {
        static let chapter = "com.valve.halflife.chapter"
        static let weapon = "com.valve.halflife.weapon"
    }
    
    var chapter: String
    var weapon: String
    
    init(chapter: String, weapon: String) {
        self.chapter = chapter
        self.weapon = weapon
    }
    
    init?(memento: Memento) {
        guard let mementoChapter = memento[Keys.chapter] as? String,
            let mementoWeapon = memento[Keys.weapon] as? String else {
                return nil
        }
        
        chapter = mementoChapter
        weapon = mementoWeapon
    }
    
    var memento: Memento {
        return [ Keys.chapter: chapter, Keys.weapon: weapon ]
    }
}

// Caretaker
enum CheckPoint {
    static func save(_ state: MementoConvertible, saveName: String) {
        let defaults = UserDefaults.standard
        defaults.set(state.memento, forKey: saveName)
        defaults.synchronize()
    }
    
    static func restore(saveName: String) -> Memento? {
        let defaults = UserDefaults.standard
        
        return defaults.object(forKey: saveName) as? Memento
    }
}

var gameState = GameState(chapter: "Black Mesa Inbound", weapon: "Crowbar")

gameState.chapter = "Anomalous Materials"
gameState.weapon = "Glock 17"
CheckPoint.save(gameState, saveName: "gameState1")

gameState.chapter = "Unforeseen Consequences"
gameState.weapon = "MP5"
CheckPoint.save(gameState, saveName: "gameState2")

gameState.chapter = "Office Complex"
gameState.weapon = "Crossbow"
CheckPoint.save(gameState, saveName: "gameState3")

if let memento = CheckPoint.restore(saveName: "gameState1") {
    let finalState = GameState(memento: memento)
    dump(finalState)
}
