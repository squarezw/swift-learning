
import UIKit

UIImage(named: "structure-chain")

/*
 In this example, the Chain of Responsibility is responsible for showing a contextual help related to the active UI element.
 
 The GUI elements are structured into a tree. The Dialog class that renders the main window is the root of the tree. The middle layer composed with Panels. The leaf-level Components are Buttons,  TextEdits and others.
 
 A Component is capable of showing contextual tooltips, as long as it has some help text assigned. Some complex components have their own way of showing contextual help.
 
 When a user points the mouse cursor to a component and presses the F1 key, the application fetches the component and sends it a help requests. The request bubbles up through all parent containers until it reaches the component capable of showing help.
 */

// Handler interface
protocol ComponentWithContextualHelp {
    func showHelp()
}

// Base class for simple components.
class Component: ComponentWithContextualHelp {
    var tooltipText: String? = nil
    
    // Container, which contains component, severs as a
    // following object in chain.
    var container: Container? = nil
    
    // Component shows tooltip if there is a help text assigned
    // to it. Otherwise it forwards the call to the container if
    // it exists.
    func showHelp() {
        if tooltipText != nil {
            // show tooltip
        } else {
            container?.showHelp()
        }
    }
}

// Containers can contain both simple components and other
// container as children. The chain relations are established
// here. The class inherits showHelp behavior from its parent.
class Container: Component {
    var children: [Component] = []
    
    func add(child: Component) {
        children.append(child)
        child.container = self
    }
}

// Primitive components may be fine with default help
// implementation...
class Button: Component {
    // ...
}

// But complex components may override the default
// implementation. If a help can not be provided in a new way,
// the component can always call the base implementation (see
// Component class).
class Panel: Container {
    var modalHelpText: String? = nil
    
    override func showHelp() {
        if modalHelpText != nil {
            // Show modal window with a help text.
        } else {
            super.showHelp()
        }
    }
}

// ...same as above...
class Dialog: Container {
    var wikiPageURL: String? = nil
    
    override func showHelp() {
        if wikiPageURL != nil {
            // Open the wiki help page.
        } else {
            super.showHelp()
        }
    }
}

// Client code.
class Application {
    // Each application configures the chain differently.
    func createUI() {
        let dialog = Dialog()
        dialog.wikiPageURL = "http://"
        let panel = Panel()
        panel.modalHelpText = "This panel does..."
        let ok = Button()
        ok.tooltipText = "This is a OK button that..."
        let cancel = Button()
        //...
        panel.add(child: ok)
        panel.add(child: cancel)
        dialog.add(child: panel)
    }
    
    // Imagine what happens here.
    func onF1KeyPress() {
//        let component = self.getComponentAtMouseCoords()
//        component.showHelp()
    }
}




/// Money Pile
final class MoneyPile {
    
    let value: Int
    var quantity: Int
    var nextPile: MoneyPile?
    
    init(value: Int, quantity: Int, nextPile: MoneyPile?) {
        self.value = value
        self.quantity = quantity
        self.nextPile = nextPile
    }
    
    func canWithdraw(amount: Int) -> Bool {
        
        var amount = amount
        
        func canTakeSomeBill(want: Int) -> Bool {
            return (want / self.value) > 0
        }
        
        var quantity = self.quantity
        
        while canTakeSomeBill(want: amount) {
            
            if quantity == 0 {
                break
            }
            
            amount -= self.value
            quantity -= 1
        }
        
        guard amount > 0 else {
            return true
        }
        
        if let next = self.nextPile {
            return next.canWithdraw(amount: amount)
        }
        
        return false
    }
}

final class ATM {
    private var hundred: MoneyPile
    private var fifty: MoneyPile
    private var twenty: MoneyPile
    private var ten: MoneyPile
    
    private var startPile: MoneyPile {
        return self.hundred
    }
    
    init(hundred: MoneyPile,
         fifty: MoneyPile,
         twenty: MoneyPile,
         ten: MoneyPile) {
        
        self.hundred = hundred
        self.fifty = fifty
        self.twenty = twenty
        self.ten = ten
    }
    
    func canWithdraw(amount: Int) -> String {
        return "Can withdraw: \(self.startPile.canWithdraw(amount: amount))"
    }
}

// Create piles of money and link them together 10 < 20 < 50 < 100.**
let ten = MoneyPile(value: 10, quantity: 6, nextPile: nil)
let twenty = MoneyPile(value: 20, quantity: 2, nextPile: ten)
let fifty = MoneyPile(value: 50, quantity: 2, nextPile: twenty)
let hundred = MoneyPile(value: 100, quantity: 1, nextPile: fifty)

// Build ATM.
var atm = ATM(hundred: hundred, fifty: fifty, twenty: twenty, ten: ten)
atm.canWithdraw(amount: 310) // Cannot because ATM has only 300
