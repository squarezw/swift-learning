import UIKit

UIImage(named: "structure-command")

/*
 
 Invoker  -> stores a reference to a Command object and uses it when an operation needs to be executed. Invoker works with commands only via their common interface, which usually exposes just a single execution method. Invokers are not responsible for creating command objects. They usually get a pre-created command from the Client via the constructor.
 
        ex:  Button
 
 Command  -> Declares the common interface for all concrete commands. The absolute minimum is a single method to run the actual operation.

 ConcreteCommand -> implement the actual operation. Some commands can be self-contained and immutable, accepting all necessary data just once, via constructor paramters. Others require a Receiver, an external context object.
 
        tips: could contain a receiver.
 
 Receiver -> contains business logic or data essential to a particular command. Commands may query these objects for addtional  info or delegate the entire operation to them. In some cases, receiver's code can be merged into command classes for the sake of simplicity.
 
        ex: a Editor, Context etc.
 
 Client   -> Creates and configures Concrete Command objects. Then passes these objects to appropriate Invokers.
 
 */

class Application {
    func doSomething() {}
}

class Editor {
    func doSomething() {}
}

class Receiver {
    func doSomething() {
        
    }
}

protocol Command {
    func execute()
}

class SimpleCommand: Command {

    var app: Application
    var editor: Editor
    
    init(app: Application, editor: Editor) {
        self.app = app
        self.editor = editor
    }
    
    func execute() {
        
    }
}

class ComplexCommand: Command {
    var receiver: Receiver
    
    init(receiver: Receiver) {
        self.receiver = receiver
    }
    
    func execute() {
        self.receiver.doSomething()
        print("ComplexCommand: some complex stuff should be done by a receiver object.\n")
    }
}

class Invoker {
    var onStart: Command?
    var onFinish: Command?
    
    func setOnStart(command: Command) {
        self.onStart = command
    }
    
    func setOnFinish(command: Command) {
        self.onFinish = command
    }
    
    func doSomethingImportant() {
        print("Invoker: Does anybody want something done before I begin?\n")
        
        if let cmd = self.onStart {
            cmd.execute()
        }
        
        print("Doing something really important...\n")
        
        print("Invoker: Does anybody want something done after I finish?\n")
        if let cmd = self.onFinish {
            cmd.execute()
        }
    }
}

// The Client code can be parametrize an invoker with any commands.

class Client {
    init() {
        let invoker = Invoker()
        invoker.setOnStart(command: SimpleCommand(app: Application(), editor: Editor()))
        let receiver = Receiver()
        invoker.setOnFinish(command: ComplexCommand(receiver: receiver))
        invoker.doSomethingImportant()
    }
}

Client()
