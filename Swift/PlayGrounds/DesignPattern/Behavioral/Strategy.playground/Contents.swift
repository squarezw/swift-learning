
import UIKit

/// Strategy is a behavioral design pattern that lets you define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from the clients that use it.

/// The strategy pattern is used to create an interchangeable family of algorithms from which the required process is chosen at run-time.

/// Real Example:
/// https://medium.com/flawless-app-stories/strategy-pattern-in-swift-1462dbddd9fe

UIImage(named: "structure-strategy")

// Common interface for all strategies.
protocol Strategy {
    func execute(a: Int, b:Int) -> Int
}

// Each concrete strategy provides unique implementation.
class ConcreteStrategyAdd: Strategy {
    func execute(a: Int, b: Int) -> Int {
        return a + b
    }
}

class ConcreteStrategySubtract: Strategy {
    func execute(a: Int, b: Int) -> Int {
        return a - b
    }
}

class ConcreteStrategyMultiply: Strategy {
    func execute(a: Int, b: Int) -> Int {
        return a * b
    }
}

// Context (as a client) always works with strategies through a
// common interface. It does not know or care which strategy is
// currently active.
class Context {
    private var strategy: Strategy
    
    init(strategy: Strategy) {
        self.strategy = strategy
    }
    
    func executeStrategy(a: Int, b:Int) -> Int {
        return strategy.execute(a: a, b: b)
    }
}

// The concrete strategy is picked on a higher level (for
// example, by application config) and passed to the client
// object. At any time, the strategy object can be replaced by a
// different strategy.
enum ActionType {
    case addition
    case subtraction
    case multiplication
}

class ExampleApplication {
    func main(a:Int, b:Int, action:ActionType) {
        // Create context object.
        // Read first number.
        // Read last number.
        // Read the desired action from user input.
        var strategy: Strategy?
        
        if action == ActionType.addition {
            strategy = ConcreteStrategyAdd()
        }
        
        if action == ActionType.subtraction {
            strategy = ConcreteStrategySubtract()
        }
        
        if action == ActionType.multiplication {
            strategy = ConcreteStrategyMultiply()
        }
        
        if let _ = strategy {
            let context = Context(strategy: strategy!)
            print(context.executeStrategy(a: a, b: b))
        }
    }
}

ExampleApplication().main(a: 3, b: 2, action: .subtraction)

/// Sample 2

protocol PrintStrategy {
    func print(_ string: String) -> String
}

final class Printer {
    
    private let strategy: PrintStrategy
    
    func print(_ string: String) -> String {
        return self.strategy.print(string)
    }
    
    init(strategy: PrintStrategy) {
        self.strategy = strategy
    }
}

final class UpperCaseStrategy: PrintStrategy {
    func print(_ string: String) -> String {
        return string.uppercased()
    }
}

final class LowerCaseStrategy: PrintStrategy {
    func print(_ string:String) -> String {
        return string.lowercased()
    }
}

var lower = Printer(strategy: LowerCaseStrategy())
lower.print("O tempora, o mores!")

var upper = Printer(strategy: UpperCaseStrategy())
upper.print("O tempora, o mores!")
