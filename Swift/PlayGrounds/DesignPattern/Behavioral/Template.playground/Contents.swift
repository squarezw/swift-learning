
import UIKit

/// Template Method is a behavioral design pattern that lets you define the skeleton of an algorithm and allow subclasses to redefine certain steps of the algorithm without changing its structure.

UIImage(named: "structure-template")


/// In this example, the Template Method pattern provides a layout for various branches of an artifical intelligence in a little strategy video game. It is simple to add a new race to the game: one just needs to create a new AI subclass and override the default methods declared in the base AI class.

/// The Template Pattern is used when two or more implementations of an algorithm exist. The template is defined and then built upon with further variations. Use this method when most (or all) subclasses need to implement the same behavior. Traditionally, this would be accomplished with abstract classes and protected methods (as in Java). However in Swift, because abstract classes don't exist (yet - maybe someday), we need to accomplish the behavior using interface delegation.

// Template method should be defined in a base class. Its
// body is a set of method class in a defined order.
// Usually, they are the steps of some algorithm.
class GameAI {
    func turn() {
        collectResources()
        buildStructures()
        buildUnits()
    }
    
    // Some of these steps may be implemented right in a base
    // class.
    func collectResources() {
        for s in buildStructures() {
            print(s)
        }
    }
    
    // And some of them may be defined as abstract.
    func buildStructures() -> [String] {
        preconditionFailure("This method must be overridden")
    }
    
    func buildUnits() {
        preconditionFailure("This method must be overridden")
    }
    
    // By the way, a class could have several template methods.
    func attack() {
        
    }
    
    func sendScouts(position: Int) {
        preconditionFailure("This method must be overridden") 
    }
    
    func sendWarriors(position: Int) {
        preconditionFailure("This method must be overridden")
    }
}

// Subclasses can provide their own steps implementation as long
// as they don't change the template method.
class OrcsAI: GameAI {
    override func buildStructures() -> [String] {
        // Build farms, then barracks, then stronghold.
        return []
    }
    
    override func buildUnits() {
        /// if (there are plenty of resources) then
//            if (there are no scouts)
                // Build peon, add it to scouts group.
//            else
            // Build grunt, add it to warriors group.
    }
    
    override func sendScouts(position: Int) {
        
    }
    
    override func sendWarriors(position: Int) {
        
    }
}


// Subclasses may not only implement abstract steps but also
// override default steps from the base class.
class MonstersAI: GameAI {
    override func collectResources() {
        // Do nothing
    }
    
    override func buildStructures() -> [String] {
        // Do nothing
        return []
    }
    
    override func buildUnits() {
        // Do nothing
    }
}

let AI = MonstersAI()
AI.turn()

/// Sample II

protocol ICodeGenerator {
    func crossCompile()
}

protocol IGeneratorPhases {
    func collectSource()
    func crossCompile()
}

class CodeGenerator : ICodeGenerator{
    var delegate: IGeneratorPhases
    
    init(delegate: IGeneratorPhases) {
        self.delegate = delegate
    }
    
    private func fetchDataforGeneration(){
        //common implementation
        print("fetchDataforGeneration invoked")
    }
    
    //Template method
    final func crossCompile() {
        fetchDataforGeneration()
        delegate.collectSource()
        delegate.crossCompile()
    }
    
}

class HTMLGeneratorPhases : IGeneratorPhases {
    func collectSource() {
        print("HTMLGeneratorPhases collectSource() executed")
    }
    
    func crossCompile() {
        print("HTMLGeneratorPhases crossCompile() executed")
    }
}

class JSONGeneratorPhases : IGeneratorPhases {
    func collectSource() {
        print("JSONGeneratorPhases collectSource() executed")
    }
    
    func crossCompile() {
        print("JSONGeneratorPhases crossCompile() executed")
    }
}
