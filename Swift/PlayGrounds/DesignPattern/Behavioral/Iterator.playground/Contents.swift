
import UIKit

UIImage(named: "structure-iterator")


// The common interface for all iterators.
protocol Iterator {
    func hasMore() -> Bool
    func getNext() -> String
}

// The concrete iterator class.
class CommonIterator: Iterator {
    // The iterator needs a reference to the collection that it
    // traverses.
    var collection: Bunch
    var id, type: String
    
    // An iterator object traverses collection independently
    // from other iterators. Therefore it has to store the
    // iteration state.
    var currentPosition: Int = 0
    lazy var cache: [String] =  {
        return self.collection.sendRequest(id: id, type: type)
    }()
    
    init(collection: Bunch, id: String, type: String) {
        self.collection = collection
        self.id = id
        self.type = type
    }
    
    // Each concrete iterator class has its own implementation
    // of the common iterator interface.
    func getNext() -> String {
        let item = cache[currentPosition]
        if hasMore() {
            currentPosition += 1
        }
        return item
    }
    
    func hasMore() -> Bool {
        return currentPosition < cache.count
    }
}


// Factory
// declares the interface for retrieving iterator from the collection

// The collection interface must declare a factory method for
// producing iterators. You can declare several methods if there
// are different kinds of iteration available in your program.
protocol IterableCollection {
    func createItertor() -> Iterator
    func createItertorAdvance() -> Iterator
}

// Each concrete collection will be coupled to a set of concrete
// iterator classes it returns. But the client will not be,
// since the signature of these methods returns iterator
// interfaces.
class Bunch: IterableCollection {
    // ... The bulk of the collection's code should go here ...
    
    func createItertor() -> Iterator {
        return CommonIterator(collection: self, id: "MockId", type: "friends")
    }
    
    func createItertorAdvance() -> Iterator {
        return CommonIterator(collection: self, id: "MockId", type: "family")
    }
    
    func sendRequest(id: String, type: String) -> [String] {
        return ["1", "2", "3"]
    }
}

// Here is another useful trick: you can pass an iterator to a
// client class, instead of giving it access to a whole
// collection. This way, you do not expose the collection to the
// client.
//
// But there is another benefit: you can change the way the
// client works with the collection at run time by passing it a
// different iterator. This is possible because the client code
// is not coupled to concrete iterator classes.
class SocialSpammer {
    func send(itertor: Iterator, message: String) {
        while itertor.hasMore() {
            let item = itertor.getNext()
            // send message for each
            print("\(message) to \(item)")
        }
    }
}

// The application class configures collections and iterators
// and then passes them to the client code.
class Application {
    var collection: IterableCollection
    var spammer: SocialSpammer
    
    init() {
        self.collection = Bunch()
        self.spammer = SocialSpammer()
    }
    
    func sendSpam() {
        let iterator = collection.createItertor()
        spammer.send(itertor: iterator, message: "Hello")
    }
}

Application().sendSpam()


// Another sample

/// Any object conform to IteratorProtocol would be able to iteration.
struct Novella {
    let name: String
}

struct Novellas {
    let novellas: [Novella]
}

struct NovellasIterator: IteratorProtocol {
    
    private var current = 0
    private let novellas: [Novella]
    
    init(novellas: [Novella]) {
        self.novellas = novellas
    }
    
    mutating func next() -> Novella? {
        defer { current += 1 }
        return novellas.count > current ? novellas[current] : nil
    }
}

extension Novellas: Sequence {
    func makeIterator() -> NovellasIterator {
        return NovellasIterator(novellas: novellas)
    }
}

let greatNovellas = Novellas(novellas: [Novella(name: "The Mist")] )

for novella in greatNovellas {
    print("I've read: \(novella)")
}


/// build-in Sample of IteratorProtocol
/// For example, consider a custom Countdown sequence. You can initialize the Countdown sequence with a starting integer and then iterate over the count down to zero. The Countdown structure’s definition is short: It contains only the starting count and the makeIterator() method required by the Sequence protocol.
///
/// IteratorProtocol acts as Iterator Interface
/// acts as Concrete Iterator
struct CountdownIterator: IteratorProtocol {
    let countdown: Countdown
    var times = 0
    
    init(_ countdown: Countdown) {
        self.countdown = countdown
    }
    
    mutating func next() -> Int? {
        let nextNumber = countdown.start - times
        guard nextNumber > 0
            else { return nil }
        
        times += 1
        return nextNumber
    }
}

/// Sequence acts as Collection Interface
/// acts as Concrete Collection
struct Countdown: Sequence {
    let start: Int
    
    func makeIterator() -> CountdownIterator {
        return CountdownIterator(self)
    }
}

let threeTwoOne = Countdown(start: 3)
for i in threeTwoOne {
    print(i)
}




