
import UIKit

/// Visitor is a behavioral design pattern that lets you define a new operation without changing the classes of the objects on which it operates.

/// The visitor pattern is used to separate a relatively complex set of structured data classes from the functionality that may be performed upon the data that they hold.

UIImage(named: "structure-visitor")

// A complex hierarchy of components.
protocol Shape {
    func move(x: Int,y: Int)
    func draw()
    func accept(v: Visitor)
}

extension Shape {
    func move(x:Int, y:Int) {
        
    }
    
    func draw() {
        
    }
}

// It is crucial to implement the accept() method in every
// single component, not just a base class. It helps the program
// to pick a proper method on the visitor class in case if a
// given component's type is unknow.
class Dot: Shape {
    func accept(v: Visitor) {
        v.visitDot(d: self)
    }
}

class Circle: Shape {
    func accept(v: Visitor) {
        v.visitCircle(c: self)
    }
}

class Rectangle: Shape {
    func accept(v: Visitor) {
        v.visitRectangle(r: self)
    }
}

class CompoundShape: Shape {
    func accept(v: Visitor) {
        v.visitCompoundShape(cs: self)
    }
}


// Visitor interface must have visiting methods for the every
// single component. Note that each time you add a new class to
// the component history, you will need to add a method to the
// visitor classes. In this case, you might consider avoiding
// visitor altogether.
protocol Visitor {
    func visitDot(d: Dot)
    func visitCircle(c: Circle)
    func visitRectangle(r: Rectangle)
    func visitCompoundShape(cs: CompoundShape)
}

// Concrete visitor adds a single operation to the entire
// hierarchy of components. Which means that if you need to add
// multiple operations, you will have to create several visitor.
class XMLExportVisitor: Visitor {
    func visitDot(d: Dot) {
        // Export the dot's id and center coordinates.
    }
    
    func visitCircle(c: Circle) {
        // Export the circle's id, center coordinates and
        // radius.
    }
    
    func visitRectangle(r: Rectangle) {
        // Export the rectangle's id, left-top coordinates,
        // width and height.
    }
    
    func visitCompoundShape(cs: CompoundShape) {
        // Export the shape's id as well as the list of its
        // children's ids.
    }
}

// Application can use visitor along with any set of components
// without checking their type first. Double dispatch mechanism
// guarantees that a proper visiting method will be called for
// any given component.

class Application {
    let allShapes: [Shape] = []
    
    func export() {
        let exportVisitor = XMLExportVisitor()
        
        for shape in allShapes {
            shape.accept(v: exportVisitor)
        }
    }
}

// If you wonder why do we need the accept method in this example, then read our article Visitor and Double Dispatch, which addresses that question in detail.

///  ============

//// Visitor and Double Dispatch

// Let's take a look at following class hierarchy of geometric shapes (beware the pseudocode):

protocol Graphic {
    func draw()
}

class TShape: Graphic {
    let id: Int? = nil
    
    func draw() {
        
    }
}

class TDot: TShape {
    let x: Int? = nil
    let y: Int? = nil
    
    override func draw() {
        
    }
}

class TCircle: TDot {
    let radius: Int? = nil
    
    override func draw() {
        
    }
}

class TRectangle: TShape {
    let width: Int? = nil
    let height: Int? = nil
    
    override func draw() {
        
    }
}

class TCompoundGraphic: Graphic {
    let children: [Graphic] = []
    
    func draw() {
        // ...
    }
}

/// The code works fine and the app is in production. But one day you decided to make an export feature. The export code would look alien if placed in these classes. So instead of adding export to all classes of this hierarchy you decided to create a new class, external to the hierarchy, and put all the export logic inside. The class would get methods for exporting public state of ech objects into XML strings:

class Exporter {
    func export(s: TShape) {
        print("Exporting shape")
    }
    func export(d: TDot) {
        print("Exporting dot")
    }
    func export(c: TCircle) {
        print("Exporting circle")
    }
    func export(r: TRectangle) {
        print("Exporting rectangle")
    }
    func export(cs: TCompoundGraphic) {
        print("Exporting compound")
    }
}

// The code look good, but let's try it out:

class App {
    func export(shape: TShape) {
        let exporter = Exporter()
        exporter.export(s: shape)
    }
}

App().export(shape: TCircle())
// Unfortunatelly, this will output "Exporting shape".

// Wait! Why?!

// Thinking as a compiler
// Note: the following information is true for the most modern object oriented programming languages (Java, C#, PHP and others).

/**
 Late/dynamic binding
 Pretend that you are compiler. You have to decide how to compile following code:
 
 method drawShape(shape: Shape) is
 shape.draw();
 Let's see... the draw() method defined in Shape class. Wait a sec, but there are also four subclasses that override this method. Can we safely decide which of the implementations to call here? Doesn't look so. The only way to know for sure is to launch the program and check the class of an object passed to the method. The only thing we know for sure is that object will have implementation of the draw() method.
 
 So the resulting machine code will be checking class of the s parameter and picking the draw() implementation from the appropriate class.
 
 Such dynamic type check is called late (or dynamic) binding:
 
 Late, because we link object and its implementation after compilation,at runtime.
 Dynamic, because every new object might need to be linked to different implementation.
 */


/**
 Early/static binding
 Now, let's "compile" following code:
 
 method exportShape(shape: Shape) is
 Exporter exporter = new Exporter()
 exporter.export(shape);
 Everything is clear with the second line: the Exporter class doesn't have a constructor, so we just instantiate an object. What's about the export() call? The Exporter has five method with the same name that differ with parameter types. Which one to call? Looks like we're going to need a dynamic binding here as well.
 
 But there's another problem. What if there is a shape class that does not have appropriate export() method in Exporter class? For instance, an Ellipse object. Compiler can't guarantee that appropriate overloaded method exists in contrast with overridden methods. The ambiguous situation arise which a compiler can't allow.
 
 Therefore, compiler developers use a safe path and use the early (or static) binding for overloaded methods:
 
 Early because it happens at compile time, before program is launched.
 Static because it can't be altered at runtime.
 Let's return to our example. We are sure that incoming argument will be of Shape hierarchy: either the Shape class or one of its subclasses. We also know that Exporter class has basic implementation of the export that supports Shape class: export(s: Shape).
 
 That's the only implementation which can be safely linked to a given code without making things ambiguous. That's why even if we pass a Rectangle object into exportShape, the exporter will still call a export(s: Shape) method.
 */


// Double dispatch
// Double dispatch is a trick that allows using dynamic binding alongside with overloaded methods. Here how it's done:

class TVisitor {
    func visit(s: XShape) {
        print("Visited shape")
    }
    func visit(d: XDot) {
        print("Visited dot")
    }
}

protocol TGraphic {
    func accept(v: TVisitor)
}

class XShape: TGraphic {
    func accept(v: TVisitor) {
        // Compiler knows for sure that `this` is a `Shape`.
        // Which means that the `visit(s: Shape)` can be safely called.
        v.visit(s: self)
    }
}

class XDot: XShape {
    override func accept(v: TVisitor) {
        // Compiler knows that `this` is a `Dot`.
        // Which means that the `visit(s: Dot)` can be safely called.
        v.visit(d: self)
    }
}

let v = TVisitor()
let g = XDot()

// The `accept()` method is overriden, not overloaded. Compiler binds it
// dynamically. Therefore the `accept()` will be executed on a class that
// corresponds to an object calling a method (in our case, the `Dot` class).
g.accept(v: v);

// Output: "Visited dot"


/// Afterword
//  Even though the Visitor pattern is built on the double dispatch principle, that's not its primary purpose. Visitor allows adding "external" operations to a whole class hierarchy without changing existing code of these classes.


/// Sample 3

protocol PlanetVisitor {
    func visit(planet: PlanetAlderaan)
    func visit(planet: PlanetCoruscant)
    func visit(planet: PlanetTatooine)
    func visit(planet: MoonJedah)
}

protocol Planet {
    func accept(visitor: PlanetVisitor)
}

class MoonJedah: Planet {
    func accept(visitor: PlanetVisitor) { visitor.visit(planet: self) }
}

class PlanetAlderaan: Planet {
    func accept(visitor: PlanetVisitor) { visitor.visit(planet: self) }
}

class PlanetCoruscant: Planet {
    func accept(visitor: PlanetVisitor) { visitor.visit(planet: self) }
}

class PlanetTatooine: Planet {
    func accept(visitor: PlanetVisitor) { visitor.visit(planet: self) }
}



class NameVisitor: PlanetVisitor {
    var name = ""
    
    func visit(planet: PlanetAlderaan)  { name = "Alderaan" }
    func visit(planet: PlanetCoruscant) { name = "Coruscant" }
    func visit(planet: PlanetTatooine)  { name = "Tatooine" }
    func visit(planet: MoonJedah)         { name = "Jedah" }
}

let planets: [Planet] = [PlanetAlderaan(), PlanetCoruscant(), PlanetTatooine(), MoonJedah()]

let names = planets.map { (planet: Planet) -> String in
    let visitor = NameVisitor()
    planet.accept(visitor: visitor)
    return visitor.name
}


/// Sample 4

// In this example, the Visitor pattern helps to introduce a reporting feature into an existing class hierarchy: Company > Department > Employee

// Once the Visitor infrastructure is added to the app, you can easily add other similar behaviors to the app, without changing the existing classes.


/**
 * The Component interface declares a method of accepting visitor objects.
 *
 * In this method, a Concrete Component must call a specific Visitor's method
 * that has the same parameter type as that component.
 */
protocol Entity {
    func accept(vistor: Visitor4) -> String
}

/**
 * The Company Concrete Component.
 */
class Company: Entity {
    let name: String
    
    let departments: [Department]
    
    init(name: String, departments: [Department]) {
        self.name = name
        self.departments = departments
    }
    
    func accept(vistor: Visitor4) -> String {
        // See, the Company component must call the visitCompany method. The
        // same principle applies to all components.
        return vistor.visitCompany(company: self)
    }
}

/**
 * The Department Concrete Component.
 */
class Department: Entity {
    let name: String
    
    let employees: [Employee]
    
    init(name: String, employees: [Employee]) {
        self.name = name
        self.employees = employees
    }
    
    func cost() -> Int {
        var cost = 0
        for employee in employees {
            cost += employee.salary
        }
        return cost
    }
    
    func accept(vistor: Visitor4) -> String {
        return vistor.visitDepartment(department: self)
    }
}

class Employee: Entity {
    let name: String
    
    let position: String
    
    let salary: Int
    
    init(name: String, position: String, salary: Int) {
        self.name = name
        self.position = position
        self.salary = salary
    }
    
    func accept(vistor: Visitor4) -> String {
        return vistor.visitEmployee(employee: self)
    }
}

/**
 * The Visitor interface declares a set of visiting methods for each of the
 * Concrete Component classes.
 */
protocol Visitor4 {
    func visitCompany(company: Company) -> String
    func visitDepartment(department: Department) -> String
    func visitEmployee(employee: Employee) -> String
}

/**
 * The Concrete Visitor must provide implementations for every single class of
 * the Concrete Components.
 */
class SalaryReport: Visitor4 {
    func visitCompany(company: Company) -> String {
        var output = ""
        var total = 0
        
        for department in company.departments {
            total += department.cost()
            output += "\n-- \(visitDepartment(department: department))"
        }
        
        output = company.name + "(\(total)) \n\n" + output
        
        return output
    }
    
    func visitDepartment(department: Department) -> String {
        var output = ""
        
        for employee in department.employees {
            output += "   \(visitEmployee(employee: employee)) \n\n"
        }
        
        output = "\(department.name) (\(department.cost())) \n\n" + output
        
        return output
    }
    
    func visitEmployee(employee: Employee) -> String {
        return "\(employee.salary) \(employee.name) (\(employee.position))"
    }
}

/**
 * The client code.
 */

var mobileDev = Department(name: "Mobile Development", employees: [
    Employee(name: "Albert Falmore", position: "designer", salary: 100000),
    Employee(name: "Ali Halabay", position: "programmer", salary: 100000),
    Employee(name: "Sarah Konor", position: "programmer", salary: 90000),
    Employee(name: "Monica Ronaldino", position: "QA engineer", salary: 31000)
    ])

var techSupport = Department(name: "Tech Support", employees: [
    Employee(name: "Larry Ulbrecht", position: "supervisor", salary: 70000),
    Employee(name: "Elton Pale", position: "operator", salary: 30000),
    Employee(name: "Rajeet Kumar", position: "operator", salary: 34000),
    Employee(name: "John Burnovsky", position: "operator", salary: 35000)
    ])

var company = Company(name: "SuperStarDevelopment", departments: [mobileDev, techSupport])

var report = SalaryReport()

print("Client: I can print a report for a whole company:\n\n")
print(company.accept(vistor: report))

print("\nClient: ...or just for a single department:\n\n")
print(techSupport.accept(vistor: report))

print("\nClient: ...or just for a employee:\n\n")
print(mobileDev.employees.first!.accept(vistor: report))
