
import UIKit

/// State is a behavioral design pattern that allows an object to alter its behavior when its internal state changes. The object will appear to change its class.

/// The state pattern is used to alter the behaviour of an object as its internal state changes. The pattern allows the class for an object to apparently change at run-time.


/// See also: https://github.com/kingreza/Swift-State

UIImage(named: "structure-state")

// Context
class Context {
    lazy var state: State = {
        return ConcreteStates(context: self)
    }()
    
    init() {
        
    }
    
    func changeState(state: State) {
        self.state = state
    }
    
    func doThis() {
        state.doThis()
    }
    
    func doThat() {
        state.doThat()
    }
}

protocol State {
    func doThis()
    func doThat()
}

class ConcreteStates: State {
    weak var context: Context?
    
    init(context: Context) {
        self.context = context
    }
    
    func doThis() {
        print("Concrete A do this")
    }
    
    func doThat() {
        print("Concrete A do that")
    }
}

class AnotherConcreteStates: State {
    weak var context: Context?
    
    init(context: Context) {
        self.context = context
    }
    
    func doThis() {
        print("Another concrete do this")
    }
    
    func doThat() {
        print("Another concrete do that")
    }
}


let context = Context()
context.doThat()
context.doThis()

let state = AnotherConcreteStates(context: context)
context.changeState(state: state)
context.doThis()
context.doThat()


/// Sample 2

// Common interface for all states.
protocol PlayState {
    var player: Player {get}
    init(_ player: Player)
    
    func clickLock()
    func clickPlay()
    func clickNext()
    func clickPrevious()
}

class CommonState: PlayState {
    func clickLock() {
        
    }
    
    func clickPlay() {
        
    }
    
    func clickNext() {
        
    }
    
    func clickPrevious() {
        
    }
    
    let player: Player
    
    required init(_ player: Player) {
        self.player = player
    }
}

// Concrete states provide the special implementation for all
// interface methods.
class LockedState: CommonState {
    override func clickLock() {
        if player.playing {
            player.changeState(PlayingState(player))
        } else {
            player.changeState(ReadyState(player))
        }
    }
    
    override func clickPlay() {
        // Do nothing
    }
    
    override func clickNext() {
        // Do nothing
    }
    
    override func clickPrevious() {
        // Do nothing
    }
}

// They can also trigger state transitions in the context.
class ReadyState: CommonState {
    override func clickLock() {
        player.changeState(LockedState(player))
    }
    
    override func clickPlay() {
        player.startPlayback()
        player.changeState(PlayingState(player))
    }
    
    override func clickNext() {
        player.nextSong()
    }
    
    override func clickPrevious() {
        player.previousSong()
    }
}

class PlayingState: CommonState {
    override func clickLock() {
        player.changeState(LockedState(player))
    }
    
    override func clickPlay() {
        player.stopPlayback()
        player.changeState(ReadyState(player))
    }
    
    override func clickNext() {
        if player.event == "DoubleClick" {
            player.nextSong()
        } else {
            player.fastForward(5)
        }
    }
    
    override func clickPrevious() {
        if player.event == "DoubleClick" {
            player.previous()
        } else {
            player.rewind(5)
        }
    }
}

// Player acts as a context.
class Player {
    lazy var state: PlayState = {
        return ReadyState(self)
    }()
    let UI: UIView
    let event: String = ""
    var playing: Bool = false
    
    init() {
        self.UI = UIView()
//        self.UI.lockButton.onClick(self.clickLock)
//        self.UI.playButton.onClick(self.clickPlay)
//        self.UI.nextButton.onClick(self.clickNext)
//        self.UI.prevButton.onClick(self.clickPrevious)
    }
    
    func changeState(_ state: PlayState) {
        self.state = state
    }
    
    func clickLock() {
        state.clickLock()
    }
    
    func clickPlay() {
        state.clickPlay()
    }
    
    func clickNext() {
        state.clickNext()
    }
    
    func clickPrevious() {
        state.clickPrevious()
    }
    
    func startPlayback() {
        
    }
    
    func stopPlayback() {
        
    }
    
    func nextSong() {
        
    }
    func previousSong() {
        
    }
    
    func previous() {
        
    }
    
    func fastForward(_ sec: Int) {
        
    }
    
    func rewind(_ sec: Int) {
        
    }
}


// Sample 2

final class SContext {
    private var state: SState = UnauthorizedState()
    
    var isAuthorized: Bool {
        get { return state.isAuthorized(context: self) }
    }
    
    var userId: String? {
        get { return state.userId(context: self) }
    }
    
    func changeStateToAuthorized(userId: String) {
        state = AuthorizedState(userId: userId)
    }
    
    func changeStateToUnauthorized() {
        state = UnauthorizedState()
    }
    
}

protocol SState {
    func isAuthorized(context: SContext) -> Bool
    func userId(context: SContext) -> String?
}

class UnauthorizedState: SState {
    func isAuthorized(context: SContext) -> Bool { return false }
    
    func userId(context: SContext) -> String? { return nil }
}

class AuthorizedState: SState {
    let userId: String
    
    init(userId: String) { self.userId = userId }
    
    func isAuthorized(context: SContext) -> Bool { return true }
    
    func userId(context: SContext) -> String? { return userId }
}

let userContext = SContext()
(userContext.isAuthorized, userContext.userId)
userContext.changeStateToAuthorized(userId: "admin")
(userContext.isAuthorized, userContext.userId) // now logged in as "admin"
userContext.changeStateToUnauthorized()
(userContext.isAuthorized, userContext.userId)
