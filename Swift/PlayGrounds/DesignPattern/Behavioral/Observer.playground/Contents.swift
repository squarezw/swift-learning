
import UIKit

UIImage(named: "structure-observer")

protocol Publisher {
    func subscribe(t: ConcreteSubscriber)
    func unsubscribe(t: ConcreteSubscriber)
    func notifySubscribers()
}

class ConcretePublisher: Publisher {
    
    var subscribers: [ConcreteSubscriber] = []
    
    func subscribe(t: ConcreteSubscriber) {
        subscribers.append(t)
    }
    
    func unsubscribe(t: ConcreteSubscriber) {
        if let index = subscribers.firstIndex(where: {$0 === t}) {
            subscribers.remove(at: index)
        }
    }
    
    func notifySubscribers() {
        for item in subscribers {
            item.update(data: "Update")
        }
    }
    
    func mainBusinessLogic() {
        print("Logic...")
        notifySubscribers()
    }

}

protocol Subscriber: class {
    associatedtype T
    func update(data: T)
}

class ConcreteSubscriber: Subscriber {
    typealias T = String
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func update(data: T) {
        print("event: \(data) \(self.name)")
    }
}


let publisher = ConcretePublisher()

let subscriber = ConcreteSubscriber(name: "1")
let subscriber2 = ConcreteSubscriber(name: "2")
publisher.subscribe(t: subscriber)
publisher.subscribe(t: subscriber2)
publisher.mainBusinessLogic()

publisher.unsubscribe(t: subscriber2)
publisher.mainBusinessLogic()


// Base publisher class. It should include the subscription
// management code and notification methods.
class EventManager {
    var listeners: [String: [EventListener]] = [:]
    
    func subscribe(eventType: String, listener: EventListener) {
        listeners[eventType]?.append(listener)
    }
    
    func unsubscribe(eventType: String, listener: EventListener) {
        guard var listeners = listeners[eventType],
            let index = listeners.firstIndex(where: {$0 === listener}) else {
            return
        }
        listeners.remove(at: index)
    }
    
    func notify(eventType: String, data: Data) {
        guard let listeners = listeners[eventType] else {
            return
        }
        
        for listener in listeners {
            listener.update(file: data)
        }
    }
}

// Concrete publisher, which contains real business logic
// interesting for some subscribers. We could derive this class
// from a base publisher, but that is not always possible in
// real life, since the concrete publisher might already have a
// different parent class. In this case, you can patch the
// subscription logic in with composition, just like we did it
// here.
class Editor {
    var events: EventManager
    private var file: Data
    
    init() {
        self.events = EventManager()
        self.file = Data(count: 2)
    }
    
    // Business logic methods can notify subscribers about the
    // changes.
    func openFile(path: String) {
        let result = self.file.base64EncodedData()
        events.notify(eventType: "open", data: result)
    }
    
    func saveFile(){
        let result = self.file.base64EncodedData()
        events.notify(eventType: "save", data: result)
    }
}

// Common subscribers interface. By the way, modern programming
// languages allow to simplify this code and use functions as
// subscribers.
protocol EventListener: class {
    func update(file: Data)
}

// List of concrete listeners. They react to publisher updates
// by doing some useful work.
class LoggingListener: EventListener {
    private var log: Data
    private let message: String
    
    init(log_filename: String, message: String) {
        self.log = Data(base64Encoded: log_filename)!
        self.message = message
    }
    
    func update(file: Data) {
        self.log = file
        print(file.description)
    }
}

class EmailAlertsListener: EventListener {
    private let email: String
    private let message: String
    
    init(email: String, message: String) {
        self.email = email
        self.message = message
    }
    
    func update(file: Data) {
        print(file.description)
    }
}

// Application can configure publishers and subscribers even in
// run time.

class Application {
    var editor: Editor = Editor()
    
    func config() {
        let logger = LoggingListener(log_filename: "/path/to/log.txt", message: "Someone has opened file")
        editor.events.subscribe(eventType: "open", listener: logger)
        
        let emailAlters = EmailAlertsListener(email: "admin@example.com", message: "Someone has changed the file")
        editor.events.subscribe(eventType: "save", listener: emailAlters)
    }
}

let app = Application()
app.config()
app.editor.openFile(path: "Open file!")
