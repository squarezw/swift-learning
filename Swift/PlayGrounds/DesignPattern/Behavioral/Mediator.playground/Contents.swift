
import UIKit

UIImage(named: "structure-mediator")

protocol Mediator {
    associatedtype AnyEvent
    associatedtype ComponentType: Component
    func notify(_ sender: ComponentType, event: AnyEvent)
}

class ConcreteMediator: Mediator {
    typealias ComponentType = BaseComponent
    
    private var componentA: ComponentA
    private var componentB: ComponentB
    private var componentC: ComponentC
    
    init(_ a: ComponentA, b: ComponentB, c: ComponentC) {
        defer {
            self.componentA.mediator = self
            self.componentB.mediator = self
            self.componentC.mediator = self
        }
        self.componentA = a
        self.componentB = b
        self.componentC = c
    }
    
    func notify(_ sender: ComponentType, event: String) {
        if sender is ComponentA {
            if event == "B" {
                self.componentB.react(sender: sender)
            }
            if event == "C" {
                self.componentC.react(sender: sender)
            }
        }
        
        if sender is ComponentB {
            self.componentC.react(sender: sender)
        }
    }
    
    deinit {
        print("Hi dealloc me")
    }
}

protocol Component {
    associatedtype MediatorType: Mediator
    var mediator: MediatorType? {get set}
}

class BaseComponent: Component {
    weak var mediator: ConcreteMediator?
    func react(sender: BaseComponent) {
        
    }
}

extension BaseComponent: CustomStringConvertible {
    var description: String {
        return "\(type(of: self))"
    }
}

class ComponentA: BaseComponent {
    func click() {
        self.mediator?.notify(self, event: "C")
    }
    
    func touch() {
        self.mediator?.notify(self, event: "B")
    }
    
    override func react(sender: BaseComponent) {
        print("A: Reacting from \(sender)")
    }
}

class ComponentB: BaseComponent {
    func click() {
        self.mediator?.notify(self, event: "A")
    }
    
    override func react(sender: BaseComponent) {
        print("B: Reacting from \(sender)")
    }
}

class ComponentC: BaseComponent {
    override func react(sender: BaseComponent) {
        print("C: Reacting from \(sender)")
    }
}


let a = ComponentA()
let b = ComponentB()
let c = ComponentC()
_ = ConcreteMediator(a, b: b, c: c)

a.click()
a.touch()
b.click()

/// Sample 2

protocol Receiver {
    associatedtype MessageType
    func receive(message: MessageType)
}

protocol Sender {
    associatedtype MessageType
    associatedtype ReceiverType: Receiver
    
    var recipients: [ReceiverType] { get }
    
    func send(message: MessageType)
}

struct Programmer: Receiver {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func receive(message: String) {
        print("\(name) received: \(message)")
    }
}

final class MessageMediator: Sender {
    internal var recipients: [Programmer] = []
    
    func add(recipient: Programmer) {
        recipients.append(recipient)
    }
    
    func send(message: String) {
        for recipient in recipients {
            recipient.receive(message: message)
        }
    }
}

func spamMonster(message: String, worker: MessageMediator) {
    worker.send(message: message)
}

let messagesMediator = MessageMediator()

let user0 = Programmer(name: "Linus Torvalds")
let user1 = Programmer(name: "Avadis 'Avie' Tevanian")
messagesMediator.add(recipient: user0)
messagesMediator.add(recipient: user1)

spamMonster(message: "I'd Like to Add you to My Professional Network", worker: messagesMediator)


// Advanced sample
// https://github.com/kingreza/Swift-Mediator
