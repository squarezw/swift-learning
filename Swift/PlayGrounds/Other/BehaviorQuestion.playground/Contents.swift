//: Playground - noun: a place where people can play

import UIKit

/**
 如果到截止日期没完成任务怎么办”，
 
 “我有什么缺点”
 
 “和同事不同意见怎么办”

 */

/**
 
 Interview Preparation Grid
 
 针对每个 project，列出以下几个方面的相关总结关键词，一定要是特别精炼的短语，不要长篇大论的句子：
 
 
 Challenges
 Mistakes/Failures
 Enjoyed
 Leadership
 Conflicts
 What You'd Do Differently
 
 
 细致准备重点项目
 
 两三个 project 要重点准备，除了上面提到的方面，还需要能够细致介绍：
 
 Technical decisions
 Choices of technologies(tradeoff)
 
 
 常见面试问题
 
 Tell me about yourself
 
 可以按照如下的方式组织准备
 
 Current Role [Headline Only]: I'm a graduate student in Carnegie Mellon University majored in Electrical and Computer Engineering.
 College: I majored in Software Engineering for my undergraduate and had a 6-month internship in Microsoft.
 Current Role [Details]: Learning both the software and the hardware, I know how to write great code on different platforms, especially computer vision and machine learning.
 Outside of Work: Outside of work, I've been working on mobile app development. One of my app named League of Legends Wiki has over 700K downloads with an average rating of 4.5 out of 5.
 Wrap Up: I'm looking for a full time job as I'll get my master degree next year and your company caught my eye. I've always been interested in creating something new so I'd like to talk more with you about the xxx position in your company.
 
 注意见缝插针描述自己的闪光点，让人感兴趣和印象深刻。
 
 What are your weaknesses
 回答技巧：实话实话，不要装逼。点明缺点之后重点强调自己是如何克服的。
 
 个人参考答案：Sometimes, I may lose focus on the whole project while plunge into very detailed problems. It's not bad to spend more time finding the best solution. But it may be better to finish the most critical part first. As it is, I'll draw the whole design on paper and put it just in front of the monitor so that I can easily find out what I should focus on.
 
 
 */


/**
 Strength:
 
 I am very good at planning  我擅长计划
    so my board will look like meticulous
 
 */
