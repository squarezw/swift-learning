//: Playground - noun: a place where people can play

import UIKit

/**
 
 [AAAA]
 ------------------
 
 Swift 源码
 https://github.com/apple/swift


 
 
 [AAA]
 ------------------
 
 中文大而全的刷题笔记
 并包含了大量基础与进阶问题，甚至包括 Behavior 答案，各有大公司对比等
 http:wdxtub.com/interview/14520603230094.html

 
 

 [AA]
 ------------------

 // Cracking the Coding Interview 作者网站，每日更新真实题库
 https://www.careercup.com

 // 大量易读真题，与详细解法，并区分了题目的难度
 http://www.geeksforgeeks.org/

 
 [个人]
 -----------------
 算法研究
 http://m.blog.csdn.net/blog/index?username=nash_ nash
 
 http://taop.marchtea.com/ 程序员编程艺术：面试和算法心得
 
*/

protocol OlderDeathStarSuperLaserAiming {
    var angleV: NSNumber {get}
    var angleH: NSNumber {get}
}

// Adaptee
struct DeathStarSuperlaserTarget {
    let angleHorizontal: Double
    let angleVertical: Double
    
    init(angleHorizontal:Double, angleVertical:Double) {
        self.angleHorizontal = angleHorizontal
        self.angleVertical = angleVertical
    }
}

// Adapter
struct OldDeathStarSuperlaserTarget : OlderDeathStarSuperLaserAiming {
    private let target : DeathStarSuperlaserTarget
    
    var angleV:NSNumber {
        return NSNumber(value: target.angleVertical)
    }
    
    var angleH:NSNumber {
        return NSNumber(value: target.angleHorizontal)
    }
    
    init(_ target:DeathStarSuperlaserTarget) {
        self.target = target
    }
}

let target = DeathStarSuperlaserTarget(angleHorizontal: 14.0, angleVertical: 12.0)
let oldFormat = OldDeathStarSuperlaserTarget(target)


protocol ComponentWithContextualHelp {
    func showHelp()
    func setNext(component: ComponentWithContextualHelp)
}

class Component: ComponentWithContextualHelp {
    var next: ComponentWithContextualHelp?
    
    func setNext(component: ComponentWithContextualHelp) {
        next = component
    }
    
    func showHelp() {
        if let next = next {
            next.showHelp()
        }
    }
}

class Button: Component {
    override func showHelp() {
        print("Button Help")
        super.showHelp()
    }
}

class Pannl: Component {
    override func showHelp() {
        print("Pannl Help")
        super.showHelp()
    }
}

class Color: Component {
    override func showHelp() {
        print("Color Help")
        super.showHelp()
    }
}

let b = Button()
let a = Pannl()

b.next = a

b.showHelp()



let jsonStringTest = """
[
{
"name": "MacBook Pro",
"screen_size": 15,
"cpu_count": 4
},
{
"name": "iMac Pro",
"screen_size": 27,
"cpu_count": 18
}
]
"""

let jsonData = Data(jsonStringTest.utf8)


struct Mac: Codable {
    var name: String
    var screenSize: Int
    var cpuCount: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case screenSize = "screen_size"
        case cpuCount = "cpu_count"
    }
}

let decoder = JSONDecoder()

do {
    let macs = try decoder.decode([Mac].self, from: jsonData)
    
    print(macs.first?.name)
} catch {
    print(error.localizedDescription)
}


