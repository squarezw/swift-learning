//: Playground - noun: a place where people can play

import UIKit
import XCTest

/// Simple Swift dependency injection with functions
/// https://www.swiftbysundell.com/posts/simple-swift-dependency-injection-with-functions


/**
 Dependency injection is a great technique for decoupling code and making it easier to test. Instead of having your objects create their own dependencies on their own, you inject them from the outside, enabling you to set them up differently for various situations — for example in production vs in a test.
 
 Most of the time, we use protocols to enable dependency injection in Swift. For example, let’s say we’re writing a simple card game, where we use a randomizer to draw a random card from a deck, like this:
 */

class Deck {
    let count: UInt32 = 1
    
    subscript (i: UInt32) -> Card {
        return Card(value: .ace, suite: .spades)
    }
    
    init(cards: [Card]) {
        
    }
}
class Card: Equatable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.value == rhs.value && lhs.suite == rhs.suite
    }
    
    enum Value {
        case ace
    }
    enum Suite {
        case spades
    }
    
    let value: Value
    let suite: Suite
    
    init(value: Value, suite: Suite) {
        self.value = value
        self.suite = suite
    }
}

class CardGame {
    private let deck: Deck
    private let randomizer: Randomizer
    
    init(deck: Deck, randomizer: Randomizer = DefaultRandomizer()) {
        self.deck = deck
        self.randomizer = randomizer
    }
    
    func drawRandomCard() -> Card {
        let index = randomizer.randomNumber(upperBound: deck.count)
        let card = deck[index]
        return card
    }
}

/**
 In the above example, you can see that we inject an implementation of Randomizer in CardGame’s initializer, which we then use to generate a random index when drawing. In order to make our API easier to use, we also use a default argument — DefaultRandomizer — if no explicit randomizer is given. Here is what that protocol & default implementation look like:
 */

protocol Randomizer {
    func randomNumber(upperBound: UInt32) -> UInt32
}

class DefaultRandomizer: Randomizer {
    func randomNumber(upperBound: UInt32) -> UInt32 {
        return arc4random_uniform(upperBound)
    }
}

/**
 While protocol based dependency injection is great when our API is more complex, when it only has a single purpose (and only requires a single method), we can reduce our complexity by simply using a function instead.
 
 We can see above that DefaultRandomizer is essentially just a wrapper around arc4random_uniform, so why not just simply pass a function of that type when doing dependency injection instead? Like this:
 */

class CardGame2 {
    typealias Randomizer = (UInt32) -> UInt32
    
    private let deck: Deck
    private let randomizer: Randomizer
    
    init(deck: Deck, randomizer: @escaping Randomizer = arc4random_uniform) {
        self.deck = deck
        self.randomizer = randomizer
    }
    
    func drawRandomCard() -> Card {
        let index = randomizer(deck.count)
        let card = deck[index]
        return card
    }
}

/**
 We have now replaced the Randomizer protocol with a simple typealias, and are passing the arc4random_uniform function directly as the default argument for randomizer. We no longer need a default implementation class, and we can still easily mock the randomizer in a test:
 */

class CardGameTests: XCTestCase {
    func testDrawingRandomCard() {
        var randomizationUpperBound: UInt32?
        
        let deck = Deck(cards: [Card(value: .ace, suite: .spades)])
        
        let game = CardGame2(deck: deck, randomizer: { upperBound in
            // Capture the upper bound to be able to assert it later
            randomizationUpperBound = upperBound
            
            // Return a constant value to remove randomness from
            // our test, making it run consistently.
            return 0
        })
        
        XCTAssertEqual(randomizationUpperBound, 1)
        XCTAssertEqual(game.drawRandomCard(), Card(value: .ace, suite: .spades))
    }
}

print("End up!!!")

let v1 =  UIView()
let v2 =  UIView()
let v3 =  UIView()

let subviews = [v1, v2, v3]
let view = UIView()

subviews.forEach { subview in
    view.addSubview(subview)
}

subviews.forEach(view.addSubview)


