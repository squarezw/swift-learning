//: Playground - noun: a place where people can play

import UIKit

//LCA of a BST

// class 是对象引用, struct 是值 copy。更为安全

class A {
    var name: String = "AA"
    var age: Int = 18
}

struct B {
    var name : String = "BB"
    var age: Int = 18
}

func www(obj: A) {
    let ref = obj   // 即使赋值了另一个对象
    ref.name = "BB"
    ref.age = 19
}

func ttt(obj: B) {
    var obj = obj
    obj.name = "AA"
    obj.age = 19
}

let a = A()
let b = B()
www(obj: a)
ttt(obj: b)

print(a.name, a.age)
print(b.name, b.age)
