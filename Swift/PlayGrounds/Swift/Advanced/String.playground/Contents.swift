//: Playground - noun: a place where people can play

import UIKit

// withUnsafeBufferPointer

// 从 UTF8 CChar 中取字符串打印
let validUTF8: [CChar] = [67, 97, 102, -61, -87, 0]
validUTF8.withUnsafeBufferPointer { ptr in
    let s = String(cString: ptr.baseAddress!)
    print(s)
}

// 统计字符串长度新方法
// old: "This is mine".characters.count
"This is mine".utf8.count