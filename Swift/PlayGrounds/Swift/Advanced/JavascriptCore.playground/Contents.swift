//: Playground - noun: a place where people can play

import UIKit
import JavaScriptCore

let context = JSContext()!
context.evaluateScript("var num = 5 + 5")
context.evaluateScript("var names = ['Grace', 'Ada', 'Margaret']")
context.evaluateScript("var triple = function(value) { return value * 3 }")
let tripleNum = context.evaluateScript("triple(num)")

let names = context.evaluateScript("names")
type(of: names)
type(of: names?.toArray())
names?.toObject()