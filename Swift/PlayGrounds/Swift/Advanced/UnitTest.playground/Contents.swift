import UIKit
import XCTest

// https://www.vadimbulavin.com/unit-testing-async-code-in-swift/

// Mock Asynchronous API

class ImageCompressor {
    
    let queue: DispatchQueue
    
    init(queue: DispatchQueue) {
        self.queue = queue
    }
    
    func compress(_ source: UIImage, completion: @escaping (UIImage) -> Void) {
        queue.async {
            sleep(1) // Simulate the delay
            completion(UIImage())
        }
    }
}


var didCompress = false

let queue = DispatchQueue(label: #function)
let sut = ImageCompressor(queue: queue)
sut.compress(UIImage()) { _ in
    didCompress = true
}

queue.sync {} // The little trick with queue.sync allows us to wait until the operation is finished. I’ve touched on this subject more in Grand Central Dispatch in Swift.

print(didCompress)


// View Controller Transition

class AppRouter {
    let navController: UINavigationController
    
    init(navController: UINavigationController) {
        self.navController = navController
    }
    
    func routeTo(_ next: UIViewController) {
        navController.pushViewController(next, animated: true)
    }
}

class AppRouterTests: XCTestCase {
    
    func testShowsTwoScreens() {
        let navController = UINavigationController()
        let firstScreen = UIViewController()
        let secondScreen = UIViewController()
        
        let sut = AppRouter(navController: navController)
        
        sut.routeTo(firstScreen)
        sut.routeTo(secondScreen)
        
        XCTAssertEqual(navController.viewControllers.count, 2)
        XCTAssertEqual(navController.viewControllers.first, firstScreen)
        XCTAssertEqual(navController.viewControllers.last, secondScreen)
    }
}

// AppRouterTests().testShowsTwoScreens()

/**
 However, this test fails with following log:
 
 XCTAssertEqual failed: ("1") is not equal to ("2")
 XCTAssertEqual failed: ("Optional(<UIViewController: 0x7fe0e4c0b830>)") is not equal to ("Optional(<UIViewController: 0x7fe0e4c0bfb0>)")
 
 View controller transition is performed asynchronously, hence assertions are executed before the navigation has finished. The solution is to mock navigation controller and inject it into AppRouter:
 */

class NonAnimatableNavController: UINavigationController {
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: false) // Disable animation
    }
}

extension AppRouterTests {
    func testShowsTwoScreensNew() {
        let navController = NonAnimatableNavController()
        let firstScreen = UIViewController()
        let secondScreen = UIViewController()
        
        let sut = AppRouter(navController: navController)
        
        sut.routeTo(firstScreen)
        sut.routeTo(secondScreen)
        
        print(navController.viewControllers.count)
        
        XCTAssertEqual(navController.viewControllers.count, 2)
        XCTAssertEqual(navController.viewControllers.first, firstScreen)
        XCTAssertEqual(navController.viewControllers.last, secondScreen)
    }
}

AppRouterTests().testShowsTwoScreensNew()


// Signaling

/**
 Unfortunately, not everything can be mocked. Signaling allows to test async code reliably without mocking. Signaling is implemented by means of XCTestExpectation API, which makes it a trivial task. The technique is summarized into three steps:
 
 1. Create expectation.
 2. Signal expectation when async operation has finished.
 3. Wait for a signal.
 4. Assert.
 */

/// Here is the implementation of MusicService:

struct SearchMediaResponse: Decodable {
    let results: [Track] = []
}

struct Track: Decodable {}

enum APIError: Error {
    case unexpected(Error?)
}

enum Result<Success, Failure> {
    case success(Success)
    case failure(Failure)
}


class MusicService {
    
    func search(_ term: String, completion: @escaping (Result<[Track], Error>) -> Void) {
        var components = URLComponents(string: "https://itunes.apple.com/search")
        components?.queryItems = [
            .init(name: "media", value: "music"),
            .init(name: "entity", value: "song"),
            .init(name: "term", value: "\(term)")
        ]
        
        URLSession.shared.dataTask(with: components!.url!) { data, response, error in
            if let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                let tracks = try? JSONDecoder().decode(SearchMediaResponse.self, from: data).results
                DispatchQueue.main.async {
                    completion(.success(tracks ?? []))
                }
            } else {
                completion(.failure(APIError.unexpected(error)))
            }
            }.resume()
    }
}

class MusicServiceTests: XCTestCase {
    
    func testSearchUsingSignaling() {
        // 1. Create expectation
        let didFinish = self.expectation(description: #function)
        
        let sut = MusicService()
        
        var result: Result<[Track], Error>?
        sut.search("Rock") {
            result = $0
            // 2. Signal
            didFinish.fulfill()
        }
        
        // 3. Wait until the expectation is fulfilled
        wait(for: [didFinish], timeout: 5)
        
        // 4. Assert
        XCTAssertNoThrow(try result?.get())
    }
}


