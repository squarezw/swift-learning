//: Playground - noun: a place where people can play

import UIKit

// ===== Comparable, Equatable

struct Date {
    let year: Int
    let month: Int
    let day: Int
}

extension Date: Comparable {
    static func < (lhs: Date, rhs: Date) -> Bool {
        if lhs.year != rhs.year {
            return lhs.year < rhs.year
        } else if lhs.month != rhs.month {
            return lhs.month < rhs.month
        } else {
            return lhs.day < rhs.day
        }
    }
    
    static func == (lhs: Date, rhs: Date) -> Bool {
        return lhs.year == rhs.year && lhs.month == rhs.month
            && lhs.day == rhs.day
    }
}

let spaceOddity = Date(year: 1969, month: 7, day: 11)   // July 11, 1969
let moonLanding = Date(year: 1969, month: 7, day: 20)   // July 20, 1969
if moonLanding > spaceOddity {
    print("Major Tom stepped through the door first.")
} else {
    print("David Bowie was following in Neil Armstrong's footsteps.")
}


// Implement Hashable Protocols
// 如果想让一个 Point 对象也可以用在 Set 结构上时，可以用 Hashable 协议

//MARK: - Equatable
struct GridPoint {
    var x: Int
    var y: Int
}

extension GridPoint: Hashable {
    var hashValue: Int {
        return x.hashValue ^ y.hashValue
    }

    static func == (lhs: GridPoint, rhs: GridPoint) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}

var tappedPoints: Set = [GridPoint(x: 2, y: 3), GridPoint(x: 4, y: 1)]
let nextTap = GridPoint(x: 0, y: 1)
if tappedPoints.contains(nextTap) {
    print("Already tapped at (\(nextTap.x), \(nextTap.y)).")
} else {
    tappedPoints.insert(nextTap)
    print("New tap detected at (\(nextTap.x), \(nextTap.y)).")
}


// 泛型, 当我们有一个方法，想让入参数满足协议即可。无所谓它是什么可以用 T 或 V 标识泛型
//func openFile(any: T) {} // 这样写就会报错，因为 Hashable 是协议不是具体类
func openFile<T: Hashable>(any: T) {}


// Making it more generic

class Racer {
    var speed: Double = 0
}


func topSpeed(of racers: [Racer]) -> Double {
    return racers.max(by: { $0.speed < $1.speed })?.speed ?? 0
}

let racer1 = Racer()
racer1.speed = 20
let racer2 = Racer()
racer2.speed = 25

let racers = [racer1, racer2, Racer(), Racer()]

topSpeed(of: racers) // 3000

// 如果我们要取一个范围, 就会报错:
// Swift complains it cannot subscript a value of type [Racer] with an index of type CountableClosedRange. Slicing returns one of those wrapper types.
// topSpeed(of: racers[0...2])

// 这时我们就要让 函数更通用

func topSpeed2<RacerType: Sequence>(of racers: RacerType) -> Double where RacerType.Iterator.Element == Racer {
    return racers.max(by: { $0.speed < $1.speed })?.speed ?? 0
}

topSpeed2(of: racers[0...2])

// 我们还可以让他更 Swifty 一些

extension Sequence where Iterator.Element == Racer {
    func topSpeed() -> Double {
        return racers.max(by: { $0.speed < $1.speed })?.speed ?? 0
    }
}

print(racers.topSpeed())
racers[0...2].topSpeed()



