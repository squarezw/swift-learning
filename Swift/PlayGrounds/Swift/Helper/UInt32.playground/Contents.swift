//: Playground - noun: a place where people can play

import UIKit

extension UInt32 {
    
    public func IPv4String() -> String {
        
        let ip = self
        
        let byte4 = UInt8(ip & 0xff)
        let byte3 = UInt8((ip>>8) & 0xff)
        let byte2 = UInt8((ip>>16) & 0xff)
        let byte1 = UInt8((ip>>24) & 0xff)
        
        return "\(byte1).\(byte2).\(byte3).\(byte4)"
    }
}

// factorial
postfix operator *** //声明***是阶乘
postfix func ***(n: Int) -> Int {
    if n == 0 { return 1 }
    return n * (n - 1)***
}

let s: UInt32 = 2896692481
s.IPv4String()

5***

