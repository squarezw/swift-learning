//
//  String.swift
//  Swift
//
//  Created by Jesse on 18/05/2017.
//
//

import Foundation

public extension String {
    
    var length: Int {
        return self.characters.count
    }
    
    subscript (i: Int) -> String {
        return self[Range(i ..< i + 1)]
    }
    
    func substring(from: Int) -> String {
        return self[Range(min(from, length) ..< length)]
    }
    
    func substring(to: Int) -> String {
        return self[Range(0 ..< max(0, to))]
    }
    
    #if swift(>=3.0)
    subscript (r: Range<Int>) -> String {
        let lowerBound = max(0, min(length, r.lowerBound))
        let upperBound = min(length, max(0, r.upperBound))
        let start = index(startIndex, offsetBy: lowerBound)
        let end = index(start, offsetBy: upperBound - lowerBound)
        return self[Range(start ..< end)]
    }
    #else
    subscript (r: Range<Int>) -> String {
    let lowerBound = max(0, min(length, r.startIndex))
    let upperBound = max(0, min(length, r.endIndex))
    let start = startIndex.advancedBy(lowerBound)
    let end = start.advancedBy(upperBound - lowerBound)
    }
    #endif
    
}
