//: Playground - noun: a place where people can play

import UIKit


// stretchable image
// when you need stretch a image, like Wechat message page. you will need to stretch a image.
/*
 当你需要拉伸图片时，注意用代码实现时的一些技巧
 
 1. 把图片放到 Assets 中，选择最小尺寸的那一个 (如果是 1x,2x,3x，选 1x)
 2. 点 start slicing, 选择左右上下。右下方你会得到系统建议的 top, left 参数。
 3. 拿到这两个参数后，在调用 stretchableImage 方法时填入相应的值。
 
 注意 leftCapWidth 的意思是从哪个点开始拉，注意其实就是一个像素点的伸展。topCapHeight 同理。
 */
let iv = UIImageView()
iv.image = UIImage(named: "left_bubble")?.stretchableImage(withLeftCapWidth: 10, topCapHeight: 15)

// resizable image
/*
 上下左右的值定义了受保护区域，能被拉伸的地方是中间区域，一般我们都设成中心点为了安全。
 该参数的意思是被保护的区域到原始图像外轮廓的上部,左部,底部,右部的直线距离
 
 .stretch：拉伸模式，通过拉伸UIEdgeInsets指定的矩形区域来填充图片
 .tile：平铺模式(瓦片)，通过重复显示UIEdgeInsets指定的矩形区域来填充图片
 
 https://www.jianshu.com/p/84848c1b2d47
*/
iv.image?.resizableImage(withCapInsets: UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 15), resizingMode: .stretch)
