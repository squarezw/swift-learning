//: Playground - noun: a place where people can play

import Cocoa

// print 大功用, 可以支持空格为结束位，替换换行
// print(_ items: Any..., separator: String = default, terminator: String = default)
// print(some,  terminator: " ")

// ===1===
// guard

// 使用 if let 解绑optional变量，在if作用域外，解绑的值是不可用的：
func checkAge(ageOfUser:Int?) {
    if let age = ageOfUser, age > 17 {
        // do stuff with age
    }
    // cannot use age anymore
//    print("you age is: \(age), continue.")
}

// 使用 guard let 解绑 optional 变量后，在包含 guard 的作用域内，解绑后的值是可用的：

func checkAge2(ageOfUser: Int?) {
    guard let age = ageOfUser, age > 17 else {
        // fast return
        return
    }
    // do stuff with age
    print("you age is: \(age), continue.")
}


// ===2===
// _ 符号的作用
// 这个符号可以帮你赋一个任何的值，这样你就不必写个等价式去判空

//if let _ = self.property as? NSArray{
//    print("123")
//}


// ===3===
// 参数传递

// 可变参数

func getSum2(_ nums: Int...) -> Int {
    var sum: Int = 0
    
    for num in nums {
        sum += num
    }
    return sum
}

print("Sum: \(getSum2(1,2,3,4,5,1,7))")


// 指针传递

var str1: String = "happy"
var str2: String = "sad"

debugPrint(str2)

func makeUppercase(str1: inout String, str2: inout String){
    str1 = str1.uppercased()
    str2 = str2.uppercased()
}

makeUppercase(str1: &str1, str2: &str2)


// 闭包 Closure 传递

func average(nums: Int...) -> Double{
    var sum = 0
    
    for num in nums {
        sum += num
    }
    
    return Double(sum) / Double(nums.count)
}

func sum(nums: Int...) -> Double{
    var sum = 0
    
    for num in nums {
        sum += num
    }
    
    return Double(sum)
}

func doMath(_ mathOption: String ) -> (Int...) -> Double {
    if mathOption == "average" {
        return average
    } else {
        return sum
    }
}

let mathFunc = doMath("average")

print(mathFunc(1,2,3,4,5,6))


// 任意类型对象

// -----------------------------------
// <..> 用于修饰任意类型对象,比如用 T
// 同时 T 也可以让它遵循某些协议，提供协议提供的功能，
// 并非一定是指定的某个类
// -----------------------------------
func printAnyArray<T>(anything: [T]) {
    for element in anything {
        print("\(element),")
    }
}

printAnyArray(anything: ["1", "2", 77])

func compareThem<T: Equatable>(val1: T, val2: T) -> Bool{
    return val1 == val2
}

compareThem(val1: 1, val2: 2)

// Optional 

// ---------- Optional Binding ---------------
//
// Xcode6 Beta5在这一版中的一个小变化：在Xcode6 Beta5之前，如果是一个Optional值，
// 可以直接放到条件判断语句中，如：
var str: String? = "Hello World!"
//if str {
//    "not nil"
//} else {
//    "nil"
//}


// 通过在条件判断语句中（如if、while等）把Optional值直接给一个临时常量，Swift会
// 自动检测Optional是否包含值，如果包含值，会隐式的拆包并给那个临时常量，在接下
// 来的上下文中就能直接使用这个临时常量了

// ------ Optional Chaining -------
//
// Optional Chaining对Swift来说是很基本但又必不可少的东西，相对于简单类型(Int、String等)来说，Optional更主要的应用场景是在复杂对象上，当一个对象包含另一个对象，同时这两个对象都有可能为nil的情况下才是Optional派上用场的地方，在Objective-C里，向nil发消息得到的就是一个nil，但是Swift不能在nil上直接调用方法或属性，同时为了方便我们使用，从而引入了Optional类型，可是这还不够，我们做一个简单的例子：
class Person {
    var pet: Pet?
}

class Pet {
    var name: String
    
    var favoriteToy: Toy?
    
    init (name: String) {
        self.name = name
    }
    
    func play() {}
}

class Toy {
    var name: String
    
    init (name: String) {
        self.name = name
    }
}

// 一个Person对象代表一个人，这个人可能有一个宠物，宠物会有它自己的名字，而且宠物可能会有自己喜爱的玩具，按照前面提到的知识，我们要首先判断这个人有没有宠物，然后再判断他的宠物有没有喜爱的玩具，然后才能得到这个玩具的名称，利用Optional Binding，我们写出来的可能就像这样：
let jackon = Person()
jackon.pet = Pet(name: "Max")
jackon.pet?.favoriteToy = Toy(name: "Ball")
if let pet = jackon.pet {
    if let toy = pet.favoriteToy {
        toy.name
    }
}

// 这里用到了两个if，因为pet和toy对象都可能为nil，我们需要预防每一个可能为nil的对象，如果这个对象再复杂一点，那if也就更多了，而使用Optional Chaining的话，写出来的就像这样：
jackon.pet = Pet(name: "Max")
jackon.pet?.favoriteToy = Toy(name: "Ball")
if let toy = jackon.pet?.favoriteToy {
    toy.name
}

// 当一个Optional值调用它的另一个Optional值的时候，Optional Chaining就形成了，基本上，Optional Chaining就是总是返回一个Optional的值，只要这个Chaining中有一个值为nil，整条Chaining就为nil，和Objective-C的向nil发消息类似。
// 有一点很有趣，就是Optional Chaining除了能将属性返回的类型变为Optional外，连方法的返回值都能强制变为Optional，哪怕这个方法没有返回值，但是别忘了，Void也算是一个类型：
typealias Void = ()
// 如果我们的Pet类有一个玩玩具的play方法的话，就可以这样来判断是否会调用成功：
if let _: Void = jackon.pet?.play() {
    "play is called"
}
// 使用Optional Chaining，能使我们的代码变得更加可读，同时更加简洁。


// 两种值交换方法

var www: [Int] = [1,2,3,4]

(www[0], www[1]) = (www[2], www[3])

print(www)

www.swapAt(0, 1)

print(www)

// 2³ = 8
pow(2, 3)

// sqrt 平方根
sqrt(6)

struct circle{
    let radius = 10.0
    
    // 圆的周长, 2πr , r 是半径
    func circumference() -> Double {
        let r = 3
        return 2 * Double.pi * Double(r)
    }
    
    /* 任意一点到圆心的距离
        套用二维空间中，两点之间的距离公式
        根号下（|X1-X2|的平方+|Y1-Y2|的平方）
        因为其中一点是圆心，所以 其中的 X2 与 Y2 即是圆上的圆点
    */
    func distance(p: CGPoint) -> Double {
        return sqrt(pow(Double(p.x) - radius, 2) + pow(Double(p.y) - radius, 2))
    }
}

let point = CGPoint(x: 1, y: 1)
circle().distance(p: point)


// 自定义操作符
infix operator >>>> : ATPrecedence
precedencegroup ATPrecedence { //定义运算符优先级ATPrecedence
    associativity: left
    higherThan: AdditionPrecedence
    lowerThan: MultiplicationPrecedence
}

// For Loops in Swift
// https://bjmiller.me/post/137624096422/on-c-style-for-loops-removed-from-swift-3

let items = [1,2,3]
let len = items.count

//INCREMENTING

for i in 0 ..< len {
    print(i)
}

// DECREMENTING

for i in (0..<len).reversed() {
    print(i)
}

// NON-SEQUENTIAL INDEXING
// Using where

for i in (0 ..< len) where i % 2 == 0 {
    print("NON-SEQUENTIAL INDEXING:", i)
}

// Using striding to or through

for i in stride(from: 0, to: len, by: 2) {
    print("stride:", i)
}
