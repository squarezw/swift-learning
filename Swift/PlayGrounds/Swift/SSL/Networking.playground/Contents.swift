
import Cocoa

// Import this to modify Playground setting
import PlaygroundSupport

// Tell playground to keep on running until we click the stop button
PlaygroundPage.current.needsIndefiniteExecution = true

// How to check whether the network is running on the HTTP 2

let url = URL(string: "https://http2.akamai.com")

// URLSession can support HTTP /2.0
let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
    if error == nil, let data = data, let str = String(data: data, encoding: .utf8) {
        if str.contains("You are using HTTP/2 right now!") {
            NSLog("Used HTTP/2")
        } else {
            NSLog("Used HTTP 1.1")
        }
    } else {
        // error...
    }
    
    // stop the playground execution
    PlaygroundPage.current.finishExecution()
}

task.resume()

// But the old Connection can't do that
let request = URLRequest(url: url!)
NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler: { (res, data, error) in
    if error == nil, let data = data, let str = String(data: data, encoding: .utf8) {
        if str.contains("You are using HTTP/2 right now!") {
            print("Used HTTP/2")
        } else {
            print("Used HTTP 1.1")
        }
    } else {
        // error...
    }
})

// URL Components
let baseURL = URL(string:"http://example.com/v1/")

URL(string: "foo", relativeTo: baseURL)
// http://example.com/v1/foo

URL(string:"foo?bar=baz", relativeTo: baseURL)
// http://example.com/v1/foo?bar=baz

URL(string:"/foo", relativeTo: baseURL)
// http://example.com/foo

URL(string:"foo/", relativeTo: baseURL)
// http://example.com/v1/foo/

URL(string:"/foo/", relativeTo: baseURL)
// http://example.com/foo/

URL(string:"http://example2.com/", relativeTo: baseURL)
// http://example2.com/
