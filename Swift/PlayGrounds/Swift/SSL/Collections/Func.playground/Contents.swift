//: Playground - noun: a place where people can play

import Cocoa


/// zip(_:_:)
/// Creates a sequence of pairs built out of two underlying sequences.

/// 动态匹配两组数据，组成一个新的 couple
let words = ["one", "two", "three", "four"]
let numbers = 1...4

for (word, number) in zip(words, numbers) {
    print("\(word): \(number)")
}

/// Sample 2
let names: Set = ["Sofia", "Camilla", "Martina", "Mateo", "Nicolás"]
var shorterIndices: [SetIndex<String>] = []

print(names.indices)

for (i, name) in zip(names.indices, names) {
    if name.count <= 5 {
        shorterIndices.append(i)
    }
}

for i in shorterIndices {
    print(names[i])
}


