//: Thread
//: Swift 3 创建线程的方式只有一种了 (Pthreads 与 NSTread 方式已不支持)，在 OC 中有三种

//: http://www.jianshu.com/p/0b0d9b1f1f19

import Cocoa

DispatchQueue.main.async {
    print("这里在主线程执行")
}

/*
 优先级
 
 DISPATCH_QUEUE_PRIORITY_HIGH: .userInitiated
 DISPATCH_QUEUE_PRIORITY_DEFAULT: .default
 DISPATCH_QUEUE_PRIORITY_LOW: .utility
 DISPATCH_QUEUE_PRIORITY_BACKGROUND: .background
 
 */

//global 中设置优先级 不设置默认是 default
DispatchQueue.global(qos: .userInitiated).async {
    print("设置优先级")
}

// 创建一个队列

let queue = DispatchQueue(label: "im.demo.test")

// 也可以指定优先级和队列

let highQueue = DispatchQueue(
    label: "high.demo.test.queue",
    qos: DispatchQoS.background,
    attributes: DispatchQueue.Attributes.concurrent,
    autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit,
    target: nil
)

// 也可以指定需要的属性

let nomarlQueue = DispatchQueue(label: "myqueue.test", attributes: DispatchQueue.Attributes.concurrent)

highQueue.async {
    print("ceshi", Thread.current)
}

nomarlQueue.sync {
    print("Hi", Thread.current)
}

// 3秒后执行

var finished = false

DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
    print("after")
    finished = true
}

repeat {
    RunLoop.current.run(mode: .default, before:NSDate.distantFuture)
} while !finished

print("is finished!")


// Dispatch Once Helper

public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    class func once(token: String, block: ()->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

private let _onceToken = NSUUID().uuidString

DispatchQueue.once(token: _onceToken) {
    print("Do This once!")
}

let dispatchGroup = DispatchGroup()
let dispatchQueue = DispatchQueue(label: "queue", attributes: .concurrent)

func asyncProcess(number: Int, completion: (_ number: Int) -> Void) {
    print("#\(number) Start")
    sleep((arc4random() % 100 + 1) / 100)
    completion(number)
}

for i in 1...3 {
    dispatchGroup.enter()
    dispatchQueue.async(group: dispatchGroup) {
        asyncProcess(number: i, completion: { (number) in
            print("\(number) End")
            dispatchGroup.leave()
        })
    }
}

dispatchGroup.notify(queue: .main) {
    print("All Process Done!")
}


let semaphore = DispatchSemaphore(value: 1)

DispatchQueue.global().async {
    print("Kid 1 - wait")
    semaphore.wait()
    print("Kid 1 - wait finished")
    sleep(1) // Kid 1 playing with iPad
    semaphore.signal()
    print("Kid 1 - done with iPad")
}

print("...")

// 除了 Dispatch 外，Operation 提供了更多可为控制的 API。
// 除了 cancel, resume, wait 外，还可以知道有多少 operation 在里面
// 看看下面会不会发生死锁

let da = BlockOperation {
    print("Hi Da.")
}
let db = BlockOperation {
    print("Hi Db.")
}
db.addDependency(da)

let dq = OperationQueue()
dq.maxConcurrentOperationCount = 1

dq.addOperation(db)
dq.addOperation(da)

// Debounce
func debounce(interval: Int, queue: DispatchQueue, action: @escaping (() -> Void)) -> () -> Void {
    var lastFireTime = DispatchTime.now()
    let dispatchDelay = DispatchTimeInterval.milliseconds(interval)

    return {
        lastFireTime = DispatchTime.now()
        let dispatchTime: DispatchTime = DispatchTime.now() + dispatchDelay

        queue.asyncAfter(deadline: dispatchTime) {
            let when: DispatchTime = lastFireTime + dispatchDelay
            let now = DispatchTime.now()
            if now.rawValue >= when.rawValue {
                action()
            }
        }
    }
}

let debouncedFunction = debounce(interval: 2000, queue: DispatchQueue.main) {
    print("called:")
};

DispatchQueue.global(qos: .background).async {
    print("第一个", DispatchTime.now())
    debouncedFunction()
    usleep(100 * 1000)
    print("第二个", DispatchTime.now())
    debouncedFunction()
//    usleep(100 * 1000)
    debouncedFunction()
//    usleep(100 * 1000)
    debouncedFunction()
//    usleep(100 * 1000)

//    debouncedFunction("4")
//    usleep(300 * 1000) // waiting a bit longer than the interval
//    debouncedFunction("5")
//    usleep(100 * 1000)
//    debouncedFunction("6")
//    usleep(100 * 1000)
//    debouncedFunction("7")
//    usleep(300 * 1000) // waiting a bit longer than the interval
//    debouncedFunction("8")
//    usleep(100 * 1000)
//    debouncedFunction("9")
//    usleep(100 * 1000)
//    debouncedFunction("10")
//    usleep(100 * 1000)
//    debouncedFunction("11")
//    usleep(100 * 1000)
//    debouncedFunction("12")
}
