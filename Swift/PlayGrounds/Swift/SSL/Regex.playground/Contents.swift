//: Playground - noun: a place where people can play

import UIKit

/**
 
"^(\\[[^\\[\\]]*\\]){4}$"
    ││├─┘├───────┘│├─┘ │  └─ the end of the line.
        │││  │        ││   └─ repeat the capturing group four times.
│││  │        │└─ a literal "]"
│││  │        └─ the previous character class zero or more times.
│││  └─ a character class containing anything but the literals "[" and "]"
││└─ a literal "[".
│└─ start a capturing group.
└─ the beginning of the string.
 
 */

// 匹配括弧里的内容 ab(cd)ef -> cd
// (\\((.*?)\\))