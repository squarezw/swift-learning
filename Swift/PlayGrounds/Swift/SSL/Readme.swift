// SSL: Swift Standard Library

//: Swift 专业名词解释
//:
//: http://wiki.jikexueyuan.com/project/swift/chapter2/01_The_Basics.html
//:

/**
 专业名词
 ========
 abstraction 抽像
 
 consecutive 连续的
 
 iterative 迭代
    比如循环
 
 inheritance 继承
 
 literal 字母
 
 odd 奇数
 even 偶数
 
 encapsulation  封装
 
 parenthese 括号
    squiggle    花括号 {}
    bracket     中括号 []
    paren       括号 ()
 
 polymorphism   多态
 
 recursive      递归
 rectangular    矩形
 
 vector 矢量
   Vector2D 坐标矢量，也称坐标系
 
 公式
 ========
 
 1. log 对数
 
 3的4次方 = 3 x 3 x 3 x 3 = 81
 
 4 = log 81
 3
 
 */


/**
 关键字和标点符号
 
 下面这些被保留的关键字不允许用作标识符，除非使用反引号转义，具体描述请参考 标识符。除了 inout、var 以及 let 之外的关键字可以用作某个函数声明或者函数调用当中的外部参数名，不用添加反引号转义。
 
 用在声明中的关键字： associatedtype、class、deinit、enum、extension、func、import、init、inout、internal、let、operator、private、protocol、public、static、struct、subscript、typealias 以及 var。
 用在语句中的关键字：break、case、continue、default、defer、do、else、fallthrough、for、guard、if、in、repeat、return、switch、where 以及 while。
 用在表达式和类型中的关键字：as、catch、dynamicType、false、is、nil、rethrows、super、self、Self、throw、throws、true、try、#column、#file、#function 以及 #line。
 用在模式中的关键字：_。
 以井字号 (#) 开头的关键字：#available、#column、#else#elseif、#endif、#file、#function、#if、#line 以及 #selector。
 特定上下文中被保留的关键字： associativity、convenience、dynamic、didSet、final、get、infix、indirect、lazy、left、mutating、none、nonmutating、optional、override、postfix、precedence、prefix、Protocol、required、right、set、Type、unowned、weak 以及 willSet。这些关键字在特定上下文之外可以被用做标识符。
 以下符号被当作保留符号，不能用于自定义运算符： (、)、{、}、[、]、.、,、:、;、=、@、#、&（作为前缀运算符）、->、`、?、!（作为后缀运算符）。
 */



// Compile
// 编译为 swift 为 SIL 中间文件
// SIL is an intermediate language used by the Swift compiler, filling the gap between the AST and the LLVM IR
//
// swiftc -emit-sil Test.swift
