//: 正则式
//:
//: https://zh.wikipedia.org/wiki/%E6%AD%A3%E5%88%99%E8%A1%A8%E8%BE%BE%E5%BC%8F


/**
 零宽断言	
 (?=exp)	匹配exp前面的位置
 (?<=exp)	匹配exp后面的位置
 (?!exp)	匹配后面跟的不是exp的位置
 (?<!exp)	匹配前面不是exp的位置
 
 比如 
 (?=5) 表示匹配 5 前面的内容
 (?<=5) 表示匹配 5 后面的内容
 
 */


import UIKit


func matches(for regex: String, in text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex)
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

// 正则式
var str = "(12144e)eeee"

// 匹配括号里的内容 (?<=\\()(.*)(?=\\))
matches(for: "(?<=\\()([\\d+])*", in: str)

str = "Chapter 19 213213"

matches(for: "Chapter [1-9][0-9]?", in: str)


/// Class representing ICU regular expression for use as StringLiteralConvertible
public final class Regex {
    let pattern: String
    let options: NSRegularExpression.Options!
    
    /**
     Default initializer
     
     :param: pattern The regular expression to compile of String type
     :param: options The regular expression options that are applied to the expression during matching. Default value is nil.
     :returns: An instance of Regex class
     */
    public required init(pattern: String, options: NSRegularExpression.Options = []) {
        self.pattern = pattern
        self.options = options
    }
    
    var matcher: NSRegularExpression? {
        do {
            let regex = try NSRegularExpression(pattern: self.pattern, options: [])
            return regex
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return nil
        }
    }
    
    func match(string: String, options: NSRegularExpression.MatchingOptions = .withoutAnchoringBounds) -> Bool {
        return self.matcher?.numberOfMatches(in: string, options: options, range: NSMakeRange(0, string.utf16.count)) != 0
    }
    
}

extension Regex: StringLiteralConvertible {
    public typealias UnicodeScalarLiteralType = StringLiteralType
    public typealias ExtendedGraphemeClusterLiteralType = StringLiteralType
    
    public convenience init(unicodeScalarLiteral value: UnicodeScalarLiteralType) {
        self.init(pattern: value)
    }
    
    public convenience init(extendedGraphemeClusterLiteral value: ExtendedGraphemeClusterLiteralType) {
        self.init(pattern: value)
    }
    
    public convenience init(stringLiteral value: StringLiteralType) {
        self.init(pattern: value)
    }
}

// MARK: -

public protocol RegularExpressionMatchable {
    func match(regex: Regex) -> Bool
}

/// Operator for String matching String on the left side to the Regex (String representation of the regex pattern) on the right side
infix operator =~ { associativity left precedence 130 }
public func =~<T: RegularExpressionMatchable> (left: T, right: Regex) -> Bool {
    return left.match(regex: right)
}

/// Switch pattern match operator for Regex instances
public func ~=<T: RegularExpressionMatchable>(pattern: Regex, matchable: T) -> Bool {
    return matchable.match(regex: pattern)
}

extension String: RegularExpressionMatchable {
    public func match(regex: Regex) -> Bool {
        return regex.match(string: self)
    }
}


"11122255a".match(regex: Regex(pattern: "\\d{8,}"))

// 正则匹配

let rawInput = "126 a.b 22219 zzzzzz"
let numbericPrefix = rawInput.prefix(while: { "0"..."1" ~= $0 })
numbericPrefix
