//: Playground - noun: a place where people can play

import Cocoa

// 自定义 Error
struct ZError : Error {
    let domain: String
    let code: Int
}

func canThrow() throws{
    let age = 10
    if age < 18{
        let error = ZError(domain: "xxx", code: 990)
        throw error
    }
}

do {
    try canThrow()
} catch let error as ZError {
    print("Error: \(error.code) - \(error.domain)") // Error: 990 - xxx
}




//在程序release后, 断言会失效.
assert(1>0)
assert(1>0, "Error02")

//assertionFailure("Error03")

precondition(1>0)
precondition(1>0, "Error")

//fatalError("Error")


// 之前使用可选型表示错误 - 错误时返回空
var a = Int("2")
var b = Int("Hello, Swift")
// 但空不一定是错误
"Hello, Swift".range(of: "He")
// 更符合语意的表示：使用Swift的Error Handling机制
// 比如文件读取; 网络连接 等等等等
class VendingMachine{

    struct Item{
        
        enum VMType: String{
            case Water
            case Cola
            case Juice
        }
        
        let type:  VMType
        let price: Int
        var count: Int
    }
    
    //定义我们自己的错误.
    enum VMErr: Error, CustomStringConvertible{
        case NoSuchItem
        case NotEnoughMoney(Int)
        case OutOfStock
        
        var description: String{
            switch self {
            case .NoSuchItem:                return "Not Such Item"
            case .NotEnoughMoney(let price): return "Not Enough Money. " + String(price) + " Yuan needed."
            case .OutOfStock:                return "Out of Stock"
            }
        }
    }
    
    private var items = [ "Mineral Water" : Item(type: .Water, price:2, count:10),
                          "Coca Cola" : Item(type: .Cola, price: 3, count: 5),
                          "Orange Juice" : Item(type: .Juice, price: 5, count: 3)]
    
    //售卖 => 可能抛出异常
    func vend(itemName: String, money: Int) throws -> Int{
        
        //延时处理代码块
        defer{ //返回和发生异常后调用.
            print("Have a nice day")
        }
        
        guard let item = items[itemName] else{
            throw VMErr.NoSuchItem
        }
        
        guard money >= item.price else{
            throw VMErr.NotEnoughMoney(item.price)
        }
        
        guard item.count > 0 else{
            throw VendingMachine.VMErr.OutOfStock
        }
        
        defer{ //defer是按倒序执行的.
            print("Thank you")
        }
        
        dispenseItem(itemName: itemName)
        
        return money - item.price
    }
    
    private func dispenseItem(itemName: String){
        items[itemName]!.count -= 1
        print("Enjoy your",itemName)
    }
    
    func display(){
        print("Want something to drink?")
        for itemName in items.keys{
            print( "*" , itemName , "count=", items[itemName]?.count ?? "nil")
        }
        print("=============================")
    }
}

let machine = VendingMachine()
machine.display()

var pocketMoney = 3
//1. 强制处理异常 try!
//try! machine.vend(itemName: "Coca Cola", money: pocketMoney) //返回 0
//2. try? 如果发生异常就返回nil
//try? machine.vend(itemName: "Coca Cola", money: 0) //返回 nil
//3. 类型可选型解包的使用方式
if let _ = try? machine.vend(itemName: "Coca Cola", money: pocketMoney) {
    //解包成功
}
else {
    //Error Handling
}

//4. 正常的异常处理逻辑 do ... catch
do{
    pocketMoney = try machine.vend(itemName: "Coca Cola", money: pocketMoney)
    print(pocketMoney, "Yuan Left")
}
catch VendingMachine.VMErr.NoSuchItem{
    print("No Such Item")
}
catch VendingMachine.VMErr.NotEnoughMoney(let price){
    print("Not Enough Money", price, "Yuan needed.")
}
catch VendingMachine.VMErr.OutOfStock{
    print("Out of Stock")
}
catch{
    print("Error occured during vending.")
}

//5. 复杂的异常处理场景下, 判断合并处理一类异常
do{
    pocketMoney = try machine.vend(itemName: "Coca Cola", money: pocketMoney)
    print(pocketMoney, "Yuan Left")
}
catch let error as VendingMachine.VMErr{ //判断产生的异常是否是Vendingmanchine.Error类型.
    print(error)
}
catch{
    print("Error occured during vending.")
}

/*
 defer 用在退出某个scope必须处理某些事情的时候
 最常见的使用场景：文件处理
 
 defer不一定必须用在Error Handling的情况下
 但是在处理错误的时候使用 defer 是最常见的情况
 
 defer本质是一种转移控制, 和 break, continue 一样
 */


/// 最有用的是以下场景，当你想在返回一个值后，对这个值再处理。想想以前如何实现？
struct Example {
    private var k: Int = 0
    
    mutating func test() -> Int {
        defer {
            k += 1
        }
        return k
    }
}


var ex = Example()
ex.test()
ex.test()


///
/// How Never Works Internally in Swift
///

func getSomeNumber() -> Int {
    // print file, function, line
    print(#file, #function, #line, #column)
    fatalError()
    // i'm not returning an Int, but this still compiles
    // because fatalError() return 'Never'.
}
