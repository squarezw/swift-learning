//: Playground - noun: a place where people can play

import Cocoa

/**
 * 继承
 */

//角色类
class Avatar{
    let name: String //名字
    var life = 100{ //生命值 //属性观察器.
        didSet{
            if self.life <= 0 {
                self.isAlive = false
            }
            if life > 100 {
                life = 100
            }
        }
    }
    var isAlive: Bool = true //是否活着
    var description: String { //计算型属性
        return "I'm Avatar \(name)."
    }
    
    required init(name: String){
        self.name = name
    }
    
    convenience init(firstName: String, lastName: String){
        self.init(name: firstName + " " + lastName)
    }
    
    //被攻击, 生命值减attack
    func beAttached(attack: Int){
        life -= attack
        if life <=  0{
            isAlive = false
        }
    }
}

//User用户角色 继承 Avatar
class User: Avatar{
    var score = 0 //分数
    var level = 0 //等级
    override var description: String{ //属性重载
        return "I'm User \(name)."
    }
    var group: String
    
    init(name: String, group: String){
        //第一段构造初值
        self.group = group //子类的属性必须先被初始化, 父类的属性才能被初始化. *****
        //self.name = name //error 父类的属性必须通过父类的构造函数.
        super.init(name: name) //必须通过父类的构造函数初始化父类的属性.
        
        //第二段构造 进一步完善属性的初始逻辑.
        if group.isEmpty {
            self.getScore(score: -10) //如果没有设定加入的组名, 统一给User减10分.
        }
    }
    
    //convenience在构造函数中通过调用自己的构造函数来完成构造.
    convenience init(group: String){
        //第一阶段 可以使用 静态方法和静态属性
        let name = User.generateUserName()
        //self.group = group //error 此时还不能使用self, Swift认为属性初始化是在self.init中完成的, 所以还不能使用self的.
        self.init(name: name, group: group) //必须加self.才能调用自己的构造函数. ****
    }
    
    //convenience 和 override 关键字顺序可以随意
    //当父类的init(name)加上required标示是必须实现的构造函数时, 此时override就没有必要了, 但是required还是必须写上.
    /*override*/ required convenience init(name: String){
        self.init(name: name, group: "")
    }
    
    //这个方法不能被子类override覆盖的.
    final func getScore(score: Int){ //加分
        self.score += score
        if self.score > level * 100{
            level += 1
        }
    }
    
    static func generateUserName() -> String{ //静态方法动态生成用户名.
        return "Player" + String(Int(arc4random())%1_000_000)
    }
}

//魔法师角色.
//final 最终类, 不能再被继承了
final class Magician: User{
    var magic = 100 //魔法值
    override var description: String{
        return "I'm Magician \(name)."
    }
    
    //重载父类的构造函数override
    override init(name: String, group: String) {
        //注: 第一段构造中不可以使用self相关的属性和方法
        let defaultGroups = ["Gryffindor", "Hufflepuff", "Ravenclaw", "Slytherin"]
        for theGroup in defaultGroups{ //限定魔法师只能所属于这四个group.
            if group == theGroup{
                super.init(name: name, group: group)
                return
            }
        }
        
        let group = defaultGroups[Int(arc4random())%4] //随机指定四个组中的任意一个.
        super.init(name: name, group: group)
    }
    
    func heal(user: User){
        user.life += 10
    }
}

//战士类
final class Warrior: User{
    static let weapons = ["Sword", "Axe", "Spear"]
    var weapon: String //武器[可选]
    override var description: String{
        return "I'm Warrior \(name)."
    }
    
    //给构造函数的参数加默认值, 这样Warrior就有多种构造函数了.
    init(name: String, group: String, weapon: String = "Sword"){
        self.weapon = weapon
        super.init(name: name, group: group)
    }
    
    //override和convenience同时使用, 即覆写了父类的构造函数, 同时通过自身现有的构造函数来方便的完成初始化.
    convenience override init(name: String, group: String) {
        let weapon = Warrior.weapons[ Int(arc4random()) % Warrior.weapons.count ]
        self.init(name: name, group: group, weapon: weapon)
    }
    
    //重载父类的构造函数.
    //    override init(name: String, group: String) {
    //        self.weapon = Warrior.weapons[ random() % Warrior.weapons.count ]
    //        super.init(name: name, group: group)
    //    }
    
    //覆写父类方法, 当战士的受攻击时, 生命值只减的一半攻击量.
    override func beAttached(attack: Int) {
        self.life -= attack/2
    }
}

//巨兽 怪物
class Monster: Avatar{
    override var description: String{ //属性重载.
        return "I'm Monster \(name)."
    }
    
    //如果Monster没有自己的构造函数, 则自动继承了父类的构造函数
    //所以当convenience init构造时, self.init(name)实际上是把继承于父类的构造函数当作自己的构造函数,
    //从而满足了convenience关键字.
    //当init(name)构造函数加上required时, Monster虽然没有实现init(name), 但是它是通过继承实现了init(name),
    //所以此时的self.init(name)其实是来自父类的required init(name)构造函数.
    convenience init(type: String){
        self.init(name: type)
    }
    
    //攻击用户
    func attack(user: User, amount: Int){
        user.beAttached(attack: amount)
    }
}

//僵尸
final class Zombie: Monster{
    var type = "Default"
    override var description: String{
        return "I'm Zombie \(name)."
    }
}

class NPC: Avatar{
    let career: String
    
    //由于NPC写了自己的构造函数, 所以NPC就不能继承父类的构造函数了, 就必须有实现required init(name)构造函数.
    init(name: String, career: String){
        self.career = career
        super.init(name: name)
    }
    
    //在这里NPC写了属于自己的构造函数, 对于required的构造函数就不能通过继承来实现了, 就必须显示写出来.
    required convenience init(name: String) {
        //fatalError("init(name:) has not been implemented")
        self.init(name: name, career: "")
    }
}

let group: String = "Group"
let player = User(name: "AAA", group: group)
player.name
player.isAlive
player.life
player.score

player.beAttached(attack: 20)
player.life

player.getScore(score: 20)
player.score

let mg = Magician(name: "Harry Portter", group: group)
mg.name
mg.life
mg.isAlive
mg.score
mg.level
mg.magic

//多态
let magician = Magician(name: "Harry Potter", group: group)
let warrior = Warrior(name: "kangqiao", group: group, weapon: "broadsword")

let zombie = Zombie(name: "default zombie1")
let monster = Monster(name: "monster")

func printBasicInfo(avatar: Avatar){
    print("The avatar's name is \(avatar.name)")
    print("The life is \(avatar.life). He is \( (avatar.isAlive) ? "still alive" : "dead")")
    print("========")
}

printBasicInfo(avatar: magician)
printBasicInfo(avatar: warrior)
printBasicInfo(avatar: zombie)
printBasicInfo(avatar: monster)

let avatarArr: [Avatar] = [magician, warrior, zombie, monster]
for avatar in avatarArr{
    avatar.life -= 10
    print(avatar.description)
}

magician.heal(user: magician)
magician.heal(user: warrior)

//重载 => 属性重载, 方法重载.
warrior.life //100
monster.attack(user: warrior, amount: 20)
warrior.life //90 攻击减半.
//两段式构造.
//Swift把构造函数分为两段
//第一段 主要是对没有初值的属性进行初始化,
//      注: 必须先初始化自己没有初值的属性, 然后必须通过super.init()去初始化父类的没有初值的属性.
//          这一步不能使用self相关的属性和方法
//第二段 进一步完善相关属性的值.
//      注: 当子类和父类没有初始化的属性值都初始化后, 这一步才算初始化完成, self可以使用,
//          相关的方法可以调用, 改变其他属性的相关值.
// 便利构造函数和指定构造函数
// 构造函数参数可以有默认值.
// 构造函数是可以被重载的.
/***
 * 构造函数的继承
 */
//用父类Avatar的convenience构造函数初始化User, 同时User也重载了init(name), 所以最终调用的是User子类的init(name)构造函数.
let user = User(firstName: "qiao", lastName: "kang")
user
let monster2 = Monster(name: "zombie")

/***
 * required构造函数.
 *     有些时候在我们设计一个类的时候, 我们知道这个类将来会是一个父类时, 我们认为某些构造函数是非常有用的,
 *     并希望继承它的所有的子类都必须实现这些构造函数, 对此, 我们只需要在父类上把这些构造函数声明成required
 *     当子类实现时, 如果没有写属于自己的构造函数, 那么它是继承了父类的构造函数, 包括required的构造函数.
 *                 如果写了属于自己的构造函数, 子类就必须显示的去实现required的构造函数, 就不能再继承而来了.
 *                 反之如果父类构造函数没有required, 子类是可以通过继承而来的, 同时也可以override重载父类的构造函数.
 *     在子类的实现中 required和override是互斥的, 不能同时存在.
 */

/**
 简单解读
 
 convenience
        当类里有多个 init 方法。并且这几个方法里有一些调用对方，则其调用者应该申明为 convenience .  
        init(name: String) {   // <-- 这前面就必须加上 convenience，因为构造函数里有 self.init
            self.init()
        }
 
 required
        当类里有一个方法申明为 required 。则子类不能覆盖它，而要实现同样的 required 方法
        required init(name: String) {  // <-- 假设父类申明了 required 同样的方法
 
        }
 
        另外，如果子类里实现了不同的 init 方法，则必须自己实现 require 方法。因为 required 是强制必须实现的
 
        required init(name: String) {
            super.init(name: name)
        }
 */

//在结构体中构造函数 重拾
struct Point{
    var x = 0.0
    var y = 0.0
}
struct Size{
    var width = 0.0
    var height = 0.0
}

struct Rectangle2{
    var origin = Point()
    var size = Size()
    
    var center: Point{
        get{
            let centerX = origin.x + size.width/2
            let centerY = origin.y + size.height/2
            return Point(x: centerX, y: centerY)
        }
        
        set{
            origin.x = newValue.x - size.width/2
            origin.y = newValue.y - size.height/2
        }
    }
    
    init(origin: Point, size: Size){
        self.origin = origin
        self.size = size
    }
    
    //由于结构体没有继承关系, 所以这里可以直接用self.init去初始化, 不需要convenience
    init(center: Point, size: Size){
        let originX = center.x - size.width/2
        let originY = center.y - size.height/2
        self.init(origin: Point(x: originX, y: originY), size: size)
    }
    
    var area: Double{
        return size.width * size.height
    }
}

// MetaType
// https://swiftrocks.com/whats-type-and-self-swift-metatypes.html

struct SwiftRocks {
    static let author = "Bruno Rocha"
    func postArticle(name: String) {}
}

let blog: SwiftRocks = SwiftRocks()
SwiftRocks.author

/**
 You can say that SwiftRocks() is an object and SwiftRocks is its type, but instead, try seeing SwiftRocks() as an instance, and : SwiftRocks itself as the representation of the type of an instance. After all, you can call the instance method postArticle() from blog, but you can't access the class property author.
 
 Now, how can we access author? The most common way would be through SwiftRocks.author which will directly return you a String, but I will ask you to forget about that one for a moment. Is there another way?
 
 I know that Bruno! You can call type(of: blog).author!
 
 Yup! That is also correct, as type(of) transforms something an object into something that allows you to access all class properties. But have you ever tried to call just type(of: blog) to see what would happen?
 */
type(of: blog).author

// blog.author <- wrong access

let ww: SwiftRocks.Type = type(of: blog)
let instance: SwiftRocks = ww.init()

//

class BlogPost {
    required init() {
        
    }
}

func createWidget<T: BlogPost>(ofType: T.Type) -> T {
    let widget = T.init()
    print(widget)
    return widget
}

createWidget(ofType: BlogPost.self)

class TutorialBlogPost: BlogPost {}
class ArticleBlogPost: BlogPost {}
class TipBlogPost: BlogPost {}

func create<T: BlogPost>(blogType: T.Type) -> T {
    switch blogType {
    case is TutorialBlogPost.Type:
//        return blogType.init(subject: currentSubject)
        return blogType.init()
    case is ArticleBlogPost.Type:
//        return blogType.init(subject: getLatestFeatures().random())
        return blogType.init()
    case is TipBlogPost.Type:
//        return blogType.init(subject: getKnowledge().random())
        return blogType.init()
    default:
        fatalError("Unknown blog kind!")
    }
}


/**
 type(of:) Dynamic Metatypes vs .self Static Metatypes
 So type(of) returns the metatype of an object, but what happens if I don't have an object? Xcode gives me a compiler error if I try to call create(blogType: TutorialBlogPost.Type)!
 
 To make it short, the reason you can't do that is the same reason why you can't call myArray.append(String): String is the name of the type, not the value! To get a metatype as a value, you need to type the name of that type followed by .self.
 
 If that sounds confusing, you can see it like this: Just like String is the type and "Hello World" is the value of an instance, String.Type is the type and String.self is the value of a metatype.
 */
protocol MyProtocol {}
struct MyType: MyProtocol {}
let metatype: MyProtocol.Type = MyType.self // Now works!

let protMetatype: MyProtocol.Protocol = MyProtocol.self


// More uses for Metatypes

public protocol DeepLink {
    static var identifier: String {get}
}

public protocol DeepLinkHandler: class {
    var handledDeepLinks: [DeepLink.Type] { get }
    func canHandle(deepLink: DeepLink) -> Bool
    func handle(deepLink: DeepLink)
}

public extension DeepLinkHandler {
    func canHandle(deepLink: DeepLink) -> Bool {
        let deepLinkType = type(of: deepLink)
        //Unfortunately, metatypes can't be added to Sets as they don't conform to Hashable!
        return handledDeepLinks.contains { $0.identifier == deepLinkType.identifier }
    }
}

class MyClass: DeepLinkHandler {
    var handledDeepLinks: [DeepLink.Type] {
        return [HomeDeepLink.self, PurchaseDeepLink.self]
    }
    
    func handle(deepLink: DeepLink) {
        switch deepLink {
        case let deepLink as HomeDeepLink:
        print(deepLink)
        case let deepLink as PurchaseDeepLink:
        print(deepLink)
        default:
            fatalError()
        }
    }
}

class HomeDeepLink: DeepLink {
    static var identifier: String = UUID().uuidString
}

class PurchaseDeepLink: DeepLink {
    static var identifier: String = UUID().uuidString
}

class FakeLink: DeepLink {
    static var identifier: String = UUID().uuidString
}

MyClass().canHandle(deepLink: FakeLink())
MyClass().canHandle(deepLink: PurchaseDeepLink())

enum CompassDirection: CaseIterable {
    case north, south, east, west
}

let caseList = CompassDirection.allCases.map({"\($0)"}).joined(separator: ", ")
print(caseList)


// dynamicMemberLookup 可以让类调用任何实例方法都可以返回内容

@dynamicMemberLookup
class Sandwich {
    subscript(dynamicMember member: String) -> String {
        return "I'm a sandwich!"
    }
}

class HotDog: Sandwich { }

let chiliDog = HotDog()
print(chiliDog.description)
print(chiliDog.xxxxxx)

@dynamicMemberLookup
protocol Subscripting { }

extension Subscripting {
    subscript(dynamicMember member: String) -> String {
        return "This is coming from the subscript"
    }
}

extension String: Subscripting { }
let str = "Hello, Swift"
print(str.username)


@dynamicMemberLookup
enum JSON {
    case intValue(Int)
    case stringValue(String)
    case arrayValue(Array<JSON>)
    case dictionaryValue(Dictionary<String, JSON>)
    
    var stringValue: String? {
        if case .stringValue(let str) = self {
            return str
        }
        return nil
    }
    
    subscript(index: Int) -> JSON? {
        if case .arrayValue(let arr) = self {
            return index < arr.count ? arr[index] : nil
        }
        return nil
    }
    
    subscript(key: String) -> JSON? {
        if case .dictionaryValue(let dict) = self {
            return dict[key]
        }
        return nil
    }
    
    subscript(dynamicMember member: String) -> JSON? {
        if case .dictionaryValue(let dict) = self {
            return dict[member]
        }
        return nil
    }
}

let json = JSON.stringValue("Example")
json[0]?["name"]?["first"]?.stringValue
json[0]?.name?.first?.stringValue


