//: Playground - noun: a place where people can play

import Cocoa

Int.max

Int16.max

Int32.max

Int64.max

UInt16.max

let binaryInt: Int = 0b10001 //二进制的Int数据, 以 0b 开头
let octalInt: Int = 0o21 //八进制的Int数据, 以 0o 开头
let hexInt: Int = 0x11 //十六进制的Int数据, 以 0x 开头
let bignum = 100_0000  //对于大数据的书写形式, 可以用下划线来分隔, 以方便显示.
let bignumZH = 100_0000 //中国习惯为四位一组的.
let imFloat: Float = 3.1415926  //声明单精度数据类型. 注意当小数到第6位后就无法显示了, 会进位的.
let imDoublue: Double = 3.1415926 //声明双精度数据类型. 注意默认小数数据如果不指定其类型, 默认是Double类型的

String(hexInt, radix: 10, uppercase: false) // 十六进制的数转为十进制字符串
Int("1101", radix: 2) // 二进制字符串转10讲制

"\u{61}" // U 码表示字符串

/**
 16进制转10进制
 例：2AF5换算成10进制:
 
 用竖式计算：
 第0位： 5 * 16^0 = 5      5 * 16的零次方
 第1位： F * 16^1 = 240    F * 16的一次方 (F 可以理解为 15)
 第2位： A * 16^2 = 2560
 第3位： 2 * 16^3 = 8192
 
 然后相加就是 10997
 
 
 16进制到二进制
 由于在二进制的表示方法中，每四位所表示的数的最大值对应16进制的15，即16进制每一位上最大值，所以，我们可以得出简便的转换方法，将16进制上每一位分别对应二进制上四位进行转换，即得所求：
 例：2AF5换算成2进制:
 第0位： (5) 16 = (0101) 2  -> 5 对应的二进制是 0101
 第1位： (F) 16 = (1111) 2  -> F 对应的就是四位的最大值 1111
 第2位： (A) 16 = (1010) 2
 第3位： (2) 16 = (0010) 2 
 -------------------------------------
 得：(2AF5)16=(0010.1010.1111.0101)2
 
 
 从二进制转换成十六进制的简便方法例举
 16进制就有16个数，0~15，用二进制表示15的方法就是1111，从而可以推断出，16进制用2进制可以表现成0000~1111，顾名思义，也就是每四个为一位。举例：
 00111101可以这样分：
 0011|1101（最高位不够可用零代替），对照着二进制的表格，1024 512 256 128 64 32 16 8 4 2 1 （一般例举这么多就够了，如果有小数的话就继续往右边列举，如0.5 0.25 0.125 0.0625……）
 1024 512 256 128 64 32 16 8 4 2 1
 0 0 1 1| 1 1 0 1
 左半边=2+1=3 右半边=8+4+1=13=D
 结果，0111101就可以换算成16进制的3D。
 */

let x = 3.1415926
var aa = 1.25e10 //科学数据的表示
var bb = 1.25e-8

var cc = 1_000_000.000_000_1 //符点数的分隔表示形式.

/***
 ** 数据类型转换
 ** Swift是强类型的语言, 不同类型的数据操作时必须强制转换为一致的数据类型方能操作.
 ***/
let xx: UInt16 = 100
let yy: UInt8 = 20
let xxyy = xx+UInt16(yy) //强制转换yy为16位的Int类型.
let xxxyyy = UInt8(xx) + yy

let w:Float = 3 //即使3看起来是Int型数据, 但指定了其变量w为Float类型, 其即是Float类型
let v:Int = Int(3.0) //在赋值时, 同样也必须为指定类型的变量赋值相同的类型.

let xyz:float_t = 9.09991

//CGFloat 一种特殊的Float符点类型.
let red:CGFloat = 0.2
let green:CGFloat = 0.5
let blue:CGFloat = 0.3

/***
 ** Tuple 元组的使用
 ***/
var point = (5, 2)
var httpResponse = (404, "Not Fount")  //元组可以支持不同的数据类型.

var point2:(Int, Int, Int) = (10, 5, 2)  //声明式赋值
var httpResponse2:(Int, String)=(200, "OK")

httpResponse2.1 = "OKl"

var (xp, yp) = point //元组赋值给元组, 等于给元组中的xy, yp进行赋值了
xp = 2
print(xp)

let point3 = (xp3: 33, yp3: 2) //给point3常量赋值元组数据. 其中指明了元组的下标.
point3.xp3
point3.0  //元组的引用
point3.1

let point4: (xp4: Int, yp4: Int) = (10, 5)  //声明元组的下标和类型, 同时为其赋值.
point4.xp4
point4.0

let loginResult = (true, "liuyuboobo")
let (isLoginSuccess, _) = loginResult  //*** 使用下划线来占位不使用元组的某项值.
if( isLoginSuccess){
    loginResult.1
}

// Tuple 的 switch 方法验证
switch point {
case (_, 2): print("..2..")
default: print("/////")
}


/***
 ** 变量名
 ***/
let imoocUrl: String = "www.imooc.com";  //定义字符串类型.

var 名字="康桥😀"  //变量名可以使用中文等任意的UNCode字符.
名字.characters.count
print("我的名字是"+名字)
var 😀 = "smile" //变量名也可以使用特殊字符 😀[control + command + space]调出面板输入.

/***
 ** Print 的使用
 ***/
print(😀)
😀.characters.count

let xxx=1, yyy=2, zzz=3, bbb=true
print(xxx, yyy, zzz, bbb) //print可以同时输出多个变量. 用豆号隔开, 每个豆号会被转换为一个空格
print(xxx, yyy, zzz, separator:"___")  //使用separator指定print函数中变量值的分隔符.
print(xxx, yyy, zzz, separator:"___", terminator:";)")//使用terminator指定print输出以什么结束, 默认是\n结束的
print("kangqiao")
print(print(😀))

print(xxx, "*", yyy, "=", xxx * yyy)  // 格式式输出
print("\(xxx) * \(yyy) = \(xxx * yyy)")  //使用反斜线加()号进行格式输出.
