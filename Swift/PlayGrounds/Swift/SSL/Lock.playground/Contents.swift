//: https://www.jianshu.com/p/ec4f65d773e9

import Cocoa

/**
 Locks

 When dealing with iOS apps, we are always sandboxed to their processes. A process creates and manages threads, which are the main building blocks in multitasking iOS applications.

 Lock is an abstract concept for threads synchronization. The main idea is to protect access to a given region of code at a time. Different kinds of locks exist:

 Semaphore — allows up to N threads to access a given region of code at a time.
 Mutex — ensures that only one thread is active in a given region of code at a time. You can think of it as a semaphore with a maximum count of 1.
 Spinlock — causes a thread trying to acquire a lock to wait in a loop while checking if the lock is available. It is efficient if waiting is rare, but wasteful if waiting is common.
 Read-write lock — provides concurrent access for read-only operations, but exclusive access for write operations. Efficient when reading is common and writing is rare.
 Recursive lock — a mutex that can be acquired by the same thread many times.
 */

/// pthread_mutex_t

/// A lower-level C pthread_mutex_t is also available in Swift. It can be configured both as a mutex and a recursive lock.

/// pthread_rwlock_t

/// is a lower-level read-write lock that can be used in Swift.

/// DispatchSemaphore

/// provides an implementation of a semaphore. It is listed here for the sake of completeness, as it makes little sense to use semaphore for designing atomic properties.

/// os_unfair_lock_s()

/// os_unfair_lock_lock

/// os_unfair_lock_unlock


/// dispatch_semaphore_t // Using in the SDWebImage, which is a lock to keep the access to `URLOperations` thread-safe


/**
 OSSpinLock has been deprecated in iOS 10

 */


/// Implementing Atomic Property using Locks

// MARK: - Locks
protocol Lock {
    func lock()
    func unlock()
}

extension NSLock: Lock {}

final class SpinLock: Lock {
    private var unfairLock = os_unfair_lock_s()

    func lock() {
        os_unfair_lock_lock(&unfairLock)
    }

    func unlock() {
        os_unfair_lock_unlock(&unfairLock)
    }
}

final class Mutex: Lock {
    private var mutex: pthread_mutex_t = {
        var mutex = pthread_mutex_t()
        pthread_mutex_init(&mutex, nil)
        return mutex
    }()

    func lock() {
        pthread_mutex_lock(&mutex)
    }

    func unlock() {
        pthread_mutex_unlock(&mutex)
    }
}

// MARK: - Atomic Property
struct AtomicProperty {
    private var underlyingFoo = 0
    private let lock: Lock

    init(lock: Lock) {
        self.lock = lock
    }

    var foo: Int {
        get {
            lock.lock()
            let value = underlyingFoo
            lock.unlock()
            return value
        }
        set {
            lock.lock()
            underlyingFoo = newValue
            lock.unlock()
        }
    }
}

// MARK: - Usage
let sample = AtomicProperty(lock: SpinLock())
_ = sample.foo


final class ReadWriteLock {
    private var rwlock: pthread_rwlock_t = {
        var rwlock = pthread_rwlock_t()
        pthread_rwlock_init(&rwlock, nil)
        return rwlock
    }()

    func writeLock() {
        pthread_rwlock_wrlock(&rwlock)
    }

    func readLock() {
        pthread_rwlock_rdlock(&rwlock)
    }

    func unlock() {
        pthread_rwlock_unlock(&rwlock)
    }
}

class ReadWriteLockAtomicProperty {
    private var underlyingFoo = 0
    private let lock = ReadWriteLock()

    var foo: Int {
        get {
            lock.readLock()
            let value = underlyingFoo
            lock.unlock()
            return value
        }
        set {
            lock.writeLock()
            underlyingFoo = newValue
            lock.unlock()
        }
    }
}

/**
Implementing Atomic Property using queues

Besides locks, Swift has DispatchQueue and OperationQueue that also can be used to design an atomic property.

Both queues are used to manage the execution of work items and they can configured to achieve lock behavior. The examples are below.
 */

// MARK: - DispatchQueue
class DispatchQueueAtomicProperty {
    private let queue = DispatchQueue(label: "com.vadimbulavin.DispatchQueueAtomicProperty")
    private var underlyingFoo = 0

    var foo: Int {
        get {
            return queue.sync { underlyingFoo }
        }
        set {
            queue.sync { [weak self] in
                self?.underlyingFoo = newValue
            }
        }
    }
}

// MARK: - OperationsQueue
class OperationsQueueAtomicProperty {
    private let queue: OperationQueue = {
        var q = OperationQueue()
        q.maxConcurrentOperationCount = 1
        return q
    }()

    private var underlyingFoo = 0

    var foo: Int {
        get {
            var foo: Int!
            execute(on: queue) { [underlyingFoo] in
                foo = underlyingFoo
            }
            return foo
        }
        set {
            execute(on: queue) { [weak self] in
                self?.underlyingFoo = newValue
            }
        }
    }

    private func execute(on q: OperationQueue, block: @escaping () -> Void) {
        let op = BlockOperation(block: block)
        q.addOperation(op)
        op.waitUntilFinished()
    }
}
