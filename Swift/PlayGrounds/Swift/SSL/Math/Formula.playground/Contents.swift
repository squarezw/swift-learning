//: Playground - noun: a place where people can play

import UIKit

/**
 The sum of four consecutive odd integers w,x,y,and z is 24.What is the median of the set{ w,x,y,z,24}?
 
 4个连续奇数w,x,y,z的和是24 ,这一组数{ w,x,y,z,24}的中位数是多少?
 median是中位数的意思 当然 y 就是这组数中的中位数啦
 4个奇数很好求 w+(w+2)+(w+4)+(w+6)=24 得w=3 y=w+4=7
 所以 答案是7
 */

