//: Playground - noun: a place where people can play

import Cocoa


let numbers = [10, 20, 30, 40, 50, 60]
let i = numbers.index(of: 30)!
numbers.suffix(from: i)
numbers.prefix(upTo: i)

/*** Swift Class **/
//define class
class Person{
    
    let firstName: String
    let lastName: String
    
    init(firstName: String, lastName:String){
        self.firstName = firstName
        self.lastName = lastName
    }
    
    init?(fullName:String){
        guard let spaceIndex = fullName.index(of: " ") else{
            return nil
        }
        
        self.firstName = String(fullName.prefix(upTo: spaceIndex))
        self.lastName = String(fullName.suffix(from: fullName.index(after: spaceIndex)))
    }
    
    func fullName() -> String{
        return firstName + " " + lastName
    }
    
    // static 变量必须有一个初始值
    static var data :Double = 33.0
}

let person1 = Person(firstName: "Alexander", lastName: "Hamilton") //返回Persion类型的persion1常量.
let person2 = Person(fullName: "Steve Jobs") //初始化使用了init?可失败的构造函数, 所以返回可选型Person?
print(person1.fullName())
print(person2!.fullName())

//***** 类是引用类型.
class Person2{
    
    var firstName: String
    let lastName: String
    private var career: String?
    
    init(firstName: String, lastName: String, career: String){
        self.firstName = firstName
        self.lastName = lastName
        self.career = career
    }
    
    init(firstName: String, lastName: String){
        self.firstName = firstName
        self.lastName = lastName
    }
    
    func fullName() -> String{
        return firstName + " " + lastName
    }
    
    func changeCareer(newCareer : String){
        self.career = newCareer //由于class是引用类型, 所以可以方法中改变其自身的属性.
    }
}

let p1 = Person2(firstName: "Yubo", lastName: "Liu", career: "Developer")
let p2 = p1
p2.firstName = "Steve" //虽然p1被let修饰, 其var修饰的firtName属性仍能被改变, 这点区别于值类型的结构体枚举类型.
//p2.lastName = "Jobs"  //lastName 被let修饰, 不可以改变.
p2.changeCareer(newCareer: "CEO") //由于career是被private修饰, 所以可以调用changeCareer方法修改其值.
p2
p1 //p1和p2是引用类型所以一个改变其另一个也被改变了


//类的等价 === 和 !==
//由于==本质是值的比较, 它只能用于值类型的比较, 不能用于引用类型.
//p1 == p2 //Binary operator '==' cannot be applied to two 'Person2' operands.
p1 === p2 // '===' 用于比较引用类型是否是同一类型的变量是否指向同样的内存空间.
let p3 = Person2(firstName: "Steve", lastName: "Liu", career: "CEO")
p1 === p3 //p1和p3分别指向不同的内存空间. 所以false
p1 !== p3 //p1和p3分别指向不同的内存空间. 所以true

/***
 * 什么时候用结构体??
 *    把结构体看作是值 例如 1.位置(经纬度) 2.坐标(二维, 三维) 3.温度 ...
 *    结构体是值类型 封装值的时候通常用值类型.
 *    结构体比类更"轻量级" 比类更高效,
 *    结构体在内存空间的栈中.
 * 什么时候用类??
 *    把类看作物体 例如 1.人 2.车 3.动物 4.商店 ...
 *    类是引用类型 如果经常改变用类. 封装物体时通常使用类.
 *    类是可以被继承的
 *    类在内存空间的堆中.
 **/

/***
 *
 * 类的第二章
 * 构建类和结构体的属性和方法
 *
 **/
//计算型的属性
struct Point{
    var x = 0.0
    var y = 0.0
}

struct Size{
    var width = 0.0
    var height = 0.0
}

class Rectangle{
    var origin = Point() //存储型的属性
    var size = Size() //存储型的属性
    var center: Point{ //计算型属性, 必须声明为var, 必须显示声明它的类型Point, 要想给其赋值必须使用get, set,
        
        //getter
        get{
            let centerX = origin.x + size.width/2
            let centerY = origin.y + size.height/2
            return Point(x: centerX, y: centerY)
        }
        
        //setter
        set(newCenter){ //如果不指定newCenter参数名, 可以使用默认newValue
            origin.x = newCenter.x - size.width/2
            origin.y = newCenter.y - size.height/2
        }
    }
    
    var area: Double{ //计算型属性
        get{
            return size.width * size.height
        }
    }
    
    init(origin: Point, size: Size){
        self.origin = origin
        self.size = size
    }
}

var rect = Rectangle(origin: Point(), size: Size(width: 10, height: 5))
rect.center // x=5, y=2.5
rect.origin = Point(x: 10, y: 10) //改变origin位置属性, center计算型属性相即被改变了
rect.center // x=15, y=12.5
rect.center = Point() // 如果有setter方法, 改变center的值也相即改变了origin的值.
rect


//Apple官方提供的CGRect
var rect2 = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 10, height: 5))
rect2

//类型属性 Type property
class Player{
    var name: String
    var score = 0
    static var highestScore = 0 //类型属性, 它是属于类的. 对类的对象共享.
    
    init(name: String){
        self.name = name
    }
    
    func play(){
        let score = Int(arc4random())%100
        print("\(name) played and got \(score) scores.")
        
        self.score += score
        print("Total score of \(name) is \(self.score).")
        
        if self.score > Player.highestScore{ //引用highestScore静态属性时, 类名不能省略.
            Player.highestScore = self.score
        }
        print("Highest score is \(Player.highestScore).")
    }
}

let player1 = Player(name: "player1")
let player2 = Player(name: "player2")

player1.play()
player1.play()
player2.play()


//Type Method 类型方法 静态方法
struct Matrix{
    var m: [[Int]]
    let row: Int
    let col: Int
    
    init?( _ arr2d:[[Int]] ){
        guard !arr2d.isEmpty else{
            return nil
        }
        
        let row = arr2d.count
        let col = arr2d[0].count
        for i in 1..<row{
            if arr2d[i].count != col{
                return nil
            }
        }
        
        self.m = arr2d
        self.row = row
        self.col = col
    }
    
    func printMatrix(){
        for i in 0..<row{
            for j in 0..<col{
                print(m[i][j], terminator:"\t")
            }
            print("")
        }
    }
    
    //返回一个线性矩阵
    static func identityMatrix(n: Int) -> Matrix?{
        guard n > 0 else{
            return nil
        }
        
        var arr2d:[[Int]] = []
        for i in 0..<n {
            var row = [Int](repeating: 0, count: n)
            row[i] = 1
            arr2d.append(row)
        }
        
        return Matrix(arr2d)
    }
}

if let m = Matrix([[1,2], [3, 4]]) {
    m.printMatrix()
}

if let e = Matrix.identityMatrix(n: 5){
    e.printMatrix()
}


//Property Observer 属性观察器.
class LightBulb{
    static let maxCurrent = 30 //通过灯泡最大的电流
    var current = 0 { //当前通过的电流
        
        willSet/*(newCurrent)*/ { //willSet将要赋值时 //参数默认newValue
            print("Current value changed. The change is \(abs(current-newValue/*newCurrent*/))")
        }
        
        didSet/*(oldCurrent)*/ { //didSet是已经赋值完成后 //参数默认oldValue
            
            if current == LightBulb.maxCurrent{
                print("Pay attention, the current value get to the maximum point.")
            }
            else if current > LightBulb.maxCurrent{
                print("Current too high, falling back to previous setting.")
                current = oldValue//oldCurrent
            }
            
            print("The current is \(current)")
        }
    }
}

let bulb = LightBulb()
bulb.current = 20
bulb.current = 30
bulb.current = 40 //40已经超过了灯泡的最大负载量.

//属性观察器应用2
enum Theme{
    case DayMode
    case NightMode
}

//注意: didSet和willSet不会在初始化阶段init调用的. *****
class UI{
    //UIColor! 由于类声明时不希望它是nil的, 但是在类在真正初始化之前,
    //我不清楚themmMode是什么模式, 所以还不能确定fontColor和backgroundColor是什么颜色, 暂时让它们是nil.
    //对于这种情况, 可以用!这种方式.
    //这样在初始化init中就不需要给其赋初值了.
    var fontColor: NSColor!
    var backgroundColor: NSColor!
    var themeMode: Theme = .DayMode{
        didSet{
            changeTheme(themeMode: themeMode)
        }
    }
    
    init(themeMode: Theme){
        self.themeMode = themeMode //在init中并不会触发didSet属性观察器. **** 我们需要手动去初始化相关的值.
        changeTheme(themeMode: themeMode)
    }
    
    func switchUI(){
        themeMode = .DayMode
    }
    
    func changeTheme(themeMode: Theme){
        switch(themeMode){
        case .DayMode:
            fontColor = NSColor.black
            backgroundColor = NSColor.white
        case .NightMode:
            fontColor = NSColor.white
            backgroundColor = NSColor.black
        }
    }
}

let ui = UI(themeMode: .NightMode)
ui

//延迟加载属性 Lazy Load
class CloseRange { //一个闭区间类
    var start : Int = 0 //存储型属性
    var end : Int = 0
    
    var width : Int{ //计算型属性
        return end - start + 1
    }
    
    //延迟属性只计算一次, 注: lazy不能应用于let常量声明的属性.
    lazy var sum: Int = { //用闭包来计算
        print("start computing sum value")
        
        var res = 0
        for i in self.start...self.end{
            res += i
        }
        return res
    }() //()代表闭包的调用
    
    init?(start : Int, end : Int){
        if start > end {
            return nil
        }
        self.start = start
        self.end = end
    }
}

if let range = CloseRange(start: 0, end: 10_000){
    range.width
    print("==== 延迟性属性只计算一次 ====")
    range.sum
    range.sum
    range.sum
}

class Location{
    let latitude: Double //常量属性
    let longitude: Double
    
    //由于计算经纬度比较耗时, 初始化时, 且只计算一次, 这时就可以用延迟属性, 当用到时只解析一次.
    lazy var address: String? = {
        //执行经纬度解析
        return nil
    }()
    
    init(latitude: Double, longitude: Double){
        self.latitude = latitude
        self.longitude = longitude
    }
}

class Book{
    let name: String
    lazy var content: String? = { //等用户需要时, 再去读取整本书的内容.
        //从本地读取书的内容
        return nil
    }()
    
    init(name: String){
        self.name = name
    }
}

class Web{
    let url: String
    lazy var html: String? = {
        //从网络读取url对应的html
        return nil
    }()
    
    init(url: String){ //当用户点击时,才去加载url代表的html内容.
        self.url = url
    }
}



// 初始化方法顺序

class Cat {
    var name: String
    init() {
        name = "cat"
    }
}

class Tiger: Cat {
    let power: Int
    override init() {
        power = 10
        super.init() // 如果没有对父类属性的修改，这一步是可以省略的，因为默认最后就会自动添加上 super.init()
        name = "tiger"
    }
}

print(Tiger().name)

// extension

// 添加静态属性
extension Person {
    
    // static 变量或是可选参数，用 ? 申明
    static var ext: String? {
        didSet {
            
        }
    }
    
}

/*
 * Designated, Convenience, Required
 */

class ClassA {
    let numA: Int
    init(num: Int) { // 加上 required 表示子类必须重写该方法
        numA = num
    }
    
    convenience init(bigNum: Bool) {
        self.init(num: bigNum ? 10000 : 1)
    }
}

class ClassB: ClassA {
    let numB: Int

    override init(num: Int) {
        numB = num + 1
        super.init(num: num)
    }
}

let anObj = ClassB(bigNum: true)
anObj.numA
anObj.numB
