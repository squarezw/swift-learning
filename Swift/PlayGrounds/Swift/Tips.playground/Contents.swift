//: Tips
//: https://developer.apple.com/swift/blog/ Swift 2 与 3 官方介绍

//: 几个推荐的网站

//: Swift 非常使用的入门详解，全程 playground
//: 所有示例代码是跟随 幕课网 liuyubobobo的玩转swift系列
//: https://github.com/KQAppleDev/SwiftLearn

import Cocoa

// 设置颜色的另类方法, #color Literal
var backgroundColor = #colorLiteral(red: 1, green: 0.2352941176, blue: 0.1882352941, alpha: 1)

// case 用在 for 循环里
let arr = ["a", "b", nil, "c"]

for case let x? in arr {
    print(x)
}


/*
 * 更 swifter 的累加
 */
func sum(input: Int...) -> Int {
    return input.reduce(0, +)
}
sum(input: 1,2,3,5)


var t = 1
while t < 10 {
    t += 1
}


/*
 是时候放弃前缀的扩展了
 以前我们要给 UIView 扩展是这样的
 
 extension UIView {
    var zz_height:CGFloat{
        set(v){
            self.frame.size.height = v
        }
     
        get{
            return self.frame.size.height
        }
    }
 }
 
 这样在自己写的属性前面加一个前缀。
 
 但是Swift 3出来后更多的选择应该是这样的view.zz.height
 
 SnapKit 也改变成了 make.snp.left 之类的语法，那么怎么写这样的扩展呢？
 */

// 写一个协议 定义一个只读的类型
public protocol UIViewCompatible {
    associatedtype CompatableType
    var zz: CompatableType { get }
}

public extension UIViewCompatible {
    // 指定泛型类型为自身 ， 自身是协议 谁实现了此协议就是谁了
    public var zz: Auto<Self> {
        get { return Auto(self) } // 初始化 传入自己
        set { }
    }
}

// Auto是一个接受一个泛型类型的结构体
public struct Auto<Base> {
    // 定义该泛型类型属性
    public let base: Base
    public init(_ base: Base) {
        self.base = base
    }
}

// 写一个Auto的扩展 指定泛型类型是UIView 或者其子类
public extension Auto where Base:NSView {
    var height:CGFloat{
        set(v){
            base.frame.size.height = v
        } get{
            return base.frame.size.height
        }
    }
}

// 扩展 UIView 实现 UIViewCompatible 协议，就拥有了zz属性 zz又是Auto类型 Auto是用泛型实例化的 这个泛型就是UIView了
extension NSView : UIViewCompatible{
}

let view = NSView(frame: NSRect(x:0, y:0, width: 100, height:100))

// 使用
view.zz.height



/*
extension UIView {
    // 根据View查找VC
    func responderViewController() -> UIViewController {
        var responder: UIResponder! = nil
        var next = self.superview
        while next != nil {
            responder = next?.next
            if (responder!.isKind(of: UIViewController.self)){
                return (responder as! UIViewController)
            }
            next = next?.superview
        }
        return (responder as! UIViewController)
    }
}
*/

/* View中的第一响应者
 
 利用这个可以在 NSNotification.Name.UIKeyboardWillShow 通知通知中拿到第一响应者，如果第一响应者是UITextfield，可以算出局底下的距离，给挡墙view做变换。避免被覆盖。
 
func findFirstResponder()->UIView?{
    if self.isFirstResponder{
        return self
    }
    for subView in self.subviews{
        let view = subView.findFirstResponder()
        if view != nil {
            return view
        }
    }
    return nil
}
 */

// 千万别把 NSArray 的 map 与 swift Array [] 的 map 搞混了。两个返回完全不一样的数据类型
// let cc: NSArray, cc.map{}   
// let cc: [Int], cc.map{}


// 避免循环引用
class Weather {
    var name: String?
    
    lazy var printName: ()->() = { [weak self] in
        let strongSelf = self;
        print("The name is \(strongSelf?.name ?? "")")
    }
}

/***
 * 访问控制 public internal private
 *    public : 指可以被模块外访问的. [当编写SDK供他人使用时]
 *    internal : 可以被本模块访问 [默认]
 *    private : 可以被本文件访问 [编写APP]
 * Sources目录中只能定义, 不能执行语句.
 */
print("==== Swift 访问控制 是以文件为单位的 ====")


// 访问控制权限由高到低依次为：open、public、internal（默认）、fileprivate，private
// open         最开放，可继承可敷盖
// public       在当前 module 内被继承或者override，在 module 外只能被访问
// internal     默认, 可敷盖
// fileprivate  文件内可敷盖
// private      完全私有，不能敷盖

// final 是一个辅助修饰词，表示当前类、属性或者方法在任何地方都只能被访问，不能被继承或者override；

// 同样用在 class 上后，里面的成员不能和 class 有冲突, 比如 class 设为 public ，func 设置为 open

/// 以下所有的类都在同一个文件中
public class Test: NSObject {
    // 只能在当前大括号内访问
    private var value: Int = 0
    // 只能在当前文件内访问
    fileprivate var valueOfFile: Int = 0
    
    // 只能在当前类访问或敷盖
    private func privatePractise() {
        value = 1
        valueOfFile = 1
    }
    // 只能在当前文件内访问
    fileprivate func fileprivatePractise() {
        
    }
    
    func government() {
        print("government")
    }
    
    final func run() {
        print("You can only running inside!")
    }
}


class SubTest: Test {
    override func fileprivatePractise() {
        print("file private practise")
    }
    
    // 这里覆盖会出错，因为父类用了 final 修饰
//    override func run() {}
}


/*
 Swift 中 struct 和 class 什么区别？举个应用中的实例
 
 struct 是值类型，class 是引用类型。
 看过WWDC的人都知道，struct 是苹果推荐的，原因在于它在小数据模型传递和拷贝时比 class 要更安全，在多线程和网络请求时尤其好用。我们来看一个简单的例子：
 */

class A {
    var val = 1
}

var a = A()
var b = a
b.val = 2
print(a.val)

// 此时 a 的 val 也被改成了 2，因为 a 和 b 都是引用类型，本质上它们指向同一内存。解决这个问题的方法就是使用 struct：

struct AA {
    var val = 1
}

var aa = AA()
var bb = aa
bb.val = 2
print(aa.val)

// 此时 A 是struct，值类型，b 和 a 是不同的东西，改变 b 对于 a 没有


// 数字转字符串
(1...5).map(String.init)


readLine() // 读取输入的一行
if let some = readLine() {
    print(some)
}

// 10 < y < 30  范围取值
let y = 20
min(30, max(y, 10))


// 打印类型
type(of: y)


// 整型数字相加 , 29 -> 2+ 9 = 11
let n = 29
var sum = 0
var m = n
while(m > 0){
    sum += m % 10
    m = m/10
}
sum


// 判断如果不是 swift 3 的情况下用什么类型
#if !swift(>=3.0)
    typealias IteratorProtocol = GeneratorType
    typealias Sequence = SequenceType
    typealias Collection = CollectionType
#endif

// #function 可以做为参数传递

func makeFileForTest(named testName: StaticString = #function) {
    print("function name:",testName)
}

func doTest() {
    makeFileForTest()
}

doTest()


// 如何加警告信息
#warning("This is terrible method of encryption")

#if os(macOS)
//#error("Please enter your API key below then delte this line.")
#endif
