//: Playground - noun: a place where people can play

import UIKit

// 如果所有代码只用一行搞定，还要什么自行车呢?

// 旋转字符串
String("This is a test string.".reversed())

// 截取字符串
let str = "abcdefghijkl"
str[str.index(str.startIndex, offsetBy: 3)...str.index(str.startIndex, offsetBy: 7)]

str.index(before: str.endIndex)
str.distance(from: str.index(before: str.endIndex), to: str.startIndex)
