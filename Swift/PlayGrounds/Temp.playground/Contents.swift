//: Playground - noun: a place where people can play

import Cocoa
import CoreLocation

//public func solution(_ N : Int) -> Int {
//    let t = String(N).characters.map { String($0) }
//
//    var result = [[String]]()
//    
//    permuteWith(t, t.count - 1, &result)
//    
//    let s = result.flatMap { (characters) -> String? in
//        if characters[0] == "0" {
//            return nil
//        }
//        return characters.joined()
//    }
//    
//    let tt = Set(s).count
//    
//    return tt
//}
//
//func permuteWith(_ a: [String], _ n: Int, _ result: inout [[String]]) {
//    if n == 0 {
//        result.append(a)
//    } else {
//        var a = a
//        permuteWith(a, n - 1, &result)
//        for i in 0..<n {
//            swap(&a[i], &a[n])
//            permuteWith(a, n - 1, &result)
//            swap(&a[i], &a[n])
//        }
//    }
//}

/**
 无向图距离
 Largest K element
 
 https://github.com/ChenYilong/iOSInterviewQuestions
 http://draveness.me/guan-yu-xie-ios-wen-ti-de-jie-da.html
 https://github.com/lzyy/iOS-Developer-Interview-Questions
 
 
 必看:
 http://wdxtub.com/interview/14520609088903.html
 
 */

extension String {
    
    var length: Int {
        return self.count
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[Range(start ..< end)])
    }
    
}

func getNext(patternStr: String) -> [Int] {
    let count = patternStr.count
    var k = -1
    var next = Array(repeating: -1, count: count)
    for var j in stride(from: 0, to: count - 1, by: 1) {
        while (k != -1 && patternStr[patternStr.index(patternStr.startIndex, offsetBy: j)] != patternStr[patternStr.index(patternStr.startIndex, offsetBy: k)]) {
            k = next[k]
        }
        k += 1
        j += 1
        next[j] = k
    }
    return next
}

func strstr(str TextStr: String, x patternStr: String) -> Int {
    var indexI = TextStr.startIndex
    var indexJ = patternStr.startIndex
    var j = 0
    let next = getNext(patternStr: patternStr)
    
    while (indexI < TextStr.endIndex && indexJ < patternStr.endIndex) {
        if j == -1 || TextStr[indexI] == patternStr[indexJ] {
            j += 1
            if indexJ == patternStr.index(before: patternStr.endIndex) {
                // 如果匹配到最后一个字符，则匹配成功，返回当前的indexI - indexJ 的位置
                return TextStr.distance(from: indexJ, to: indexI)
            }
            //如果当前字符串匹配成功，则同时向后移动一位
            indexI = TextStr.index(indexI, offsetBy: 1)
            indexJ = patternStr.index(indexJ, offsetBy: 1)
        }else {
            //如果匹配失败，则 TextStr 的 indexI 保持不变，同时 patternStr 的 indexJ 跳到 next[j] 的位置
            //如果next[j] = -1 ,直接跳到 P 的开头
            let distance = TextStr.distance(from: patternStr.startIndex, to: indexJ)
            j = next[distance]
            if j == -1 {
                indexJ = patternStr.startIndex
            } else {
                indexJ = patternStr.index(patternStr.startIndex, offsetBy: j)
            }
        }
        
    }
    return -1
    
//    let startIndex = s.characters.index(of: x.characters.first!)
//    let w = s.characters.distance(from: s.startIndex, to: index!)
//    
//    let arrS = Array(s.characters)
//    let arrX = Array(x.characters)
//    for i in w..<arrS.count {
//        if arrS[i] == arrX.first && i + arrX.count < arrS.count {
//            if String(arrS[i..<i+arrX.count]) == x {
//                return true
//            }
//        }
//    }
//    return false
}

strstr(str: "abcdefceg", x: "ce")


let lowercaseLetters = Character("a")...Character("z")

print(lowercaseLetters)

let myRange: CountableClosedRange = 1...3
let s = "abcdef"

Array(s)[myRange]



let c = 16

let d = 11 ^ 13
let e = 1073741823 ^ 1071513599






func differentRightmostBit(n: Int, m : Int) -> Int {
    String(n ^ m, radix:2).components(separatedBy: ",")
    return Int(pow(2, Double(String(n ^ m, radix:2).components(separatedBy: ",").last!.count)))
}

differentRightmostBit(n: 11, m: 13)

//Array(String(e, radix:2).characters).filter { $0 == "1"}.count

let a = 10
let b = 11

print(String(a, radix:2))
print(String(b, radix:2))

print(String(~(a ^ b), radix:2))

//(a^b) & -(a^b)

a^b

print(String((a^b), radix:2))
print(String(122, radix:2))
print(String((a^b) & -(a ^ b), radix:2))

func leastFactorial(n: Int) -> Int {
    var i = 1
    var result = 1
    while result < n {
        result *= i
        i += 1
    }
    return result
}

leastFactorial(n: 17)

cos(20*0.9)


var view = NSView()

view.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
print(view.bounds)

//view.setBoundsOrigin(CGPoint(x: 10, y: 10))
view.translateOrigin(to: CGPoint(x: 10, y: 10))

print(view.bounds)
print(view.frame)


let url = URL(string: "https://http2.akamai.com")

// URLSession can support HTTP /2.0
let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
    if error == nil, let data = data, let str = String(data: data, encoding: .utf8) {
        if str.contains("You are using HTTP/2 right now!") {
            NSLog("Used HTTP/2")
        } else {
            NSLog("Used HTTP 1.1")
        }
    } else {
        // error...
    }
}

task.resume()

// But the old Connection can't
//let request = URLRequest(url: url!)
//NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler: { (res, data, error) in
//    if error == nil, let data = data, let str = String(data: data, encoding: .utf8) {
//        if str.contains("You are using HTTP/2 right now!") {
//            print("Used HTTP/2")
//        } else {
//            print("Used HTTP 1.1")
//        }
//    } else {
//        // error...
//    }
//})

//@objcMembers class Kid: NSObject {
//    dynamic var x: String
//    dynamic var y: String
//}
//
//
//struct BirthdayParty {
//    let c: Kid
//    var theme: String
//    var attending: [Kid]
//}
//
//let ben = Kid(x: "1", y: "2")
//
//let age = ben[keyPath: \Kid.x]

struct Person: Codable {
    let name: String
    let age: Int
    let friends: [Person]?
}

let person = Person(name: "Brad", age: 53, friends: nil)
// Sorry Brad
let encoder = JSONEncoder()
let jsonData = try encoder.encode(person)
String(data: jsonData, encoding: .utf8)! // {"name":"Brad","age":53}

let decoder = JSONDecoder()
let decoded = try decoder.decode(Person.self, from: jsonData)
type(of: decoded) // Person

// ===========

protocol Mappable: Decodable {
    init?(jsonString: String)
}
extension Mappable {
    init?(jsonString: String) {
        guard let data = jsonString.data(using: .utf8) else {
            return nil
        }
        self = try! JSONDecoder().decode(Self.self, from: data)
        // I used force unwrap for simplicity.
        // It is better to deal with exception using do-catch.
    }
}

let jsonString = """
{
\"name\": \"Brad\",
\"age\": 53
}
"""

struct PersonM: Mappable {
    let name: String
    let age: Int
    let friends: [PersonM]?
}
let newPerson = PersonM(jsonString: jsonString)
type(of: newPerson) // Person?

struct PersonMM: Mappable {
    let alias: String
    let age: Int
    let comrades: [PersonMM]?
    
    private enum CodingKeys : String, CodingKey {
        case alias = "name"
        case age
        case comrades = "friends"
    }
}

struct PersonMMM: Mappable {
    let birth: Date
    let position: CLLocation
    let name: String?
    enum CodingKeys: String, CodingKey {
        case birth = "birth_date"
        // Response will contain lat & long but
        // we will store as coordinate
        case lat
        case long
        case name
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let birthString = try values.decode(String.self, forKey: .birth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        birth = formatter.date(from: birthString)!
        // Parse lat and long to initialize coordinate
        let lat = try values.decode(Float.self, forKey: .lat)
        let long = try values.decode(Float.self, forKey: .long)
        position = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}
