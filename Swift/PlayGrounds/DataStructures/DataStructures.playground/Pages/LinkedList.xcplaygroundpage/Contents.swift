//: Queue

import UIKit

struct Node<T: Equatable> {
    var value: T? = nil
    var next: Node? = nil
}

class LinkedList<T: Equatable> {
    var head = Node<T>()


    func insert(value: T) {
        //find to see if empty list
        if self.head.value == nil {
            self.head.value = value
        } else {
            //find the last node without a next value
            var lastNode = self.head
            while lastNode.next != nil {
                lastNode = lastNode.next!
            }
            //once found, create a new node and connect the linked list
            let newNode = Node<T>()
            newNode.value = value
            lastNode.next = newNode
        }
    }
}



public class LinkedList {
    fileprivate var head: Node?
    private var tail: Node?

    public var isEmpty: Bool {
        return head == nil
    }

    public var first: Node? {
        return head
    }

    public var last: Node? {
        return tail
    }
}

public func append(value: String) {
    // 1
    let newNode = Node(value: value)
    // 2
    if let tailNode = tail {
        newNode.previous = tailNode
        tailNode.next = newNode
    }
    // 3
    else {
        head = newNode
    }
    // 4
    tail = newNode
}

let dogBreeds = LinkedList()
dogBreeds.append(value: "Labrador")
dogBreeds.append(value: "Bulldog")
dogBreeds.append(value: "Beagle")
dogBreeds.append(value: "Husky")

print(dogBreeds)

extension LinkedList: CustomStringConvertible {
    // 2
    public var description: String {
        // 3
        var text = "["
        var node = head
        // 4
        while node != nil {
            text += "\(node!.value)"
            node = node!.next
            if node != nil { text += ", " }
        }
        // 5
        return text + "]"
    }
}



public class ListNode<T>: CustomStringConvertible {
    var next: ListNode<T>?
    weak var previous: ListNode<T>?

    let element: T

    init(element: T) {
        self.element = element
    }
    
    public var description: String {
        return "cur:\(String(describing: element)) pre:\(String(describing: previous?.element)) next:\(String(describing: next?.element))"
    }
}
public struct LinkedList<T> {
    public typealias Element = T
    private var head: ListNode<T>?
    private weak var tail: ListNode<T>?
}
extension LinkedList: CustomStringConvertible {
    public mutating func append(element: Element) {
        let node = ListNode<T>(element: element)
        if head == nil || tail == nil {
            self.head = node
            self.tail = head
        } else {
            tail!.next = node
            node.previous = tail
            tail = node;
        }
   
    }

    public mutating func removeAll() {
        head = nil
    }
    
    public var description: String {
        var ans: Array<ListNode<T>> = []
        var cur = head;
        while let node = cur {
            ans.append(node)
            cur = node.next;
        }
                
        // for swift4:
        // ans.map{"\($0.description)"}.joined(separator: "\n")
        return ans.map{"\($0)"}.reduce("") {$0 + "\n" + $1}
    }
}

var count = 3
var ll = LinkedList<Int>()
for i in 0..<count {
    ll.append(element: i)
}
//ll.removeAll()
print(ll)
