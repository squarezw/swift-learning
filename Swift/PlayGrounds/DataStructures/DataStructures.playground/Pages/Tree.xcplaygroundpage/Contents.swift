//: Playground - noun: a place where people can play

import UIKit

class Node {
    var left: Node?
    var right: Node?
    let data: Int
    var level: Int = 1
    
    init(data: Int) {
        self.data = data
        self.left = nil
        self.right = nil
    }
    
    func insert(value: Int) {
        if value <= data {
            if left == nil {
                left = Node(data: value)
                left?.level = level + 1
            } else {
                left?.insert(value: value)
            }
        } else {
            if right == nil {
                right = Node(data: value)
                right?.level = level + 1
            } else {
                right?.insert(value: value)
            }
        }
    }
    
    func contains(value: Int) -> Bool {
        if value == data {
            return true
        } else if (value <= data) {
            if left == nil {
                return false
            } else {
                return left!.contains(value: value)
            }
        } else {
            if right == nil {
                return false
            } else {
                return right!.contains(value: value)
            }
        }
    }
    
    // inorder: left -> root -> right
    func printInOrder() {
        if left != nil {
            left!.printInOrder()
        }
        for _ in (0..<level) {
            print("", terminator: "\t")
        }
        print(data)
        if right != nil {
            right!.printInOrder()
        }
    }
    
    // preorder: root -> left -> right
    func printPreorder() {
        print(data)
        if left != nil {
            left!.printPreorder()
        }
        if right != nil {
            right!.printPreorder()
        }
    }
    
    class func desc(prefix: String, _ node: Node?, _ isLeft: Bool) {
        if node == nil {
            return
        }
        
        print(prefix + (isLeft == true ? "|-- " : "\\-- " ) + String(node!.data))
        desc(prefix: prefix + (isLeft == true ? "|   " : "    "), node?.left, true);
        desc(prefix: prefix + (isLeft == true ? "|   " : "    "), node?.right, false);
    }
    
    // Unfinish
    func printChildren(n: Int) {
        if left == nil && right == nil {
            return
        }
        
        //        if n != level {
        //            print("\n")
        //        }
        
        for _ in (0..<(5 - level)) {
            print("--", terminator: "\t")
        }
        
        print(childrens().map({$0.data}), terminator: "\t")
        
        if let _ = left {
            left!.printChildren(n: level)
        }
        
        if let _ = right {
            right!.printChildren(n: level)
            print("\n")
        }
    }
    
    func childrens() -> [Node] {
        var arr: [Node] = []
        if left != nil {
            arr.append(left!)
        }
        if right != nil {
            arr.append(right!)
        }
        return arr
    }
    
    func children() -> Bool {
        if left != nil || right != nil {
            return true
        }
        return false
    }
    
    class func height(node: Node?) -> Int {
        if node == nil {
            return 0
        }
        
        return max(Node.height(node: node?.left), Node.height(node: node?.right)) + 1
    }
}

let n = [5,3,1,4,6,7,9,6]
let node1 = Node(data: n[0])

for i in 1..<n.count-1 {
    node1.insert(value: n[i])
}

node1.printInOrder()

print(Node.height(node: node1))

node1.printChildren(n: 1)

Node.desc(prefix: "", node1, false)

