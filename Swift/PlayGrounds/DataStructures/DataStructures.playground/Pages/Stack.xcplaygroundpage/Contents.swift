//: Playground - noun: a place where people can play

import UIKit

// Stack class 结构

public class Stack<T> {
    
    /// 是否为空
    public var isEmpty: Bool { return stack.isEmpty }
    /// 栈的大小
    public var size: Int { return stack.count }
    /// 栈顶元素
    public var peek: Any? {
        return stack.last
    }
    
    private var stack: [T]
    
    /// 构造函数
    public init() {
        stack = [T] ()
    }
    
    /// 加入一个新元素
    public func push(_ obj: T) {
        stack.append(obj)
    }
    
    /// 推出栈顶元素
    public func pop() -> T? {
        return stack.popLast()
    }
}


// Struct  Stack 结构
struct StackS<T>  {
    fileprivate var array: [T] = []
    
    mutating func push(_ element: T) {
        array.append(element)
    }
    
    mutating func pop() -> T? {
        return array.popLast()
    }
    
    func peek() -> T? {
        return array.last
    }
    
    var isEmpty: Bool {
        return array.isEmpty
    }
    
    var count: Int {
        return array.count
    }
}
