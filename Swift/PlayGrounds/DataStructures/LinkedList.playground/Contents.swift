//: Playground - noun: a place where people can play

import UIKit

class Node {
    var next: Node?
    let data: Int   // 可以是 Int 类型的简单数据，也可以是 Hashmap 类的数据进行更复杂的操作(如 LRU)
    
    init(data: Int) {
        self.data = data
    }
}

class HashTableList {
    var head: Node?
    
    func append(data: Int) {
        let node = Node(data: data)

        if head == nil {
            head = node
            return
        }
        
        var current = head
        
        while current?.next != nil {
            current = current?.next
        }
        
        current?.next = node
    }
    
    func prepend(data: Int) {
        let node = Node(data: data)

        node.next = head
        head = node
    }
    
    func deleteWithValue(data: Int) {
        if head == nil {
            return
        } else if head?.data == data {
            head = head?.next
            return
        }
        
        var current = head
        
        while current?.next != nil {
            if current?.next?.data == data {
                current?.next = current?.next?.next
                return
            }
            current = current?.next
        }
    }
    
    func description() {
        var node = list.head
        while node != nil {
            if let data = node?.data {
                print(data, terminator: "->")
            }
            node = node?.next
        }
        print()
    }
}

let list = HashTableList()

list.append(data: 3)
list.append(data: 4)
list.prepend(data: 5)
list.prepend(data: 3)
list.description()

list.deleteWithValue(data: 5)
list.description()
