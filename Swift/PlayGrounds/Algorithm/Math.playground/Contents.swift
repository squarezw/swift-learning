//: Playground - noun: a place where people can play

import UIKit

// https://www.dotnetperls.com/math-swift

/**
 阶乘
 */
func factorial(n: Int) -> Int {
    if n == 0 {
        return 1
    }
    return n * factorial(n: n - 1)
}

factorial(n: 5)

// pow 默认输出为 decimal 类型，所以要转为 int 只有两种方式
// let n = Int(pow(10,0)) // ambiguous use of 'pow'
let n1 = Int(pow(Double(10),Double(0)))
let n2 = Int(pow(Float(10),Float(0)))

/**
 求个位数，十位数，百位数
 */
let x: Int = 12345
let pow10: (_ x: Int) -> Int = { (x) in
    return Int(pow(Double(10),Double(x)))
}

(x / pow10(0)) % 10 // 个位 10 的 0 次方
(x / pow10(1)) % 10 // 十位 10 的 1 次方
(x / pow10(2)) % 10 // 百位 10 的 2 次方
(x / pow10(3)) % 10 // 千位 10 的 3 次方

/*求最大公约数*/

func gcd(a: Int , b: Int) -> Int {
    if a % b != 0 {
        return gcd(a: b, b: a % b)
    }
    return b
}

gcd(a: 18, b: 12)

// 以 2 为底的 10 的对数
// 即: 2 的 3.3219 次方 = 10
log2(10.0)

UIImage(named: "q3Opc")

// 根据公式，你可以自行定义 log 几
// logb(x) = logd(x) / logd(b)
func log3(val: Double) -> Double {
    return log(val)/log(3.0)
}
log3(val: 10.0)

// 下表列出了这些底数的常用的对数符号以及他们所使用的领域。许多学科都写log(x)来代替logb(x)，而 b 的值根据前后文可以确定
log(10.0)
