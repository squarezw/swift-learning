import UIKit

/**
 Input: arr[]   = {2, 0, 2}
 Output: 2
 Structure is like below
 | |
 |_|
 We can trap 2 units of water in the middle gap.
 
 Input: arr[]   = {3, 0, 0, 2, 0, 4}
 Output: 10
 Structure is like below
 |
 |    |
 |  | |
 |__|_|
 We can trap "3*2 units" of water between 3 an 2,
 "1 unit" on top of bar 2 and "3 units" between 2
 and 4.  See below diagram also.
 
 Input: arr[] = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
 Output: 6
 |
 |   || |
 _|_||_||||||
 Trap "1 unit" between first 1 and 2, "4 units" between
 first 2 and 3 and "1 unit" between second last 1 and last 2
 */

UIImage(named: "watertrap")

// Wrong solution.
class Stack {
    var items = [Int]()
    
    func push(item: Int) {
        items.append(item)
    }
    
    func pop() -> Int {
        return items.removeLast()
    }
    
    func isEmpty() -> Bool {
        return items.count == 0
    }
}

let stk = Stack()
stk.push(item: 3)
stk.push(item: 4)
stk.push(item: 5)
stk.pop()
stk.pop()

func water(items: [Int]) -> Int {
    var tallest = 0
    let stack = Stack()
    var j = 0
    var result = 0
    
    while j < items.count {
        if items[j] <= tallest {
            stack.push(item: items[j])
            j += 1
        } else {
            while !stack.isEmpty() {
                result += tallest - stack.pop()
            }
            tallest = items[j]
            stack.push(item: items[j])
            j += 1
        }
    }
    
    return result
}

assert(water(items: [3, 0, 0, 2, 0, 4]) == 10, "should be 10")
//assert(water(items: [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1])
// above solution is not work, cuz 2,1,2 is also a gap to trap

/// 方法2， 先设定 Tallest 为你第一个，然后走第二个，如果第二个小于它，设置为 lowest,如果第三个数字小于 lowest 将第三个设置为 lowest； 如果大于 lowest ，判断是否大于 tallest ，如果大于将 tallest 做为最高值，与之前的每一个相减，得到的差值为填充数，以 tallest 为基准填满坑位；如果小于 tallest ，则将第三个设置为基准，填充每个坑位并记录。

/// Init tallest & lowest first, then go through array, if the next one taller than lowest, there should be a gap occurred, then compare back.

/// if this one is either taller existed tallest, set existed as baseline or smaller existed tallest, set this one as base line. to subtract each elements and fill each smaller elements until one is equal or bigger this tallest.

/// the fill value will be store to result incrementlly.
func trapWater(items: [Int]) -> Int {
    var items = items
    var tallest = items[0]
    var lowest = items[0]

    var j = 1
    var result = 0
    
    while j < items.count {
        if items[j] < lowest {
            lowest = items[j]
        } else if items[j] > lowest {
            if items[j] >= tallest {
                var i = j - 1
                while i >= 0 && items[i] < tallest {
                    result += tallest - items[i]
                    items[i] = tallest
                    i -= 1
                }
                tallest = items[j]
            } else {
                var i = j - 1
                while i >= 0 && items[i] < items[j] {
                    result += items[j] - items[i]
                    items[i] = items[j]
                    i -= 1
                }
                lowest = items[j]
            }
        }
        j += 1
    }
    return result
}

trapWater(items: [3, 0, 0, 2, 0, 4])
trapWater(items: [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1])

/// An element of array can store water if there are higher bars on left and right. We can find amount of water to be stored in every element by finding the heights of bars on left and right sides. The idea is to compute amount of water that can be stored in every element of array. For example, consider the array {3, 0, 0, 2, 0, 4}, we can store three units of water at indexes 1 and 2, and one unit of water at index 3, and three units of water at index 4.

/// A Simple Solution is to traverse every array element and find the highest bars on left and right sides. Take the smaller of two heights. The difference between smaller height and height of current element is the amount of water that can be stored in this array element. Time complexity of this solution is O(n2).

/// An Efficient Solution is to prre-compute highest bar on left and right of every bar in O(n) time. Then use these pre-computed values to find the amount of water in every array element. Below is C++ implementation of this solution.

func findWater(arr: [Int],_ n: Int) -> Int {
    // left[i] contains height of tallest bar to the
    // left of i'th bar including itself
    var left = [Int](repeating: 0, count: n)
    
    // Right [i] contains height of tallest bar to
    // the right of ith bar including itself
    var right = [Int](repeating: 0, count: n)
    
    var water = 0
    
    // Fill, left array
    left[0] = arr[0]
    for i in 1..<n {
        left[i] = max(left[i-1], arr[i])
    }
    
    // Fill, right array
    right[n - 1] = arr[n-1]
    for i in (0..<n-1).reversed() {
        right[i] = max(right[i+1], arr[i])
    }
    
    for i in 0..<n {
        water += min(left[i], right[i]) - arr[i]
    }
    
    return water
}

findWater(arr: [3, 0, 0, 2, 0, 4], 6)
findWater(arr: [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1], 12)

/// Complexity: O(n)
func find2Water(items:[Int], _ n: Int) -> Int {
    var result = 0
    
    var left_max = 0, right_max = 0
    
    var lo = 0, hi = n - 1
    
    while lo <= hi {
        if items[lo] < items[hi] {
            if items[lo] > left_max {
                // update max in left
                left_max = items[lo]
            } else {
                result += left_max - items[lo]
                lo += 1
            }
        } else {
            if items[hi] > right_max {
                // update right maximum
                right_max = items[hi]
            } else {
                result += right_max - items[hi]
                hi -= 1
            }
        }
    }
    
    return result
}

find2Water(items: [1,2,3,2,1], 5)
find2Water(items: [4,2,3,2,1], 5)
