//: 对称子字符串的最大长度
//:
//: 题目：输入一个字符串，输出该字符串中对称的子字符串的最大长度。比如输入字符串“google”，由于该字符串里最长的对称子字符串是“goog”，因此输出4。

//: 分析：可能很多人都写过判断一个字符串是不是对称的函数，这个题目可以看成是该函数的加强版。

//: 引子：判断字符串是否对称
//: 要判断一个字符串是不是对称的，不是一件很难的事情。我们可以先得到字符串首尾两个字符，判断是不是相等。如果不相等，那该字符串肯定不是对称的。否则我们接着判断里面的两个字符是不是相等，以此类推。基于这个思路，我们不难写出如下代码：

import UIKit

func isSymmetrical(chars: String) -> Bool {
    if chars.isEmpty || chars.first != chars.last {
        return false
    }
    
    var i = 0
    var j = chars.count - 1
    
    while i < j {
        if chars[chars.index(chars.startIndex, offsetBy: i)] != chars[chars.index(chars.startIndex, offsetBy: j)] {
            return false
        }
        
        i += 1
        j -= 1
    }
    
    return true
}

isSymmetrical(chars: "goog")
isSymmetrical(chars: "google")


/**
 解法二：O（n2）的算法
 如果我们换一种思路，我们从里向外来判断。也就是我们先判断子字符串A是不是对称的。如果A不是对称的，那么向该子字符串两端各延长一个字符得到的字符串肯定不是对称的。如果A对称，那么我们只需要判断A两端延长的一个字符是不是相等的，如果相等，则延长后的字符串是对称的。因此在知道A是否对称之后，只需要O（1）的时间就能知道aAa是不是对称的。
 
 我们可以根据从里向外比较的思路写出如下代码：
 
 ////////////////////////////////////////////////////////////////
 // Get the longest length of its all symmetrical substrings
 // Time needed is O(T^2)
 ////////////////////////////////////////////////////////////////
 int GetLongestSymmetricalLength_2(char* pString)
 {
 if(pString == NULL)
 return 0;
 
 int symmeticalLength = 1;
 
 char* pChar = pString;
 while(*pChar != '\0')
 {
 // Substrings with odd length
 char* pFirst = pChar - 1;
 char* pLast = pChar + 1;
 while(pFirst >= pString && *pLast != '\0' && *pFirst == *pLast)
 {
 pFirst--;
 pLast++;
 }
 
 int newLength = pLast - pFirst - 1;
 if(newLength > symmeticalLength)
 symmeticalLength = newLength;
 
 // Substrings with even length
 pFirst = pChar;
 pLast = pChar + 1;
 while(pFirst >= pString && *pLast != '\0' && *pFirst == *pLast)
 {
 pFirst--;
 pLast++;
 }
 
 newLength = pLast - pFirst - 1;
 if(newLength > symmeticalLength)
 symmeticalLength = newLength;
 
 pChar++;
 }
 
 return symmeticalLength;
 }
 由于子字符串的长度可能是奇数也可能是偶数。长度是奇数的字符串是从只有一个字符的中心向两端延长出来，而长度为偶数的字符串是从一个有两个字符的中心向两端延长出来。因此我们的代码要把这种情况都考虑进去。
 
 在上述代码中，我们从字符串的每个字符串两端开始延长，如果当前的子字符串是对称的，再判断延长之后的字符串是不是对称的。由于总共有O（n）个字符，每个字符可能延长O（n）次，每次延长时只需要O（1）就能判断出是不是对称的，因此整个函数的时间效率是O（n2）。
 */