//: Playground - noun: a place where people can play

/**
 http://taop.marchtea.com/04.03.html
 
 求数组中出现次数超过一半的元素
 给定一个n个整型元素的数组a，其中有一个元素出现次数超过n / 2，求这个元素。
 
 解法1:
 ------
 一个数字在数组中的出现次数超过了一半，那么在已排好序的数组索引的N/2处（从零开始编号
 
 如果 无序 ，那么我们是不是可以先把数组中所有这些数字 先进行排序 （至于排序方法可选取最常用的快速排序）。排完序后，直接遍历，在遍历整个数组的同时统计每个数字的出现次数，然后把那个出现次数超过一半的数字直接输出，题目便解答完成了。总的时间复杂度为O(nlogn + n)。
 
 但 如果是有序的数组呢 ，或者经过排序把无序的数组变成有序后的数组呢？是否在排完序O(nlogn)后，还需要再遍历一次整个数组？
 
 我们知道，既然是数组的话，那么我们可以根据数组索引支持直接定向到某一个数。我们发现，一个数字在数组中的出现次数超过了一半，那么在已排好序的数组索引的N/2处（从零开始编号），就一定是这个数字。自此，我们只需要对整个数组排完序之后，然后直接输出数组中的第N/2处的数字即可，这个数字即是整个数组中出现次数超过一半的数字，总的时间复杂度由于少了最后一次整个数组的遍历，缩小到O(n*logn)。
 
 然时间复杂度并无本质性的改变，我们需要找到一种更为有效的思路或方法。
 
 解法2:
 ------
 设置一个当前值和当前值的计数器，初始化当前值为数组首元素，计数器值为1，然后从第二个元素开始遍历整个数组，对于每个被遍历到的值a[i]
 
 1 如果a[i]==currentValue，则计数器值加1
 
 2 如果a[i] != currentValue， 则计数器值减1，如果计数器值小于0，则更新当前值为a[i]，并将计数器值重置为1
 
 
 在上述代码中，有两点值得讨论：
 （1）我们需要考虑当输入的数组或者长度无效时，如何告诉函数的调用者输入无效。关于处理无效输入的几种常用方法，在本博客系列的第17题中有详细的讨论；
 （2）本算法的前提是输入的数组中的确包含一个出现次数超过数组长度一半的数字。如果数组中并不包含这么一个数字，那么输入也是无效的。因此在函数结束前我还加了一段代码来验证输入是不是有效的。
 
 */
import UIKit


// 解法2
func findNumber(a:[Int]) -> Int {
    let n = a.count
    
    var curValue = a[0]
    var count = 1
    
    for i in 1..<n {
        if curValue == a[i] {
            count += 1
        } else {
            count -= 1
            if count < 0 {
                curValue = a[i]
                count = 1
            }
        }
    }
    
    // verify whether the input is valid
    // 验证是否真的存在这样一个数字
    count = 0;
    for i in 0..<n {
        if a[i] == curValue {
            count += 1
        }
    }
    
    if count * 2 <= n {
        curValue = 0
    }
    
    
    return curValue
}

findNumber(a: [1,2,3,4,3,3,3])
findNumber(a: [5,2,3,1,1]) // 没这样的数字，长度大于一半

// 举一反三
//
// 找出出现次数刚好是一半的数字
//
// 根据上面的例子，最后我们可能会输出不是符合条件的数字，那么仔细分析的话，占一半的数字，只能在两个变量中出现：candidate和arr[n-1]。如果arr[n-1]不是占一半的数据key，那么candidate最后保持着key，另一种情况，就是arr[n-1]为key。我们遍历到最后，再遍历一趟判断一下是否arr[n-1]占据一半即可。

// PS: 如果把数组长度减1，即可能为上面出现超过一半的情况类型，另一种就是那个数在未尾还出现了一次。所以根据这个就可以去判断了

// 改进：

// 我们再遍历的过程中，让每一个数据与arr[n-1]比较，统计和arr[n-1]相同的数据，那么到最后就不用再遍历了，代码如下：

func findNumberA(a:[Int]) -> Int {
    let n = a.count
    var candidate = a[0]
    var sumLast = 0 // 最后一个元素的个数
    var sumCan = 0 // 候选数字元素个数
    
    for i in 0..<n-1 {
        if a[i] == a[n-1] {
            sumLast += 1
        }
        
        // 下面的逻辑和上面的一样
        if sumCan == 0 {
            candidate = a[i]
            sumCan += 1
        } else {
            if a[i] == candidate {
                sumCan += 1
            } else {
                sumCan -= 1
            }
        }
    }
    
    if sumLast+1 == n / 2 {
        return a[n-1]
    } else {
        return candidate
    }
}


findNumberA(a: [0,1,2,1])
findNumberA(a: [0,5,5,1,5,2])
findNumberA(a: [1,2,2,1,5,2])


