//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport

/// 求两个矩形相交的面积及 frame

let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
view.backgroundColor = UIColor.gray

PlaygroundPage.current.liveView = view

let r1 = UIView(frame: CGRect(x: 30, y: 0, width: 50, height: 50))
r1.backgroundColor = UIColor.brown

let r2 = UIView(frame: CGRect(x: 50, y: 20, width: 50, height: 50))
r2.backgroundColor = UIColor.darkGray
r2.alpha = 0.7

view.addSubview(r1)
view.addSubview(r2)

/**
 
 - Parameter K: left x coordinate   A
 - Parameter L: top y coordinate    A
 - Parameter M: right x coordinate  A
 - Parameter N: bottom y coordinate A
 - Parameter P: left x coordinate   B
 - Parameter Q: top y coordinate    B
 - Parameter R: right x coordinate  B
 - Parameter S: bottom y coordinate B
 
 - a----b
 - |    |
 - |    |
 - c----d
 */
func intersectArea(K: CGFloat, L: CGFloat, M: CGFloat, N: CGFloat, P: CGFloat, Q: CGFloat, R: CGFloat, S: CGFloat) -> Float {
    let left = max(K, P)    // -> a
    let right = min(M, R)   // -> b
    let bottom = max(L, Q)  // -> c
    let top = min(N, S)     // -> d
    
    let si = max(0, right - left) * max(0, bottom - top) // width * height = area
    
    return Float(si)
}

func intersectFrame(K: CGFloat, L: CGFloat, M: CGFloat, N: CGFloat, P: CGFloat, Q: CGFloat, R: CGFloat, S: CGFloat) -> CGRect {
    let left = max(K, P)    // -> a
    let right = min(M, R)   // -> b
    let bottom = max(L, Q)  // -> c
    let top = min(N, S)     // -> d
    
    return CGRect(x: left, y: top, width: right - left, height: bottom - top)
}


let rect = intersectFrame(K: r1.frame.origin.x, L: r1.frame.origin.y, M: r1.frame.maxX, N: r1.frame.maxY,
                          P: r2.frame.origin.x, Q: r2.frame.origin.x, R: r2.frame.maxX, S: r2.frame.maxY)

let v = UIView(frame: rect)
v.backgroundColor = UIColor.orange
view.addSubview(v)