//: Playground - noun: a place where people can play

import UIKit

// Longest common subsequence of s1 and s2
extension String {
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }

    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }

    subscript (i: Int) -> String {
        let start = index(startIndex, offsetBy: i)
        let end = index(startIndex, offsetBy: i+1)
        return String(self[start..<end])
    }
}

func lcs(_ P: String,_ Q: String,_ n: Int,_ m: Int, mem: inout [[String?]]) -> String {
    var result: String = ""
    if let result = mem[n][m] {
        return result
    }

    if n > P.count - 1 || m > Q.count - 1 {
        result = ""
    } else if P[n] == Q[m..<m+1] {
        result = P[n] + lcs(P, Q, n+1, m+1, mem: &mem)
    } else{
        let tmp1 = lcs(P, Q, n+1, m, mem: &mem)
        let tmp2 = lcs(P, Q, n, m+1, mem: &mem)
        result += tmp1.count > tmp2.count ? tmp1 : tmp2
    }
    mem[n][m] = result
    return result
}

var memo: [[String?]] = Array(repeating: Array(repeating: nil, count: 8), count: 7)
lcs("AGGTAB", "GXTXAYB", 0, 0, mem: &memo)

