//: Playground - noun: a place where people can play

import Cocoa

// 将数组里的元素随机排列
var s: [Int] = [2,1,9,3]

for _ in 0...5 {
    for index in 1..<s.endIndex {
        let i = s.endIndex - index
        let x = Int.random(in: 0..<i)
        s.swapAt(i, x)
    }
    
    print(s)
}
