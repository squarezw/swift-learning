//: Playground - noun: a place where people can play

import UIKit

/// Knapsack
///


let w = [ 1,  3,  5,  7,  9]
let v = [10, 30, 20, 50, 100]

/// Knapsack Problem
///
/// - Parameters:
///   - n: index
///   - c: capacity
///
/// ex:
///   - w: [1,3,5,7,9]
///   - v: [10,30,20,50,100]
///   - c: 8
///   - result: 60
func KS(n: Int, c: Int) -> Int {
    if n < 0 || c == 0 {
        return 0
    } else if (w[n] > c) {
        return KS(n: n - 1, c: c)
    } else {
        let tmp1 = KS(n: n - 1, c: c)
        let tmp2 = v[n] + KS(n: n - 1, c: c - w[n])
        return max(tmp1, tmp2)
    }
}

KS(n: w.count - 1, c: 8)

/// memorize
var mem: [[Int?]] = Array(repeating: Array(repeating: nil, count: 100), count: 100)

func KS2(n: Int, c: Int) -> Int {
    if let value = mem[n][c] {
        return value
    }

    if n >= w.count || c == 0 {
        return 0
    } else if (w[n] > c) {
        return KS2(n: n + 1, c: c)
    } else {
        let tmp1 = KS2(n: n + 1, c: c)
        let tmp2 = v[n] + KS2(n: n + 1, c: c - w[n])
        mem[n][c] = max(tmp1, tmp2)
        return max(tmp1, tmp2)
    }
}

KS2(n: 0, c: 8)
