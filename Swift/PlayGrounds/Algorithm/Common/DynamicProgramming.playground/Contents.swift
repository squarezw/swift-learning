//: Playground - noun: a place where people can play

import UIKit

/**
 MEMOIZATION and Dynamic Programming
 
 https://youtu.be/P8Xa2BitN3I
 
                                fib(6)
 
                    fib(5)                  fib(4)
 
            fib(4)          fib(3)
 
        fib(3)  fib(2)
 
    fib(2) fib(1)
 
fib(1) fib(0)

// fib(n) = fib(n-1) + fib(n-2)
 */
func fib(_ n: Int, memo: inout [Int:Int]) -> Int {
    if n <= 0 {
        return 0
    } else if (n == 1) {
        return 1
    } else if (memo[n] == nil){
        memo[n] = fib(n-1, memo: &memo) + fib(n-2, memo: &memo)
    }
    
    return memo[n]!
}

var memo: [Int:Int] = [:]

fib(7, memo: &memo)


/**
 利用上面的实例可以用在 Grid 迷宫算出最佳解法上
 
 比如 有一个 grid
 
 -----------------------------
 | A |   |   |   |   |   |   |
 |   | * | * |   |   |   |   |
 |   |   |   | * |   |   |   |
 | * |   |   |   |   | * |   |
 |   |   | * |   |   |   |   |
 | * |   | * | * |   |   |   |
 |   |   |   |   |   |   | B | <-- end
 -----------------------------
 
 从 Person A 走到 End 可能的路径有哪些
 */

// navie 的方法
func countPaths(grid: [[Int]], row: Int, col: Int) -> Int {
    if (!validSquare(grid, row, col)) {
        return 0
    }
    if (isAtEnd(grid, row, col)) {
        return 1
    }
    return countPaths(grid: grid, row: row + 1, col: col) + countPaths(grid: grid, row: row, col: col + 1)
}

func validSquare(_ grid: [[Int]], _ row: Int, _ col: Int) -> Bool {
    if row < 0 || col < 0 {
        return false
    }
    if grid.count < row || grid[0].count < col {
        return false
    }
    
    return true
}

func isAtEnd(_ grid: [[Int]], _ row: Int, _ col: Int) -> Bool {
    if grid[0].count == col && grid.count == row {
        return true
    }
    return false
}

func countPaths(grid: [[Int]], row: Int, col: Int, paths: inout [[Int]]) -> Int {
    if (!validSquare(grid, row, col)) {
        return 0
    }
    if (isAtEnd(grid, row, col)) {
        return 1
    }
    
    if (paths[row][col] == 0) {
        paths[row][col] = countPaths(grid: grid, row: row + 1, col: col) + countPaths(grid: grid, row: row, col: col + 1)
    }
    return paths[row][col]
}

let n:[[Int]] = [Array(repeating: 0, count: 5),
                 Array(repeating: 0, count: 5),
                 Array(repeating: 0, count: 5)]

countPaths(grid: n, row: 1, col: 1)

// MEMO
var paths: [[Int]] = n

countPaths(grid: n, row: 1, col: 1, paths: &paths)

