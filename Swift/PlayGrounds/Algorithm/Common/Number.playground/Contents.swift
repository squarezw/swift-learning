//: Playground - noun: a place where people can play

import UIKit

// 得到有几位数字位数
func getNumOfDigits(n: Int) -> Int {
    var count = 0
    var nn = n
    while nn > 0 {
        nn = nn / 10
        count += 1
    }
    
    return count
}

getNumOfDigits(n: 12345)

// 寻找重复数
let x = [1, 1, 2, 3, 4, 5, 5]

print(x.filter({ (i: Int) in x.filter({ $0 == i }).count > 1}))


/**
 找出绝对值最小的元素
 
 给定一个有序整数序列（非递减序），可能包含负数，找出其中绝对值最小的元素，比如给定序列 -5, -3, -1, 2, 8 则返回1。
 
 分析
 
 由于给定序列是有序的，而这又是搜索问题，所以首先想到二分搜索法，只不过这个二分法比普通的二分法稍微麻烦点，可以分为下面几种情况
 
 如果给定的序列中所有的数都是正数，那么数组的第一个元素即是结果。
 如果给定的序列中所有的数都是负数，那么数组的最后一个元素即是结果。
 如果给定的序列中既有正数又有负数，那么绝对值得最小值一定出现在正数和负数的连接处。
 为什么？因为对于负数序列来说，右侧的数字比左侧的数字绝对值小，如上面的-5, -3, -1, 而对于整整数来说，左边的数字绝对值小，比如上面的2, 8，将这个思想用于二分搜索，可先判断中间元素和两侧元素的符号，然后根据符号决定搜索区间，逐步缩小搜索区间，直到只剩下两个元素。
 */
func sameSign(a:Int, b:Int) -> Bool {
    if a * b > 0 {
        return true
    } else {
        return false
    }
}

func minimumAbsoluteValue(a:[Int]) -> Int {
    if a.count == 1 {
        return a[0]
    }
    
    if sameSign(a: a[0], b: a[a.count - 1]) {
        return a[0] >= 0 ? a[0] : a[a.count - 1]
    }
    
    var l = 0
    var r = a.count - 1
    
    while l < r {
        if l + 1 == r {
            return abs(a[l]) < abs(a[r]) ? a[l] : a[r]
        }

        let m = (l + r) / 2
        if sameSign(a: a[m], b: a[r]) {
            r = m
            continue
        } else {
            l = m
            continue
        }
    }
    
    return a[l]
}

minimumAbsoluteValue(a: [-5,-3,-2,1,4])

minimumAbsoluteValue(a: [1,2,3,4,5])

minimumAbsoluteValue(a: [-1,1,2])
