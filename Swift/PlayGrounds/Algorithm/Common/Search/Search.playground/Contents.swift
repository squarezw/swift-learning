//: Playground - noun: a place where people can play

import UIKit

// Binary Search

// Solution 1

func binaryRecursive(array: [Int], x: Int, left: Int, right: Int) -> Bool {
    if left > right {
        return false
    }
    
    let mid = left + (right - left) / 2
    if array[mid] == x {
        return true
    } else if (x < array[mid]) {
        return binaryRecursive(array: array, x: x, left: left, right: mid - 1)
    } else {
        return binaryRecursive(array: array, x: x, left: mid + 1, right: right)
    }
}

func binaryIterative(array: [Int], x: Int) -> Bool {
    var left = 0
    var right = array.count - 1
    
    while left <= right {
        let mid = left + (right - left) / 2
        
        if array[mid] == x {
            return true
        } else if x < array[mid] {
            right = mid - 1
        } else {
            left = mid + 1
        }
    }
    
    return false
}

let arr = [1,2,5,13,24,26,27,30,31]

let number = arr[Int(arc4random()) % arr.count]

binaryRecursive(array: arr, x: number, left: 0, right: arr.count - 1)
binaryIterative(array: arr, x: arr.randomElement()!)

// Search cloest number
// 搜索最接近的数

/**
 0  1  2   3   4   5   6   7   8   9  <- indexes
 2 42 82 122 162 202 242 282 322 362  <- values
 L             M                   H  L=0, H=9, M=4, 162 higher, H<-M
 L     M       H                      L=0, H=4, M=2, 82 lower/equal, L<-M
       L   M   H                      L=2, H=4, M=3, 122 higher, H<-M
       L   H                          L=2, H=3, difference of 1 so exit
           ^
           |
           H (122-112=10) is closer than L (112-82=30) so choose H
 */
func closestNumber(arr: [Int], num: Int) -> Int {
    var mid: Int
    var lo = 0
    var hi = arr.count - 1
    
    while hi - lo > 1 {
        mid = (lo + hi) / 2
        if arr[mid] < num {
            lo = mid
        } else {
            hi = mid
        }
    }
    if num - arr[lo] <= arr[hi] - num {
        return arr[lo]
    }
    return arr[hi]
}

let array = [2, 42, 82, 122, 162, 202, 242, 282, 322, 362]
let n = 112

closestNumber(arr: array, num: n)
