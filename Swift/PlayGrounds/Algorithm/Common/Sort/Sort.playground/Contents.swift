//: Playground - noun: a place where people can play
//: Array Sorting Algorithms

//: Algorithm              Time Complexity
//: Best            Average
//: Heapsort           Ω(n log(n))     Θ(n log(n))
//: Bubble Sort        Ω(n)            Θ(n^2)
//: Insertion Sort     Ω(n)            Θ(n^2)


import Cocoa

NSImage(named: "ArraySortingAlgorithms.png")

// 简单的排序，线性排序
// linear sort
func linearSort(n: inout [Int]) {
    for i in 0..<n.count {
        for j in i+1..<n.count {
            print(j,n[i],n[j], n)
            if n[i] > n[j] {
                n.swapAt(i, j)
            }
        }
    }
}

var n: [Int] = []

for _ in 0..<5 {
    n.append(Int(arc4random()) % 10)
}


linearSort(n: &n)


/// Quick Sort
/**
 HackerRank

 https://youtu.be/SLauY6PpjW4

 left       Pivot       right
 |           |           |
 9 , 2,  6,  4,  3,  5,  1

 1-->
 1 , 2,  6,  4,  3,  5,  9

 2-->
 1 , 2,  3,  4,  6,  5,  9

 ...
 */
func pivotSort(array: inout [Int], left: Int, right: Int) {
    if left >= right {
        return
    }
    let pivot = array[(left + right) / 2]
    let index = partition(array: &array, left, right, pivot)

    pivotSort(array: &array, left: left, right: index - 1)
    pivotSort(array: &array, left: index, right: right)
}

func partition(array: inout [Int], _ left: Int, _ right: Int, _ pivot: Int) -> Int {
    var left = left
    var right = right

    while left <= right {
        while array[left] < pivot {
            left += 1
        }

        while array[right] > pivot {
            right -= 1
        }

        if left <= right {
            array.swapAt(left, right)
            left += 1
            right -= 1
        }
    }

    return left
}


// 优化的解法
func partitionLomuto<T: Comparable>(_ a: inout [T], low: Int, high: Int) -> Int {
    let pivot = a[high]
    
    var i = low
    for j in low..<high {
        if a[j] <= pivot {
            (a[i], a[j]) = (a[j], a[i])
            i += 1
        }
    }
    
    (a[i], a[high]) = (a[high], a[i])
    return i
}

func quicksortLomuto<T: Comparable>(_ a: inout [T], low: Int, high: Int) {
    if low < high {
        let p = partitionLomuto(&a, low: low, high: high)
        quicksortLomuto(&a, low: low, high: p - 1)
        quicksortLomuto(&a, low: p + 1, high: high)
    }
}

var a: [Int] = [ 10, 0, 3, 9, 2, 14, 26, 27, 1, 5, 8, -1, 8 ]
var b = a


pivotSort(array: &b, left: 0, right: b.count - 1)

quicksortLomuto(&a, low:0, high: a.count - 1)






