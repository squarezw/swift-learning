//: Playground - noun: a place where people can play
//: 合并两个数组

import UIKit

/**
 给定含有n个元素的两个有序（非降序）整型数组a和b。合并两个数组中的元素到整型数组c，要求去除重复元素并保持c有序（非降序）。例子如下
 
 a = 1, 2, 4, 8
 
 b = 1, 3, 5, 8
 
 c = 1, 2, 3, 4, 5, 8
 
 利用合并排序的思想，三个指针i,j,k 分别指向数组 a,b,c，然后比较两个指针对应元素的大小，有以下三种情况
 
 1. a[i] < b[j]，则c[k] = a[i]。
 
 2. a[i] == b[j]，则c[k]等于a[i]或b[j]皆可。
 
 3. a[i] > b[j]，则c[k] = b[j]。
 
 重复以上过程，直到i或者j到达数组末尾，然后将剩下的元素直接copy到数组c中即可
 
 */

func sortedMerge(a: [Int], b: [Int]) -> [Int] {
    var i = 0
    var j = 0
    var c: [Int] = []
    
    while i < a.count && j < b.count {
        if a[i] < b[j] {
            c.append(a[i])
            i += 1
        } else if a[i] == b[j] {
            c.append(a[i])
            i += 1
            j += 1
        } else {
            c.append(b[j])
            j += 1
        }
    }
    if i < a.count {
        c.append(contentsOf: a[i..<a.endIndex])
    }
    
    if j < b.count {
        c.append(contentsOf: b[j..<b.endIndex])
    }
    
    return c
}

sortedMerge(a: [1,2,4,6,7,8], b: [1,3,5,8,9])


// Merge k sorted list
//
// http://bangbingsyb.blogspot.hk/2014/11/leetcode-merge-k-sorted-lists.html

/**
 方法1: Merge two lists
 
 利用合并两个list的方法，依次将每个list合并到结果的list中。这个方法的空间复杂度为O(1)，时间复杂度为：
 2n + 3n + ... + kn = [(k+1)*k/2-1]*n = O(nk^2)
 由于时间复杂度较大，大数据测试会超时。
 */

func mergeKLists(arr: [[Int]]) {
    var result: [Int] = []
    
    for i in 0..<arr.count {
        result = sortedMerge(a: result, b: arr[i])
    }
    
    print(result)
}


/**
 方法2: 二分法
 
 类似merge sort，每次将所有的list两两之间合并，直到所有list合并成一个。如果用迭代而非递归，则空间复杂度为O(1)。时间复杂度：
 2n * k/2 + 4n * k/4 + ... + (2^x)n * k/(2^x) = nk * x
 k/(2^x) = 1 -> 2^x = k -> x = log2(k)
 所以时间复杂度为O(nk log(k))，与方法一相同。
 */
func mergeKLists2(arr: [[Int]]) {
    var result: [[Int]] = arr
    
    var end = arr.count - 1
    
    while end > 0 {
        var begin = 0
        while begin < end {
//            print(begin, end)
            result[begin] = sortedMerge(a: result[begin], b: result[end])
            begin += 1
            end -= 1
        }
    }
    
    print(result.first!)
}

var kk = [[1,3,5,7], [2,4,6,7], [0,9,10,11], [1,12,13,14]]

mergeKLists(arr: kk)

mergeKLists2(arr: kk)






