//: Playground - noun: a place where people can play

import UIKit

/// Complexity: O(a+b+c)
///     a, b, c is size of each sorted list
///
/// they are should be sorted list, if not, make sure each of them are sorted and then use them,
/// otherwise the satisfied algorithm shoud be a*b*c.
func intersection(a: [Int], b: [Int], c: [Int]) -> [Int] {
    var result: [Int] = []
    var indexA: Int = 0
    var indexB: Int = 0
    var indexC: Int = 0

    while indexA < a.count && indexB < b.count && indexC < c.count {
        if a[indexA] == b[indexB] && b[indexB] == c[indexC] {
            result.append(a[indexA])
            indexA += 1
            indexB += 1
            indexC += 1
        } else if a[indexA] < b[indexB] {
            indexA += 1
        } else if b[indexB] < c[indexC] {
            indexB += 1
        } else {
            indexC += 1
        }
    }

    return result
}

intersection(a: [1,2,5,6,8], b: [1,2,6], c: [1,3,6])
