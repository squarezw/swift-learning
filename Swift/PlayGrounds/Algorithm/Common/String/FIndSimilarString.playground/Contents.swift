// if L contains each words in W, returns count of contains words
/**
 i.e
 
 "doog" is not familiar wtih "good"
 "love" is familiar "ove"
 "love" is familiar "ovel"
 "love" is not familiar "lowe"
 
 */

let L = "love" // readLine()!
let W = "lev voe olve lowe" // readLine()!


var chars = [Character](L)
var words = W.split {$0 == " "}.map(String.init)

func naiveSolution(_ chars: [Character], _ words:[String]) -> Int {
    var count = 0
    
    for word in words {
        var isContains = false
        for j in word {
            if !chars.contains(j) {
                isContains = false
                break
            } else {
                isContains = true
            }
        }
        if isContains {
            count += 1
        }
    }
    
    return count
}

print(naiveSolution(chars, words))
