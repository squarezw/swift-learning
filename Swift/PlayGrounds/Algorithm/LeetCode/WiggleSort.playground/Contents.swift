//: Playground - noun: a place where people can play

import UIKit

/**
 Given an unsorted array nums, reorder it in-place such that nums[0] <= nums[1] >= nums[2] <= nums[3]....

 For example, given nums = [3, 5, 2, 1, 6, 4], one possible answer is [1, 6, 2, 5, 3, 4].
 
 摇摆排序
 
 我们可以先将数组排序，这时候从第3个元素开始，将第3个元素和第2个元素交换。然后再从第5个元素开始，将第5个元素和第4个元素交换，以此类推。就能满足题目要求。
 */

func wiggleSort(a: [Int]) -> [Int] {
    var sortedArray = a.sorted()
    
    for i in stride(from: 2, to: sortedArray.count, by: 2) {
        sortedArray.swapAt(i, i-1)
    }
    
    return sortedArray
}

wiggleSort(a: [3,5,2,1,6,4])


/**
 
 */
func wiggleSort2(a: [Int]) -> [Int] {
    var a = a
    for i in 1..<a.count {
        if (i % 2 == 1 && a[i] < a[i-1]) || (i % 2 == 0 && a[i] > a[i-1]) {
            a.swapAt(i, i-1)
        }
    }
    return a
}

wiggleSort2(a: [3,1,2,4,6])


func wiggleSort3(a: [Int]) -> [Int] {
    var sorted = a.sorted()
    var i = (sorted.count - 1) / 2, j = sorted.count - 1
    for k in 1..<a.count {
        if k % 2 == 0 {
            i -= 1
            sorted[k] = sorted[i]
        } else {
            j -= 1
            sorted[k] = sorted[j]
        }
    }
    
    return sorted
}

wiggleSort3(a: [3,1,2,4,6])


// follow up 

/**
 
 http://www.geeksforgeeks.org/rearrange-array-maximum-minimum-form-set-2-o1-extra-space/
 
 
 Rearrange an array in maximum minimum form | Set 2 (O(1) extra space)
 Given a sorted array of positive integers, rearrange the array alternately i.e first element should be the maximum value, second minimum value, third-second max, fourth-second min and so on.
 
 Examples:
 
 Input  : arr[] = {1, 2, 3, 4, 5, 6, 7}
 Output : arr[] = {7, 1, 6, 2, 5, 3, 4}
 
 Input  : arr[] = {1, 2, 3, 4, 5, 6}
 Output : arr[] = {6, 1, 5, 2, 4, 3}
 
 
 ======
 
 
 even index : remaining maximum element.
 odd index  : remaining minimum element.
 
 max_index : Index of remaining maximum element
 (Moves from right to left)
 max_index : Index of remaining minimum element
 (Moves from left to right)
 
 Initialize: max_index = 'n-1'
             min_index = 0
             max_element = arr[max_index] + 1
 
 For i = 0 to n-1
    If 'i' is even
        arr[i] += arr[max_index] % max_element * max_element
        max_index--
    ELSE // if 'i' is odd
        arr[i] +=  arr[min_index] % max_element * max_element
        min_index++
 
 How does expression “arr[i] += arr[max_index] % max_element * max_element” work ?
 The purpose of this expression is to store two elements at index arr[i]. arr[max_index] is stored as multiplier and “arr[i]” is stored as remainder. For example in {1 2 3 4 5 6 7 8 9}, max_element is 10 and we store 91 at index 0. With 91, we can get original element as 91%10 and new element as 91/10.
 
 1. 首先从 right to left , left to right 遍历
 
 2. 这里有一个 trick .它没有用交互法，而是用了去模当中间变量，它取了一个比这里面任意一个元素都要大的值，做为取模的分母，这样余数肯定是当前的元素，然后再与数组中每个元素相乘，最后再把它们除掉。就得到了最新的值。
 
    公式是：(n * m + n) % m = n  如果想将一个数字变成另一个数字，而且想变回来最简单的办法就是如此。-> % m。
 
    好处是：不改变当前的顺序结构，只是将各自的数加权到各个数上。这样既保证了原有的顺序不变，又可以将我们想要的结果反应出来。
 */

/// 原始方法用的是
///  func rearrage(arr: inout [Int], n: Int)
///  n = 数据的长度。但有一个问题是，如果数据的长度并不一定是数组的最大值，也就是说有如果数组里的最大值不是等于 n 的话，就傻眼了，所以我这里改 n 为 max_number > max(value in the array)



/// 
/// 原始方法用的是
///
/// `func rearrage(arr: inout [Int], n: Int)`
///
/// 这里的 n = 数据的长度。
/// 但有一个问题是，如果数据的长度并不一定是数组的最大值，也就是说有如果数组里的最大值不是等于 array.length 的话，就傻眼了，所以我这里改 n 为 max_number > max(value in the array)

/// - Parameters:
///   - arr: [Int]
///   - max_number: 比数组元素中所有的值都大的值
func rearrage(arr: inout [Int], max_number: Int) {
    // initialize index of first minimum and first
    // maximum element
    var max_idx = arr.count-1 , min_idx = 0
    
    // store maximum element of array
    let max_elem = max_number // 可以是做任意比所有元素都大的数字即可
    
    // traverse array elements
    for i in 0..<arr.count {
        // at even index: we have to put maximum element
        if i % 2 == 0 {
            arr[i] += (arr[max_idx] % max_elem) * max_elem
            max_idx -= 1
        }
            
        // at odd index: we have to put minimum element
        else {
            arr[i] += (arr[min_idx] % max_elem) * max_elem
            min_idx += 1
        }
    }
    
    
    // array elements back to it's original form
    for i in 0..<arr.count {
        arr[i] = arr[i] / max_elem
    }
}

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
rearrage(arr: &arr, max_number: arr.max()! + 1)

var arr2 = [1, 7, 3, 4, 5, 6] // 即使数据是无序的，也是保持原有的顺序输出。
rearrage(arr: &arr2, max_number: arr2.max()! + 1)

