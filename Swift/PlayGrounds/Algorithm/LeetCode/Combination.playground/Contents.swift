//: 组合
//:
//: 由于侧重点在输入数据无重复，所以先看输入数据无重复类型
//: http://m.blog.csdn.net/article/details?id=8315418

import UIKit

func factorial(n: Int) -> Int {
    if n == 0 { return 1 }
    return n * factorial(n: n - 1)
}

/**
 求 combination 数量
 
 ABCDE
 
 以上有 5! 的组合
 
 如果是 AABCD 要去掉 2! 的重复数据 5! / 2! = 60
 
 如果是 AAABC 要去掉 3! 的重复数据 5! / 3! = 20
 
 如果是 AABBC 要去掉 2!2! 的重复数据 5! / 2!2! = 30
 */
func combinationCount(_ n: Int) -> Int {
    var count = 1
    var numberCount = 0
    var nn = n
    var numbers: [Int] = []
    var dupNumbers: [Int:Int] = [:]
    
    while nn > 0 {
        let x = (n / Int(pow(Double(10),Double(numberCount)))) % 10
        numberCount += 1
        
        if numbers.contains(x) {
            
            if let index = dupNumbers.index(forKey: x) {
                dupNumbers[x] = dupNumbers[index].value + 1
            } else {
                dupNumbers[x] = 2
            }
            
        } else {
            numbers.append(x)
            
            count *= numberCount
        }
        
        nn = nn / 10
    }
    
    print(numberCount, dupNumbers)
    
    count = factorial(n: numberCount)
    
    for (_,v) in dupNumbers {
        count = count / factorial(n: v)
    }
    
    return count
}

combinationCount(112344)

// 输入两个整数 n 和 m，从数列1，2，3.......n 中 随意取几个数，使其和等于 m ,要求将其中所有的可能组合列出来
// 如m =5,n=4 输出14，23

// 这种问法是典型01背包问题，因为要求是输出所有组合，所以我们不用DP，而用回溯
// 算法思想：从最大数n开始尝试装包，输出所有情况，再尝试n不装包，输出所有情况。

func combine(m: Int, n:Int) {
    var n = n
    
    if m < 1 || n < 1 {
        return
    }
    
    if n > m {
        n = m
    }
    let b = Array(repeatElement(false, count: n+1))
    combination(m: m, n: n, b: b)
}

func combination(m:Int, n:Int, b: [Bool]) {
    var b = b
    
    if m < 1 || n < 1 {
        return
    }
    
    if m == n {
        b[n] = true
        print("")
        for i in 1..<b.count {
            if b[i] == true {
                print(i, terminator: "+")
            }
        }
        
        b[n] = false
    }
    b[n] = true // 装包
    combination(m: m-n, n: n-1, b: b)
    b[n] = false // 不装包
    combination(m: m, n: n-1, b: b)
}

combine(m: 10, n: 12)

