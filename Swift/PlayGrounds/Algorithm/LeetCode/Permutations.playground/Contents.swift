//: Permutation
//: 排列
//:
//: 排列组合在数学中占有重要的地位，其与概率论也有密切关系
//:
//: 排列组合的对象无外是整形数组或字符数组，可以按输入数据分为两大类：输入数据有重复和无重复，又可以按输出数据分为两大类：输出数据有重复和无重复；而排列问题也偶尔会考非递归。
//: 首先提一下全排列的几种算法：
//:　　1——字典序法
//:　　2——递增进位数制法;
//:　　3——递减进位数制法
//:　　4——邻位交换法
//:　　5——n进制数法
//:　　6——递归生成法
//:　　7——循环移位法
//:　　8——回溯法
//:
//: http://m.blog.csdn.net/article/details?id=8351611

import Cocoa

/*
 先看输入数据无重复类型
 
 又可以分为"全排列"和"分组后排列"
 
 基本的全排列:
 */

/**
 输出数组的全排列(不可重复取)
 
 [1,2,3]
 
 i.e:
 [
 [1,2,3],
 [1,3,2],
 [2,1,3],
 [2,3,1],
 [3,1,2],
 [3,2,1]
 ]

 */

// naive way
func perm<T:Hashable>(nums n: [T], list: inout [[T]], tempList: inout [T]) {
    let nums = n
    
    if tempList.count == nums.count {
        list.append(tempList)
        return
    }
    
    for i in 0..<nums.count {
        if tempList.contains(nums[i]) {
            continue
        }
        
        tempList.append(nums[i])
        perm(nums: nums, list: &list, tempList: &tempList)
        tempList.remove(at: tempList.count - 1)
    }
}

NSImage(named: NSImage.Name("NewPermutation.gif"))

// 算法思想：可以输出1加上23的全排列，2加13的全排列，3加上12的全排列，运用递归求比如23的全排列..依次递归下去；比如现在要2开头求全排，首先要交换1，2的位置，让a[0]变为2，在用递归求13的所有全排列，前面加个2就是2开头的所有全排列了，最后运用回溯再把1，2调换回来。
func perm2<T: Comparable>(_ a:[T], _ index: Int) {
    var a = a
    if index == a.count - 1 {
        print(a)
    } else {
        for i in index..<a.count {
            // 去除重复元素, 这个去重还是有 Bug 的。
            if a[index..<i].contains(a[i]) {
                continue
            }
            if index != i { a.swapAt(index, i) }
            perm2(a, index+1)
            if index != i { a.swapAt(index, i) }
        }
    }
}



// 更优解
// https://github.com/raywenderlich/swift-algorithm-club/tree/master/Combinatorics

func permuteWirth<T>(_ a: [T], _ n: Int) {
    if n == 0 {
        print(a)   // display the current permutation
    } else {
        var a = a
        permuteWirth(a, n - 1)
        for i in 0..<n {
            a.swapAt(i, n)
            permuteWirth(a, n - 1)
            a.swapAt(i, n)
        }
    }
}

permuteWirth(["x","y","z"], 2)

/**
 输出数组的全排列(可重复取)
 
 [1,2] => [11,12,21,22]
 
 */

// 算法思想：用一个辅助空间b数组存储待输出的排列，用一个参数index记录一个排列的个数
func permDup<T>(_ a: [T], _ b: [T], index: Int) {
    var b = b
    if index == a.count {
        return
    }
    
    for i in 0..<a.count {
        b[index] = a[i]
        permDup(a, b, index: index + 1)
    }
}

/**
 输出从数组a中取n个数的所有排列
 
 [1,2,3] n=2 => [12，21，13，31，23，32]
 
 这可以看作是排列组合的综合题
 
 */

// 算法思想：求出数组中选取n个数的所有组合，分别对其进行全排列。
// n: 多少个一组
func comb<T: Comparable>(_ a: [T], _ b: [T], n: Int, begin: Int, index: Int) {
    var b = b
    // 够一达了
    if n == 0  {
        perm2(b, 0)
        return
    }
    
    for i in begin..<a.count {
        b[index] = a[i]
        comb(a, b, n: n - 1, begin: i+1, index: index + 1)
    }
}



/**
 输入数据有重复类型：
 
 输出数组的全排列
 
 如[1,1,2] 输出 112,121,211
 
 */

// 算法思想：我们改进一下1的算法，在for中判断是否有包含重复元素，也就是index和i之间是否有和a[i]相等的值，比如对于2313这个数列，当index=0(a[index] = 2),i=3(a[i] = 3)的时候,如果要交换这两个数变成3312的话就是计算重复了，因为它们之间有1个3，当i=1的时候，它已经转换过3312了。所以加一个函数判断中间有没有包含重复元素，如有没有重复元素，再做交换。

// PS: 上面的算法思想，还是有 Bug ,如何是 1121 或 1112 就不 Work 了, 不如在最后返回结果时直接放入 Hash Set


// 参照上面的方法 perm 实现，加上下面的判断
func contains<T: Comparable>(_ a:[T], m: Int, n: Int) -> Bool {
    for i in m..<n {
        if a[i] == a[n] {
            return true
        }
    }
    return false
}

/**
 给一个数组 nums，求全排列
  n = [1,2,3]
 答：[[1, 2, 3], [1, 2], [1, 3], [1], [2, 3], [2], [3], []]
 */

var ans:[Any] = [];
var t:[Int] = [];

func dfs(_ curr: Int, nums: [Int]) {
    if (curr == nums.count) {
        ans.append(t)
        return
    }
    
    t.append(nums[curr])
    dfs(curr + 1, nums: nums)
    t.popLast()
    print(curr, t)
//    dfs(curr + 1, nums: nums)    
}

dfs(0, nums: [1,2,3])

print(ans)


/**
 输出数组的全排列，重复或不重复 (非递归)
 
 代码有点多，不贴了，非递归直接跪 ~
 
 ......
 */


//
// ================= Test =================

//let sample = ["A","B","C"]
let sample = [1,1,2]
//let sample = [1,2,3]
//let sample = ["1","2","3","4"]

var b: [Int]

b = Array(repeatElement(0, count: sample.count))
permDup(sample, b, index: 0)

var n = 3 // 两个一组
b = Array(repeatElement(0, count: n))
comb(sample, b, n: n, begin:0, index: 0)

//perm2(sample, 0)
//perm3(sample, sample.count - 1)

//var result: [[String]] = [[String]]()
//var ff = [String]()
//var recursion = 1
//perm(nums: sample, list: &result, tempList: &ff)
//print(result)


// 用 Stack 方法 Perm
// 还可去重
//
// Print all distinct permutations of a given string with duplicates
// http://www.geeksforgeeks.org/print-all-permutations-of-a-string-with-duplicates-allowed-in-input-string/

//func stringPermutation(str: [String]) {
//    let size = str.count
//    
//    let str = str.sorted()
//    
//    var isFinished = false
//    
//    while !isFinished {
//        
//        // print this permutation
//        var x = 1
//        print(str)
//        
//        // Find the rightmost character which is smaller than its next
//        // character. let us call it 'first char'
//        var i = 0
//        
//        for i in stride(from: size-2, to: 0, by: -1) {
//            if str[i] < str[i+1] {
//                break
//            }
//        }
//        
//        if (i == -1) {
//            isFinished = true
//        } else {
//            let ceilIndex = findCeil(str, str[i], i+1, size-1)
//            
//            swap(&str[i], &str[ceilIndex])
//            
//            
//        }
//        
//        
//    }
//}


// 精典字符串 Permutation 方法
func stringPermutation(prefix: String, str: String, result: inout Set<String>) {
    let n = str.count
    if n == 0 {
        result.insert(prefix)
    } else {
        for i in 0..<n {
            stringPermutation(prefix: prefix + str[i], str: str[0..<i] + str[i+1..<n], result: &result)
        }
    }
}

var tmp: Set<String> = []

stringPermutation(prefix: "", str: "1121", result: &tmp)

print(tmp.sorted())
