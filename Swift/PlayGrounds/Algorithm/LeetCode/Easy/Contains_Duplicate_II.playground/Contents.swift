import UIKit

/**
 https://leetcode.com/problems/contains-duplicate-ii/
 
 Given an array of integers and an integer k, find out whether there are two distinct indices i and j in the array such that nums[i] = nums[j] and the absolute difference between i and j is at most k.
 
 Example 1:
 
 Input: nums = [1,2,3,1], k = 3
 Output: true
 Example 2:
 
 Input: nums = [1,0,1,1], k = 1
 Output: true
 Example 3:
 
 Input: nums = [1,2,3,1,2,3], k = 2
 Output: false
 */

class Solution {
    // Naive solution
    func containsNearbyDuplicateA(_ nums: [Int], _ k: Int) -> Bool {
        for i in 0..<nums.count {
            var j = i + 1
            while j <= i + k && j < nums.count {
                if nums[i] == nums[j] {
                    return true
                }
                j += 1
            }
        }
        
        return false
    }
    
    // Optimized
    func containsNearbyDuplicateB(_ nums: [Int], _ k: Int) -> Bool {
        var mm: [Int: Int] = [:]
        
        for i in 0..<nums.count {
            if let num = mm[nums[i]] {
                if i - num <= k {
                    return true
                } else {
                    mm[nums[i]] = i
                }
            } else {
                mm[nums[i]] = i
            }
        }
        
        return false
    }
    
    // Best
    func containsNearbyDuplicateC(_ nums: [Int], _ k: Int) -> Bool {
        var set = Set<Int>()
        
        for i in 0..<nums.count {
            if i > k {
                set.remove(nums[i - k - 1])
            }
            if !set.insert(nums[i]).inserted {
                return true
            }
        }
        
        return false
    }
}

Solution().containsNearbyDuplicateA([1,0,1,1], 1)
Solution().containsNearbyDuplicateA([1,2,3,1,2,3], 2)

Solution().containsNearbyDuplicateB([1,0,1,1], 1)
Solution().containsNearbyDuplicateB([1,2,3,1,2,3], 2)

Solution().containsNearbyDuplicateC([1,0,1,1], 1)
Solution().containsNearbyDuplicateC([1,2,3,1,2,3], 2)
