//: Playground - noun: a place where people can play

import UIKit

// https://www.youtube.com/watch?v=XKu_SEDAykw&t=398s

// 1. 给定一个排序过的数组 返回是否存在两个数之合等于 Sum

func hasPairWithSumByOrdered(data: [Int], sum: Int) -> Bool {
    var low = 0
    var high = data.count - 1
    
    while low < high {
        let k = data[low] + data[high]
        if k == sum {
            return true
        } else if k > sum {
            high -= 1
        } else {
            low += 1
        }
    }
    
    return false
}

hasPairWithSumByOrdered(data: [1,2,4,8], sum: 8)
hasPairWithSumByOrdered(data: [1,2,3,5], sum: 8)


// 2. 给定一个未排序数组，返回是否存在两个数之合等于 Sum

func hasPairWithSum(data: [Int], sum: Int) -> Bool {
    var comp: Set<Int> = Set();
    
    for i in data {
        if comp.contains(i) {
            return true
        }
        comp.insert(sum - i)
    }
    
    return false
}

hasPairWithSum(data: [1,2,4,4], sum: 8)
hasPairWithSum(data: [1,2,4,1], sum: 8)

// 如果有百万的数据 input , 这个时候，Set 是符合 Memory 的，但是 input 已超过 Memory ，你需要分布计算。把 Input 拆成好几块丢在不同的计算机里，然后合并结果