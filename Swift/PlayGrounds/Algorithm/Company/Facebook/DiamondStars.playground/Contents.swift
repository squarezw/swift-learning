//: Playground - noun: a place where people can play

import UIKit

/**
 Given an ODD number, print diamond pattern of stars recursively.
 
 n = 5, Diamond shape is as follows:
   *
  ***
 *****
  ***
   *
 
 */
func star_print(n: Int, k:Int = 1, delta:Int = 2) {
    if k < 0 {
        return
    }
    
    var delta = delta
    let t = (n - k) / 2
    let space = [Character](repeating: "-", count: t)
    let stars = [Character](repeating: "*", count: k)
    
    print(String(space) + String(stars) + String(space))
    
    if k == n {
        delta = -delta
    }
    
    star_print(n: n, k: k + delta, delta: delta)
}

star_print(n: 5)
