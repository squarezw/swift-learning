//: Playground - noun: a place where people can play

import UIKit

/**
 给定含有n个元素的整型数组a，其中包括0元素和非0元素，对数组进行排序，要求：
 
 1. 排序后所有0元素在前，所有非零元素在后，且非零元素排序前后相对位置不变
 
 2. 不能使用额外存储空间
 
 例子如下
 
 输入 0, 3, 0, 2, 1, 0, 0
 
 输出 0, 0, 0, 0, 3, 2, 1
 
 
 分析:
 
 此排序非传统意义上的排序，因为它要求排序前后非0元素的相对位置不变，或许叫做整理会更恰当一些。我们可以从后向前遍历整个数组，遇到某个位置i上的元素是非0元素时，如果a[k]为0，则将a[i]赋值给a[k]，a[k]赋值为0。实际上i是非0元素的下标，而k是0元素的下标
 */

// 将零移动到最后
func movesZero(numbers:[Int]) {
    var result = numbers
    var i = 0
    var j = 0
    
    while i < result.count {
        
        if result[i] != 0 {
            if result[j] == 0 {
                result.swapAt(i, j)
            }
            j += 1
        }
        
        i += 1
    }
    
    print(result)
}

var aa = [1,0,3,0,9,2,0,0,3]
movesZero(numbers:aa);

// 将零移动到最前
func arrange(a: [Int]) -> [Int] {
    var a = a
    // zero index
    var k = a.count - 1
    for i in stride(from: k, to: -1, by: -1) {
        print(i)
        if a[i] != 0 {
            if a[k] == 0 {
                a.swapAt(i, k)
            }
            k -= 1
        }
    }
    return a
}

arrange(a: [0,3,1,0,0])
arrange(a: [1,3,0,2,1,0,0,1])
