import UIKit

// Abs combine, -1 + 1 = abs 2
let str = "12\n-33\nwww\n"

let www = str.split(separator: "\n")

let xxx = www.reduce(10) { (result, string) -> Int in
    guard let str = Int(string) else {
        return result
    }
    return result + abs(str)
}
print(xxx)

// either way
var nn = 0
for i in www {
    if let number = Int(i) {
        nn += abs(number)
        print(nn)
    }
}
print(nn)


// Big O is ?
var j = 0
let n = 15
for i in (n/2)..<n {
    while j < i {
        j = j + 1
    }
}

// How to improve?
class View: UIViewController {
    var complet: ((Data?) -> Void)? = nil
    
    deinit {
        print("deinit")
    }
    
    override func viewDidLoad() {
        complet = { [weak self] data in
            self?.process(data: data)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: "http://localhost:3000")
        let dataTask = URLSession.shared.dataTask(with: url!) { [weak self] (data: Data?, _: URLResponse? , _: Error?) in
            self?.complet?(data)
        }
        dataTask.resume()
    }
    
    private func process(data: Data?) {
        print("Process data...")
    }
}

var xx: UIViewController? = View()

xx = nil

enum Taskstate {
    case todo
    case done
}

struct TodoTask {
    let name: String
    var isImp: Bool
    let state: Taskstate
}


var w = TodoTask(name: "ww", isImp: true, state: .done)

w.isImp = false
