//: Playground - noun: a place where people can play

import Cocoa

// Example 1
// 对一个 matrix ，找出所有的 negative (负数)，
// 此 matrix row (行) 与 column (列) 均是有序排列
// 即 -4 -2 -1 -1 -> row
//    -4 -3 -1 -> column
var matrix: [[Int]] = [[-4,-2,-1,1],
                       [-3,-1,1,2],
                       [-1,1,2,3]]

func negative(_ M: [[Int]],_ n: Int,_ m: Int) -> Int {
    var count: Int = 0
    var i = 0
    var j = m - 1
    while j >= 0 && i < n {
        print(".", i, j)
        if M[i][j] < 0 {
            count += j + 1
            i += 1
        } else {
            j -= 1
        }
    }
    return count
}

negative(matrix, 3, 4)

