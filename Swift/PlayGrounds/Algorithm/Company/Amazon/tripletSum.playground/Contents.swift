//: Playground - noun: a place where people can play

import UIKit

/**
 Note: Write a solution with O(n2) time complexity, since this is what you would be asked to do during a real interview.
 
 You have an array a composed of exactly n elements. Given a number x, determine whether or not a contains three elements for which the sum is exactly x.
 
 Example
 
 For x = 15 and a = [14, 1, 2, 3, 8, 15, 3], the output should be
 tripletSum(x, a) = false;
 
 For x = 8 and a = [1, 1, 2, 5, 3], the output should be
 tripletSum(x, a) = true.
 
 The given array contains the elements 1,2, and 5, which add up to 8.
 */

// 先排序，从两头（高位与低位）拿数相加。直到趋于接近
func tripletSum(x: Int, a: [Int]) -> Bool {
    var a = a.sorted().filter { $0 < x } // O(n*logn)
    
    for i in 0..<a.count {  // O(n)
        var low = i + 1
        var high = a.count - 1
        
        while low < high {  // O(n)
            if a[i] + a[low] + a[high] == x {
                return true
            } else if a[i] + a[low] + a[high] < x {
                low += 1
            } else {
                high -= 1
            }
        }
    }
    return false
}

tripletSum(x: 8, a: [1,1,2,5,3])
