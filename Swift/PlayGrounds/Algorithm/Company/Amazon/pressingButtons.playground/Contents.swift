//: Playground - noun: a place where people can play

import UIKit

/**
 Given a string of digits, return all of the possible letter combinations that the number could represent. The mapping of digits to letters is the same as you would find on a telephone's buttons, as in the example below:
 
 The resulting array should be sorted lexicographically.
 
 Example
 
 For buttons = "42", the output should be
 pressingButtons(buttons) = ["ga", "gb", "gc", "ha", "hb", "hc", "ia", "ib", "ic"]
 
 */
func pressingButtons(buttons: String) -> [String] {
    let map: [Character: [String]] =
        ["2":["a","b","c"],
         "3":["d","e","f"],
         "4":["g","h","i"],
         "5":["j","k","l"],
         "6":["m","n","o"],
         "7":["p","q","r","s"],
         "8":["t","u","v"],
         "9":["w","x","y","z"]]
    var array: [[String]] = [[String]]()
    
    for char in buttons {
        if let arr = map[char] {
            array.append(arr)
        }
    }
    
    var result: [String] = []
    
    perm(array: array, i: 0, j: 0, char: "", result: &result)
    
    return result
}


// i: column
// j: row
func perm(array: [[String]], i: Int, j: Int, char: String, result: inout [String]) {
    if j >= array.count {
        result.append(char)
        return
    }
    
    for column in 0..<array[j].count {
        perm(array: array, i: column, j: j+1, char: char + array[j][column], result: &result)
    }
}

pressingButtons(buttons: "435")
