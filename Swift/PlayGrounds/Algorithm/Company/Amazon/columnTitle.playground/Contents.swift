//: Playground - noun: a place where people can play

import UIKit

var letters: [Character] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".map {$0}

func columnTitle(number: Int) -> String {
    let n = letters.count

    let number = number - 1
    if number >= 0 && number < n {
        return String(letters[number])
    } else {
        print(number, number % n)
        return columnTitle(number: number / n) + columnTitle(number: (number % n + 1))
    }
}

columnTitle(number: 27)
columnTitle(number: 470211273)


var w = [Int]()

w.append(1)
w.append(2)
w.append(3)
w.popLast()
