//
//  String.swift
//  Swift
//
//  Created by Jesse on 18/05/2017.
//
//

import Foundation

public extension String {
    
    var length: Int {
        return self.count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(from: Int) -> String {
        return self[min(from, length) ..< length]
    }
    
    func substring(to: Int) -> String {
        return self[0 ..< max(0, to)]
    }
    
    subscript (r: Range<Int>) -> String {
        
        let lowerBound = max(0, min(length, r.lowerBound))
        let upperBound = min(length, max(0, r.upperBound))

//      // Swift 2
//        let lowerBound = max(0, min(length, r.startIndex))
//        let upperBound = min(length, max(0, r.endIndex))
//        let start = startIndex.advancedBy(lowerBound)
//        let end = start.advancedBy(upperBound - lowerBound)
        
        let start = index(startIndex, offsetBy: lowerBound)
        let end = index(start, offsetBy: upperBound - lowerBound)
        return self[start ..< end]
    }
    
}
