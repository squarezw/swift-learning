//: KMP
//: Knuth-Morris-Pratt
//: 
//: ---
//: kmp算法完成的任务是：给定两个字符串O和f，长度分别为n和m，判断f是否在O中出现，如果出现则返回出现的位置。常规方法是遍历a的每一个位置，然后从该位置开始和b进行匹配，但是这种方法的复杂度是O(nm)。kmp算法通过一个O(m)的预处理，使匹配的复杂度降为O(n+m)。
//:
//:
//: http://www.ruanyifeng.com/blog/2013/05/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm.html
//:
//: ---
//:
//: 1. 算说要匹配的值的部分匹配表
//: 2. 移动位数 = 已匹配的字符数 - 对应的部分匹配值
//:
//: 《部分匹配表》（Partial Match Table）
//: 首先，要了解两个概念："前缀"和"后缀"。 "前缀"指除了最后一个字符以外，一个字符串的全部头部组合；"后缀"指除了第一个字符以外，一个字符串的全部尾部组合。
//:
//:
//: "部分匹配值"就是"前缀"和"后缀"的最长的共有元素的长度。以"ABCDABD"为例，
//:　　－　"A"的前缀和后缀都为空集，共有元素的长度为0；
//:　　－　"AB"的前缀为[A]，后缀为[B]，共有元素的长度为0；
//:　　－　"ABC"的前缀为[A, AB]，后缀为[BC, C]，共有元素的长度0；
//:　　－　"ABCD"的前缀为[A, AB, ABC]，后缀为[BCD, CD, D]，共有元素的长度为0；
//:　　－　"ABCDA"的前缀为[A, AB, ABC, ABCD]，后缀为[BCDA, CDA, DA, A]，共有元素为"A"，长度为1；
//:　　－　"ABCDAB"的前缀为[A, AB, ABC, ABCD, ABCDA]，后缀为[BCDAB, CDAB, DAB, AB, B]，共有元素为"AB"，长度为2；
//:　　－　"ABCDABD"的前缀为[A, AB, ABC, ABCD, ABCDA, ABCDAB]，后缀为[BCDABD, CDABD, DABD, ABD, BD, D]，共有元素的长度为0。

import UIKit

// 例:
// 找出 nano 在 banananobano 出现的位置

/** 
 
 Naive string matching
 
 for (i=0; T[i] != '\0'; i++)
 {
 for (j=0; T[i+j] != '\0' && P[j] != '\0' && T[i+j]==P[j]; j++) ;
 if (P[j] == '\0') found a match
 }

 
 There are two nested loops; the inner one takes O(m) iterations and the outer one takes O(n) iterations so the total time is the product, O(mn). This is slow; we'd like to speed it up.
 In practice this works pretty well -- not usually as bad as this O(mn) worst case analysis. This is because the inner loop usually finds a mismatch quickly and move on to the next position without going through all m steps. But this method still can take O(mn) for some inputs. In one bad example, all characters in T[] are "a"s, and P[] is all "a"'s except for one "b" at the end. Then it takes m comparisons each time to discover that you don't have a match, so mn overall.
 
 Here's a more typical example. Each row represents an iteration of the outer loop, with each character in the row representing the result of a comparison (X if the comparison was unequal). Suppose we're looking for pattern "nano" in text "banananobano".

 0  1  2  3  4  5  6  7  8  9 10 11
 T: b  a  n  a  n  a  n  o  b  a  n  o
 
 i=0: X
 i=1:    X
 i=2:       n  a  n  X
 i=3:          X
 i=4:             n  a  n  o
 i=5:                X
 i=6:                   n  X
 i=7:                         X
 i=8:                            X
 i=9:                               n  X
 i=10:                                 X
 */

func navieMatch(string s: String, pattern x: String) -> Int {
    let arrayS = Array(s.characters)
    let arrayX = Array(x.characters)

    for i in 0..<arrayS.count {
        var j = 0
        while i + j < arrayS.count && j < arrayX.count && arrayS[i+j] == arrayX[j] {
            j += 1
        }

        if j >= arrayX.count {
            return i
        }
    }
    return -1
}

let string = "banananobano"
let pattern = "nano"

navieMatch(string: string, pattern: pattern)

// 系统的 range 方法
if let range = string.range(of: pattern) {
    let index = string.distance(from: string.startIndex, to: range.lowerBound)
}


// KMP

// 字符的部分匹配映射表

func overlap(_ string: String) -> [Int] {
    let count = string.characters.count
    var list = [Int](repeating: 0, count: count+1)
    list[0] = -1
    
    for i in 0..<count {
        list[i+1] = list[i] + 1
        
        while list[i + 1] > 0 && string[i] != string[list[i+1] - 1] {
            list[i+1] = list[list[i+1]-1] + 1
        }
        
    }
    return list
}

// 另种解法求 部分匹配表, 
// -- 不稳定, 验证字符串还有些问题!
func getNext(_ patternStr: String) -> [Int] {
    let count = patternStr.characters.count
    var k = -1
    var next = [Int](repeating: -1, count: count)
    
    for case var j in 0..<count - 1 {
        while (k != -1 && patternStr[j] != patternStr[k]) {
            k = next[k]
        }
        k += 1
        j += 1
        if patternStr[j] != patternStr[k] {
            next[j] = k
        } else {
            next[j] = next[k]
        }
    }
    return next
}

//
/**
 Skipping outer iterations
 
 The Knuth-Morris-Pratt idea is, in this sort of situation, after you've invested a lot of work making comparisons in the inner loop of the code, you know a lot about what's in the text. Specifically, if you've found a partial match of j characters starting at position i, you know what's in positions T[i]...T[i+j-1].
 You can use this knowledge to save work in two ways. First, you can skip some iterations for which no match is possible. Try overlapping the partial match you've found with the new match you want to find:
 
 i=2: n  a  n
 i=3:    n  a  n  o
 Here the two placements of the pattern conflict with each other -- we know from the i=2 iteration that T[3] and T[4] are "a" and "n", so they can't be the "n" and "a" that the i=3 iteration is looking for. We can keep skipping positions until we find one that doesn't conflict:
 i=2: n  a  n
 i=4:       n  a  n  o
 Here the two "n"'s coincide. Define the overlap of two strings x and y to be the longest word that's a suffix of x and a prefix of y. Here the overlap of "nan" and "nano" is just "n". (We don't allow the overlap to be all of x or y, so it's not "nan"). In general the value of i we want to skip to is the one corresponding to the largest overlap with the current partial match:
 String matching with skipped iterations:
 
 i=0;
 while (i<n)
 {
 for (j=0; T[i+j] != '\0' && P[j] != '\0' && T[i+j]==P[j]; j++) ;
 if (P[j] == '\0') found a match;
 i = i + max(1, j-overlap(P[0..j-1],P[0..m]));
 }
 */

func skippingMatch(_ s: String, x: String) -> Int {
    let n = s.characters.count
    let m = x.characters.count
    
    var i = 0
    var o = 0
    
    var next = overlap(x) //overlap(x)
//    next.remove(at: 0)
    
    while i < n {
        var j = o
        while i + j < n && j < m && s[i+j] == x[j] {
            j += 1
        }
        
        if j >= m {
            return i
        }
        o = next[j+1]

        i = i + max(1, j-o)
    }
    return -1
}

//skippingMatch(string, x: pattern)
//skippingMatch("a", x: "a")
//skippingMatch("sst", x: "st")
//skippingMatch("a", x: "A")
skippingMatch("CodefightsIsAwesome", x: "IA")
skippingMatch("CodefightsIsAweIsAsome", x: "IsAs")
//skippingMatch("ABCDABD", x: "ABD")


overlap("ABCDABD")
getNext("ABCDABD")


// KMP 判断是否是自身重复
func repeatedSubstringPattern1(_ str: String) -> Bool {
    
    let length = str.characters.count
    //i表示的是下一需要比较的字符串的下标
    var i = 1,j = 0
    var next = [Int](repeating:0, count:length+1)
    while i < length {
        if str[i] == str[j] {
            //若位置i的字符与j匹配的上则对下一个位置的字符进行比较
            //同时，对next数组（即最大公共长度数组进行更新），其下标中的数值即是当前的最大公共长度，即j+=1后的值
            i+=1
            j+=1
            next[i] = j
        } else if j == 0 {
            //若公共长度为一，这种情况可能出现在一开始，也就是字符一开始没有匹配上的时候，亦可能出现在匹配过程中因为匹配不上而不断切割公共长度，直到公共长度为0时。
            //此时，将需要比对的字符向后移动一位进行匹配，也就是i+=1
            i+=1
        } else {
            //当字符不匹配而公共长度又不为0时则不断切割最大公共长度，即前移j在字符串中的位置，也就是将之前保存的next容器中的最大公共长度进行切割，获取j的历史值
            j = next[j]
        }
        
        print(next)
    }
    return  next[length] > 0 && next[length] % (length - next[length]) == 0
}

repeatedSubstringPattern1("ababab")
