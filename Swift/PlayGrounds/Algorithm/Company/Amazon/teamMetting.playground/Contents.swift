//: Playground - noun: a place where people can play

import UIKit

// https://hireon.amazon.com/live/b3ac36ee-ad3e-4ff3-a4c2-9e61bfaa6e1a
//
// Question: given a set of people, schedule a meeting among them
// (0, 1000) => start of the range, and end of the range to schedule the meeting
// A: (2, 10), (15, 30)
// B: (20, 35), (40, 50)

// 公司组织所有人开会，不过每个人的 schedule 上都有更自相应的工作，找出所有人都有空的时间段

func getMeetingTime(people:[[(Int,Int)]], maxHours: Int) {
    var arr = Array(repeating: 0, count: maxHours)
    var start = 0
    
    for person in people {
        for j in person {
            for k in j.0..<j.1 {
                arr[k] = 1
            }
        }
    }
    
    for j in 0..<arr.count {
        if arr[j] == 0 {
            if start == 0 {
                start = j + 1
            } else if j == arr.count - 1 {
                print((start,j))
            }
        } else {
            if start != 0 {
                print((start, j))
                start = 0
            }
        }
    }
    
    print(arr)
}

let person1 = [(2, 10), (15, 30)]
let person2 = [(20, 35), (40, 50)]

let people = [person1, person2]

getMeetingTime(people: people, maxHours: 80)

// Helper 方法，判断两个区间是否相交

public func isIntersection(a:(Int, Int), b:(Int, Int)) -> Bool {
    if max(a.0, min(b.0, a.1)) == b.0 || max(b.0, min(a.0, b.1)) == a.0  {
        return true
    }
    return false
}