//: Playground - noun: a place where people can play

import UIKit

/**
 You are given an n x n 2D matrix that represents an image. Rotate the image by 90 degrees (clockwise).
 
 Try to solve this in-place - in a real interview, you will only be allowed to use O(1) additional memory.
 
 Example
 
 For
 
 a = [[1, 2, 3],
 [4, 5, 6],
 [7, 8, 9]]
 the output should be
 
 rotateImage(a) =
 [[7, 4, 1],
 [8, 5, 2],
 [9, 6, 3]]
 
 */
func rotateImage(a: [[Int]]) -> [[Int]] {
    var result = [[Int]](repeating: [Int](repeating: 0, count: a.first!.count), count: a.count)
    
    for i in 0..<a.count {
        for j in 0..<a.count {
            result[i][j] = a[a.count-1-j][i]
        }
    }
    
    return result
}

let a = [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]]

rotateImage(a: a)
