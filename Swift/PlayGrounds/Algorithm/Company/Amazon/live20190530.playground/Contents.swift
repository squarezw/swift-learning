import UIKit


// it was doing on May.31 for interview.

func min(_ numOfParts: Int, _ parts: [Int]) -> Int {
    let parts = parts.sorted()
    var left = numOfParts

    var result = parts[0]
    var results: [Int] = []

    for i in 1..<parts.count {
        result += parts[i]
        if parts.count - i < left {
            results.append(result)
            print(results)
            left -= 1
        }
    }
    return results.reduce(0, +)
}

min(2, [20,4,8,2])
min(6, [1,2,5,10,35,89])

let w = [20,4,8,2]
let v = [1,2,5,10]

func KS(n: Int, c: Int) -> Int {
    if n < 0 || c == 0 {
        return 0
    } else if (w[n] > c) {
        return KS(n: n - 1, c: c)
    } else {
        let tmp1 = KS(n: n - 1, c: c)
        let tmp2 = v[n] + KS(n: n - 1, c: c - w[n])
        return max(tmp1, tmp2)
    }
}

KS(n: w.count - 1, c: 8)

func opt(_ max: Int, _ forws: [[Int]], _ ret: [[Int]]) -> [[Int]] {
    
    let forws = forws.sorted(by: {$0[1] > $1[1]})
    
    var results: [[Int]] = []
    
    var ret = ret.sorted(by: {$0[1] > $1[1]})
    
    print(forws)
    print(ret)
    
    for complement in forws {
        for j in 0..<ret.count {
            if complement[1] + ret[j][1] <= max {
                print(complement[1], ret[j][1])
                results.append([complement[0], ret[j][0]])
                break
            }
        }
    }
    return results
}


opt(10000, [[1,3000],[2,5000], [3,7000], [4,10000]], [[1,2000],[2,3000],[3,4000],[4,5000]])
