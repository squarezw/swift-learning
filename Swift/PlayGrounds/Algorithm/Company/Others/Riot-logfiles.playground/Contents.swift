//: Playground - noun: a place where people can play

/**
 PROBLEM CONTEXT
 
 Let’s assume you’re a Site Reliability Engineer for a MOBA game’s microservice backend.
 In the game, players can purchase items, such as skins or power-ups. This functionality is provided by a PlayerItems microservice, which​ has a CRUD API for players’ purchased items. This is the READ API:
 - GET /api/playeritems?<...> ​returns a JSON list of the items a player has purchased The PlayerItems microservice logs each API request to a text log file, stored at:
 
 /var/log/httpd/\*.log
 
 The log files are rotated every day at midnight, so there are multiple days of logs in that directory. Example:
 /var/log/httpd/2018-13-10.log ​← Contains logs for Oct 13, 2018 /var/log/httpd/2018-12-10.log
 /var/log/httpd/2018-11-10.log
 ...
 Each log file contains multiple log lines, and the format for each log line is:
 IP_ADDRESS [timestamp] "HTTP_VERB URI" HTTP_ERROR_CODE RESPONSE_TIME_IN_MILLISECONDS
 Example:
 10.2.3.4 [2018/13/10:14:02:39] "GET /api/playeritems?playerId=3" 200 1230 10.3.4.5 [2018/13/10:14:02:41] "GET /api/playeritems?playerId=2" 200 4630
 You'd like to understand whether the READ API’s response time has a wide variance. This may indicate a problem. For example:
 ​​READ API:
 90% of requests return a response in 1300 ms 95% of requests return a response in 1400 ms
 99% of requests return a response in 10000 ms ← ​Potential issue? Why are 5% of requests taking ~10x as long?
 
 PROBLEM STATEMENT
 
 (1) Please write code that calculates the 90%, 95% and 99% percentile response time for READ API requests, based on ALL log files stored in /var/log/httpd/\*.log.
 (2) Please also indicate your code’s time-complexity and space-complexity, in terms of “Big O” notation (ie. O(n)).
 Use whatever language or framework you'd like. The code should print output in this form (replace X, Y, Z with calculated #s):
 90% of requests return a response in X ms 95% of requests return a response in Y ms 99% of requests return a response in Z ms
 
 */


/**
 
 Let's assume each file stored a bunch of log, which means it is hardly sort merged and unsorted log files.
 
 Solution:
 
 Therefore I'm gonna sort logs for each file first, and then merge sorted result to one place. after that figure out 90%, 95%, 99% percentile response time.
 
 Also assume that I've already extracted response time into a list from each log file.

    10.2.3.4 [2018/13/10:14:02:39] "GET /api/playeritems?playerId=3" 200 1230 10.3.4.5 [2018/13/10:14:02:41] "GET /api/playeritems?playerId=2" 200 4630
    10.2.3.4 [2018/13/10:14:02:39] "GET /api/playeritems?playerId=3" 200 1230 10.3.4.5 [2018/13/10:14:02:41] "GET /api/playeritems?playerId=2" 200 5630
    ...
 
 */

var mockFile1 = [1330, 3630, 3600, 4100, 4200, 4200, 10630]
var mockFile2 = [2600, 5600, 3330, 3500, 3700, 4200, 8130]
var mockFile3 = [3630, 5300, 6230, 6230, 4200, 8000, 8220]

/// Quick Sort
///
/// - Parameter array: comparable array
///   - array: comparable array
///   - left: left index of array
///   - right: right index of array
///
/// - Complexity: O(nlog(n))
/// - Space complexity: O(log(n))
func sort(_ array: inout [Int], left: Int, right: Int) {
    if left >= right {
        return
    }
    let pivot = array[(left + right) / 2]
    let index = partition(array: &array, left, right, pivot)
    
    sort(&array, left: left, right: index - 1)
    sort(&array, left: index, right: right)
}

func partition(array: inout [Int], _ left: Int, _ right: Int, _ pivot: Int) -> Int {
    var left = left
    var right = right
    
    while left <= right {
        while array[left] < pivot {
            left += 1
        }
        
        while array[right] > pivot {
            right -= 1
        }
        
        if left < right {
            array.swapAt(left, right)
        }
        
        left += 1
        right -= 1
    }
    
    return left
}

/// Merge sorted list
///
/// - Parameters:
///   - a: given a comparable array
///   - b: another comparable array
/// - Returns: comparable array
///
/// Discussion:
///     We could choose another soultion for it which is create an array of size n+m and one by one copy all arrays to it. and than choose any O(n log n) sorting algorithm such as QuickSort. however for our case, each log response time is in the almost same period, and size of array is like static length, so probably that it is not going to be wrost case, therefore i took this sort algorithm.
///
/// - Complexity: O(n+m).
///     In this func n is size a array, m is size b array.
///     The wrost case is n+m, which means if a size is equal b and a is exactly that one of a is less than one of b, and next one is greater than one of b. like this [1,3,5], [2,4,6]. which will be n + m

/// - Space complexity: O(n+m)
///     Create an array "c" to store compared result everytime which is Extra Space as O(n+m).
func mergeSortedList<T: Comparable>(a: [T], b: [T]) -> [T] {
    var i = 0
    var j = 0
    var c: [T] = []
    
    while i < a.count && j < b.count {
        if a[i] < b[j] {
            c.append(a[i])
            i += 1
        } else if a[i] == b[j] {
            c.append(a[i])
            c.append(b[j])
            i += 1
            j += 1
        } else {
            c.append(b[j])
            j += 1
        }
    }
    if i < a.count {
        c.append(contentsOf: a[i..<a.endIndex])
    }
    
    if j < b.count {
        c.append(contentsOf: b[j..<b.endIndex])
    }
    
    return c
}

/// Merge K sorted list
///
/// - Complexity: O(n).
///     based on above merge solution, k is how many array we are going to merge, n is each array of size, n1*n2...nk
/// - Space complexity: O(n)
///     a extra space to palce result
func mergeKLists<T: Comparable>(_ arr: [[T]]) -> [T] {
    var result: [T] = []
    
    for i in 0..<arr.count {
        result = mergeSortedList(a: result, b: arr[i])
    }
    
    return result
}

/// Step 1: sorted each log file
sort(&mockFile1, left: 0, right: mockFile1.count - 1)
sort(&mockFile2, left: 0, right: mockFile2.count - 1)
sort(&mockFile3, left: 0, right: mockFile3.count - 1)

/// Step 2: merge sorted log files
let sortedlist = mergeKLists([mockFile1, mockFile2, mockFile3])

/// Report response time
///
/// - Parameter per: percentile, ex: 0.99, 0.95, 0.90
func report(_ per: Float) {
    guard per < 1 else {
        print("percentile is invalid!")
        return
    }
    let count = sortedlist.count
    let index = min(Int(Float(count) * per), count - 1)
    let result = sortedlist[index]
    print("\(per * 100)% of requests return a response in \(result)")
}

// Step 3: report

[0.99, 0.95, 0.90].map {report($0)}


///////////////////////////////////////////////////////

/// common letters
func intersection(a: String, b: String) {
    var inter: Set<Character> = []

    // Step 1: get intersection character
    if a.count > b.count {
        b.forEach {
            if a.contains($0) {
                inter.insert($0)
            }
        }
    } else {
        a.forEach {
            if b.contains($0) {
                inter.insert($0)
            }
        }
    }

    // Step 2: count common character for each
    var result = [Character:Int]()

    (a+b).forEach {
        if inter.contains($0) {
            if let value = result[$0] {
                result[$0]! += 1
            } else {
                result[$0] = 1
            }
        }
    }

    print(result)
}
intersection(a: "apple", b: "pear")
intersection(a: "aaaa", b: "bbb")
intersection(a: "aaaa", b: "bbaaww")
