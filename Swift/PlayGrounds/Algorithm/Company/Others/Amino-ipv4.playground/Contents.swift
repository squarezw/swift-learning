//: Playground - noun: a place where people can play

import UIKit

// ================

struct ZError: Error {
    let message: String
}

func matches(for regex: String, in text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex)
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

func pad(string : String, toSize: Int) -> String {
    var padded = string
    for _ in 0..<(toSize - string.characters.count) {
        padded = "0" + padded
    }
    return padded
}

func solution(s: String) throws -> UInt32? {
    // old: (\\d{1,3})
    let ip: [String] = matches(for: "\\b([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\b", in: s)
    
    guard ip.count == 4 else {
        throw ZError(message: "Ip address invalid \(s)")
    }
    
    var result = ""
    
    for c in ip {
        result += pad(string: String(UInt8(c)!, radix: 2), toSize: 8)
    }
    
    return UInt32(result, radix: 2)
}

// Test Case

func testCase(_ str: String, equal: UInt32) -> Bool {
    do {
        let data: UInt32? = try solution(s: str)
        return data == equal
    } catch let error as ZError {
        print(error.message)
        return false
    } catch {
        return false
    }
}

assert(testCase("172.168.5.1", equal: 2896692481) == true)
assert(testCase(" 172 .168.5.1", equal: 2896692481) == true)
assert(testCase("  172  . 168 .  5 . 1 ", equal: 2896692481) == true)
assert(testCase("1c2.168.5.1", equal: 2896692481) == false)
assert(testCase("17 2.168.5.1", equal: 2896692481) == false)
