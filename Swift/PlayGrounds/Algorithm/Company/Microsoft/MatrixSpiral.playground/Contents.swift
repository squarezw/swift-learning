//: Playground - noun: a place where people can play

import UIKit

/**
 Given a rectangular matrix, return all of the elements of the matrix in spiral order.
 
 Example
 
 For
 
 matrix =
 [
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
 ]
 
 the output should be
 matrixElementsInSpiralOrder(matrix) = [1, 2, 3, 4, 5, 6, 7, 8, 9].
 */

enum Direction: Int {
    case right = 0
    case bottom = 1
    case left = 2
    case top = 3
}

func matrixElementsInSpiralOrder(matrix: [[Int]]) -> [Int] {
    var matrix = matrix
    var row = 0
    var column = 0
    let row_count = matrix.count
    let column_count = matrix.first?.count ?? 0
    if row_count == 0 && column_count == 0 {
        return []
    }
    
    var direction: Direction = .right
    
    var isFinish = false
    var result: [Int] = []
    while !isFinish {
        //        print(row, column)
        if matrix[row][column] != -99999  {
            result.append(matrix[row][column])
            matrix[row][column] = -99999
        }
        
        let right = column + 1
        let bottom = row + 1
        let left = column - 1
        let top = row - 1
        
        if direction == .right {
            if right == column_count || matrix[row][right] == -99999 {
                direction = .bottom
            } else {
                column += 1
            }
        }
        
        if direction == .bottom {
            if bottom == row_count || matrix[bottom][column] == -99999 {
                direction = .left
            } else {
                row += 1
            }
        }
        
        if direction == .left {
            if left < 0 || matrix[row][left] == -99999 {
                direction = .top
            } else {
                column -= 1
            }
        }
        
        if direction == .top {
            if top < 0 || matrix[top][column] == -99999 {
                direction = .right
            } else {
                row -= 1
            }
        }
        
        if direction == .right {
            if right == column_count || matrix[row][right] == -99999 {
                isFinish = true
            }
        }
    }
    
    return result
}

//let matrix: [[Int]] = [[0,3],[5,5]]
let matrix: [[Int]] = [[ 1, 2, 3 ],
                       [ 8, 9, 4 ],
                       [ 7, 6, 5 ]]

matrixElementsInSpiralOrder(matrix: matrix)