//: Playground - noun: a place where people can play

import UIKit

/**
 Lowest Common Ancestor in a Binary Tree | Set 2 (Using Parent Pointer)
 
 Given values of two nodes in a Binary Tree, find the Lowest Common Ancestor (LCA). It may be assumed that both nodes exist in the tree.
 
            20
          /    \
        8       22
      /   \
    4       12
          /    \
        10      14
 
 For example, consider the Binary Tree in diagram, LCA of 10 and 14 is 12 and LCA of 8 and 14 is 8.
 
 Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is defined as the lowest node in T that has both n1 and n2 as descendants (where we allow a node to be a descendant of itself).
 http://en.wikipedia.org/wiki/Lowest_common_ancestor
 
 ------
 
 We strongly recommend you to minimize your browser and try this yourself first.
 
 We have discussed different approaches to find LCA in set 1. Finding LCA becomes easy when parent pointer is given as we can easily find all ancestors of a node using parent pointer.
 
 Below are steps to find LCA.
 
 Create an empty hash table.
 Insert n1 and all of its ancestors in hash table.
 Check if n2 or any of its ancestors exist in hash table, if yes return the first existing ancestor.
 Below is C++ implementation of above steps.
 */

class Node : Hashable, CustomStringConvertible {
    var left: Node?
    var right: Node?
    weak var parent: Node?
    var level: Int = 1
    
    var data: Int
    
    init(data: Int) {
        self.data = data
    }
    
    var hashValue: Int {
        return self.data.hashValue
    }
    
    static func ==(lhs: Node, rhs: Node) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    var description: String {
        return String(data)
    }
    
    
    // insert a new node
    func insert(value: Int) {
        if value < data {
            if left == nil {
                left = Node(data: value)
                left?.parent = self
                left?.level = level + 1
            } else {
                left?.insert(value: value)
            }
        } else {
            if right == nil {
                right = Node(data: value)
                right?.parent = self
                right?.level = level + 1
            } else {
                right?.insert(value: value)
            }
        }
    }
    
    func find(value: Int) -> Node? {
        if value == data {
            return self
        }
        if value < data {
            if left != nil {
                return left!.find(value: value)
            } else {
                return nil
            }
        } else {
            if right != nil {
                return right!.find(value: value)
            } else {
                return nil
            }
        }
    }
    
    func desc() {
        if left != nil {
            left!.desc()
        }
        for _ in (0..<level) {
            print("", terminator: "\t")
        }
        print(data)
        if right != nil {
            right!.desc()
        }
    }
    
    class func desc(prefix: String, _ node: Node?, _ isLeft: Bool) {
        if node == nil {
            return
        }
        
        print(prefix + (isLeft == true ? "|-- " : "\\-- " ) + String(node!.data))
        desc(prefix: prefix + (isLeft == true ? "|   " : "    "), node?.left, true);
        desc(prefix: prefix + (isLeft == true ? "|   " : "    "), node?.right, false);
    }
    
    // LCA with value , what if you use node object , you need to change approach
    func lca(value1: Int, value2: Int) -> Node? {
        // hash map
        var ancestors: [Node: Bool] = [:]
        var n1: Node? = find(value: value1)
        var n2: Node? = find(value: value2)
        if n1 == root || n2 == root {
            return root
        }
        
        // Insert n1 and all its ancestors in map
        while n1 != nil {
            ancestors[n1!] = true
            n1 = n1!.parent
        }
        
        while n2 != nil  {
            if ancestors[n2!] != nil {
                return n2
            }
            n2 = n2!.parent
        }
        
        return nil
    }
}

let root = Node(data: 20)

root.insert(value: 8)
root.insert(value: 22)
root.insert(value: 4)
root.insert(value: 12)
root.insert(value: 10)
root.insert(value: 14)

Node.desc(prefix: "", root, false)

print(root.lca(value1: 8, value2: 14))

/**
 Note : The above implementation uses insert of Binary Search Tree to create a Binary Tree, but the function LCA is for any Binary Tree (not necessarily a Binary Search Tree).
 
 Time Complexity : O(h) where h is height of Binary Tree if we use hash table to implement the solution (Note that the above solution uses map which takes O(Log h) time to insert and find). So the time complexity of above implementation is O(h Log h).
 
 Auxiliary Space : O(h)
 */




/**
 A O(h) time and O(1) Extra Space Solution
 :
 The above solution requires extra space because we need to use a hash table to store visited ancestors. We can solve the problem in O(1) extra space using following fact : If both nodes are at same level and if we traverse up using parent pointers of both nodes, the first common node in the path to root is lca.
 The idea is to find depths of given nodes and move up the deeper node pointer by the difference between depths. Once both nodes reach same level, traverse them up and return the first common node.
 Thanks to Mysterious Mind for suggesting this approach.
 */







