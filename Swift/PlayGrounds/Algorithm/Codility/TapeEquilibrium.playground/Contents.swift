//: Playground - noun: a place where people can play

import Cocoa

/*
 Minimize the value |(A[0] + ... + A[P-1]) - (A[P] + ... + A[N-1])|
 
 在数组中找出平衡值
 
 如 [5,1,3,4]
 
 5 - (1+3+4)   = 3
 (5+1) - (3+4) = 1
 (5+1+3) - 4   = 5
 
 最小值是 1, 平衡值是1
 */

func solution(_ A : inout [Int]) -> Int {
    var left = A[0]
    var right = A[1..<A.count].reduce(0, +)
    
    var min: Int = abs(left - right)
    
    for i in 1..<(A.count-1) {
        left += A[i]
        right -= A[i]
        if abs(left - right) < min {
            min = abs(left - right)
        }
    }
    
    return min
}

var a = [5,1,3,4]

solution(&a)
