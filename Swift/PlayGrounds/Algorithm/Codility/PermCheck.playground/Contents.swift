//: Playground - noun: a place where people can play

import Cocoa

/*
 找出非线性序列的数组
 
 i.e: [4,1,3,2] is ture,  [4,3,2] or [4,1,2] is false
 
 A non-empty zero-indexed array A consisting of N integers is given.
 
 A permutation is a sequence containing each element from 1 to N once, and only once.
 
 For example, array A such that:
 
 A[0] = 4
 A[1] = 1
 A[2] = 3
 A[3] = 2
 is a permutation, but array A such that:
 
 A[0] = 4
 A[1] = 1
 A[2] = 3
 is not a permutation, because value 2 is missing.
 
 The goal is to check whether array A is a permutation.
 
 Write a function:
 
 public func solution(_ A : inout [Int]) -> Int
 that, given a zero-indexed array A, returns 1 if array A is a permutation and 0 if it is not.
 
 For example, given array A such that:
 
 A[0] = 4
 A[1] = 1
 A[2] = 3
 A[3] = 2
 the function should return 1.
 
 Given array A such that:
 
 A[0] = 4
 A[1] = 1
 A[2] = 3
 the function should return 0.
 
 Assume that:
 
 N is an integer within the range [1..100,000];
 each element of array A is an integer within the range [1..1,000,000,000].
 Complexity:
 
 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 */

func solution(_ A : inout [Int]) -> Int {
    var set: Set<Int> = Set()
    
    for i in 0..<A.count {
        let num = A[i]
        print("++", num, set)
        if !set.contains(where: {$0 == num}) && num <= A.count {
            set.insert(num)
        } else {
            return 0
        }
    }
    
    return 1
}

/*性能更好点*/
func solution2(_ A : inout [Int]) -> Int {
    let tmpA = A.sorted()
    var x: Int = 1
    
    if let a = tmpA.first, a != x {
        return 0
    }
    
    for i in 1..<tmpA.count {
        x += 1
        if x != tmpA[i] {
            return 0
        }
    }
    
    return 1
}

var a = [1,3,4,2]
var b = [1,2,4,5]

solution(&a)

solution2(&b)
