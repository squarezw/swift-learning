//: Playground - noun: a place where people can play

import Cocoa

/*
 Find the earliest time when a frog can jump to the other side of a river.
 
 A small frog wants to get to the other side of a river. The frog is initially located on one bank of the river (position 0) and wants to get to the opposite bank (position X+1). Leaves fall from a tree onto the surface of the river.
 
 You are given a zero-indexed array A consisting of N integers representing the falling leaves. A[K] represents the position where one leaf falls at time K, measured in seconds.
 
 The goal is to find the earliest time when the frog can jump to the other side of the river. The frog can cross only when leaves appear at every position across the river from 1 to X (that is, we want to find the earliest moment when all the positions from 1 to X are covered by leaves). You may assume that the speed of the current in the river is negligibly small, i.e. the leaves do not change their positions once they fall in the river.
 
 For example, you are given integer X = 5 and array A such that:
 
 A[0] = 1
 A[1] = 3
 A[2] = 1
 A[3] = 4
 A[4] = 2
 A[5] = 3
 A[6] = 5
 A[7] = 4
 
 In second 6, a leaf falls into position 5. This is the earliest time when leaves appear in every position across the river.
 
 有一支青蛙要过河，河边有一颗树，树会不间断的飘下叶子，叶子每次飘的位置不一，
 当叶子铺成一条路时，青蛙就可以过河了。假设河中间的距离为 X ，每次飘落的位置
 在数组 A 中，A[0] = 1 表示第一次落在位置 1，当所有的位置都覆盖到时，即从
 1..X 都有叶子时，它就可以过去了，返回飘落的最早会是在哪儿, 如果没有则返回 -1
 */

func solution(_ X: Int, _ A: inout [Int]) -> Int {
    var steps = X;
    
    var elements: Set<Int> = []
    
    for i in 0..<A.count {
        if !elements.contains(A[i]) {
            elements.insert(A[i])
            steps -= 1
        }
        if steps == 0 {
            return i
        }
    }
    return -1
}

var a = [1,2,4,3,5,6,3]

solution(6, &a)