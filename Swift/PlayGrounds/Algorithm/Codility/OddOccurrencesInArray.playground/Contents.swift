//: Playground - noun: a place where people can play

import Cocoa

/*
 Using Bitwise operation -- XOR
 相当于包含关系
 
 比如 9 ^= 8 ，位运算后就是这两个数的合集，结果是 1。
 如果再进行  1 ^= 8 结果就会是 9。这就是进阶的排除法。
 如果是 9 ^= 8 ^= 3 结果是 2 ，如果 2 ^= 3 结果会是几呢？
 */
func solution(_ A: inout[Int]) -> Int {
    var unpaired = 0
    for number in A {
        unpaired ^= number
        print("\(number)--\(unpaired)")
    }
    return unpaired
}

var array = [9,8,3,3,9,7,8]

solution(&array) // result = 7