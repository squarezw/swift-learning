//: Playground - noun: a place where people can play

import Cocoa

/*
 MissingInteger
 
 Find the minimal positive integer not occurring in a given sequence.
 
 Write a function:
 
 int solution(int A[], int N);
 that, given a non-empty zero-indexed array A of N integers, returns the minimal positive integer (greater than 0) that does not occur in A.
 
 For example, given:
 
 A[0] = 1
 A[1] = 3
 A[2] = 6
 A[3] = 4
 A[4] = 1
 A[5] = 2
 the function should return 5.
 
 Assume that:
 
 N is an integer within the range [1..100,000];
 each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
 Complexity:
 
 expected worst-case time complexity is O(N);
 expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 Elements of input arrays can be modified.
 
 */

func solution(_ A : inout [Int]) -> Int {
    let elements: Set<Int> = Set(A)
    var lookFor = 1
    
    while elements.contains(lookFor) {
        lookFor += 1
    }
    
    return lookFor
}

var a = [1,3,6,4,1,2]
solution(&a)


// solution on my own
public func solutionB(_ A : inout [Int]) -> Int {
    // 1,1,2,3,4,6
    // 1 -> 2
    // 2 > 1 -> 2
    // 2 = 2 -> 3
    // 3 = 3 -> 4
    // 4 = 4 -> 5
    // 5 < 6 -> 5
    
    let tmpA = A.sorted()
    var smallest: Int = 1
    
    for e in tmpA {
        if smallest < e {
            break
        } else if smallest == e {
            smallest += 1
        }
    }
    
    return smallest
}

solutionB(&a)
