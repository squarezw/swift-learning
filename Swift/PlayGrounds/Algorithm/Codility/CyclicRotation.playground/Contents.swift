//: Playground - noun: a place where people can play

import Cocoa

/*
 CyclicRotation
 循环序列
 
 public func solution(_ A : inout [Int], _ K : Int) -> [Int]
 that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.
 
 For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].
 */

public func solution(_ A : inout [Int], _ K : Int) -> [Int]
{
    let count = A.count
    
    if count == 0  {
        return A
    }
    
    var tmpA: [Int] = []
    
    let N = K % count
    
    // 顺时针循环
    //    tmpA.append(contentsOf: Array(A[N..<count]))
    //    tmpA.append(contentsOf: Array(A[0..<N]))
    
    // 逆时针循环
    tmpA.append(contentsOf: Array(A[(count - N)..<count]))
    tmpA.append(contentsOf: Array(A[0..<(count - N)]))
    
    return tmpA
}

var k1 = [3,8,9,7,6]

solution(&k1, 3)