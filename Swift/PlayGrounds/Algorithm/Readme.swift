/**
 Big O Notation
 
 f(n)       Name
 -----------------------
 1          Constant
 logn       Logarithmic
 n          Linear
 nlognn     Linear
 n2         Quadratic
 n3         Cubic
 2n         Exponential
 
 http://interactivepython.org/runestone/static/pythonds/_images/newplot.png
 
 */

import Foundation
