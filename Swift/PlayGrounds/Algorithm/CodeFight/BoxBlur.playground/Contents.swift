//: Playground - noun: a place where people can play

import UIKit

/**
 Last night you had to study, but decided to party instead. Now there is a black and white photo of you that is about to go viral. You cannot let this ruin your reputation, so you want to apply box blur algorithm to the photo to hide its content.
 
 The algorithm works as follows: each pixel x in the resulting image has a value equal to the average value of the input image pixels' values from the 3 × 3 square with the center at x. All pixels at the edges are cropped.
 
 As pixel's value is an integer, all fractions should be rounded down.
 
 Example
 
 For
 
 image = [[1, 1, 1],
 [1, 7, 1],
 [1, 1, 1]]
 the output should be boxBlur(image) = [[1]].
 
 In the given example all boundary pixels were cropped, and the value of the pixel in the middle was obtained as (1 + 1 + 1 + 1 + 7 + 1 + 1 + 1 + 1) / 9 = 15 / 9 = ~rounded down~ = 1.
 */
func boxBlur(image: [[Int]]) -> [[Int]] {
    var result: [[Int]] = []
    
    var sum = 0
    var column = 0, row = 0
    var counts: [Int] = []
    
    while true {
        if column + 3 > image.first!.count {
            column = 0
            row += 1
            result.append(counts)
            
            if row + 3 > image.count {
                break
            }
            
            counts = []
        }
        
        for column in column..<column+3 {
            for row in row..<row+3 {
                sum += image[row][column]
            }
        }
        
        counts.append(sum/9)
        sum = 0
        column += 1
    }
    
    
    return result
}

var image = [[1, 1, 1],
             [1, 7, 1],
             [1, 1, 1]]

boxBlur(image: image)

image = [[1, 1, 1, 1],
         [1, 7, 1, 1],
         [1, 1, 1, 2],
         [1, 1, 2, 2]]

boxBlur(image: image)

// 其它解法

func boxBlur2(image: [[Int]]) -> [[Int]] {
    var r = [[Int]] (repeating: [Int] (repeating: 0, count: image[0].count - 2), count: image.count-2)
    
    for x in 0 ..< r.count {
        for y in 0 ..< r[0].count {
            var sum = 0
            
            // 通用多排多列相加法则, 针对多维数组使用
            for k in -1 ... 1 {
                for j in -1 ... 1 {
                    sum += image[x+k+1][y+j+1]
                }
            }
            
            r[x][y] = sum / 9
        }
    }
    
    return r
}

boxBlur2(image: image)