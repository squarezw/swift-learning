//: Playground - noun: a place where people can play

import UIKit

/**
 You have a string s that consists of English letters, punctuation marks, whitespace characters, and brackets. It is guaranteed that the parentheses in s form a regular bracket sequence.
 
 Your task is to reverse the strings contained in each pair of matching parentheses, starting from the innermost pair. The results string should not contain any parentheses.
 
 Example
 
 For string s = "a(bc)de", the output should be
 reverseParentheses(s) = "acbde".
 
 */

var str = "ab(cd)efghij" // -> apmnolkjihgfedcbq

func reverseParentheses(s: String) -> String {
    
    var str = s
    
    if let a = str.range(of:"(") {
        str =  str[..<a.lowerBound] + reverseParentheses(s: String(str[a.upperBound...]))
        print("1. ->",str)
    }
    
    if let b = str.range(of:")") {
        str = String(str[..<b.lowerBound].reversed()) + String(str[b.upperBound...])
        print("2. ->", str)
    }
    
    return str
}

reverseParentheses(s: str)


// 正则法

func matches(for regex: String, in text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex)
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

// Swift 3 has some problem to solve
func reverseRegex(s: String) -> String {
    /*
     Swift 2
     
     guard let range = s.rangeOfString("\\([^\\(\\)]*\\)", options: .RegularExpressionSearch) else { return s }
     let open = range.first!
     let close = range.last!
     let prefix = s.substringToIndex(open)
     var middle = String(s[open.advancedBy(1)..<close].characters.reverse())
     let suffix = s.substringFromIndex(close.advancedBy(1))
     return reverseParentheses(prefix + middle + suffix)

     */
    // \\((.*?)\\)
    guard let range = s.range(of: "\\([^\\(\\)]*\\)", options: .regularExpression) else { return s }
    let open = range.lowerBound
    let close = range.upperBound
    let prefix = s.substring(to: open)
    
    var middle = String(s[open..<close].characters.reversed())
    let end = s.substring(from: s.index(close, offsetBy: 0))
    return reverseRegex(s: prefix + middle + end)
}

reverseRegex(s: str)



// Original 原创法

func reverse(s: String, startSymbol: Character, endSymbol: Character) -> String {
    
    if s.range(of: "(") == nil || s.range(of: ")") == nil {
        return s
    }
    
    var startIndex = 0
    var endIndex = 0
    var arr: [String] = []
    var deep = 1
    let chars = Array(s.characters)
    
    for i in 0..<chars.count {
        if chars[i] == startSymbol {
            if deep == 1 {
                startIndex = i
            }
            
            deep += 1
        } else if (chars[i] == endSymbol) {
            deep -= 1
            if deep == 1 {
                endIndex = i
                arr.append(String(chars[startIndex..<endIndex+1]))
            }
        }
    }
    
    var result = s
    
    for text in arr {
        let tt = String(text.characters.reversed()[1..<text.characters.count-1])
        result = result.replacingOccurrences(of: text, with: tt)
    }
    
    return reverse(s: result, startSymbol: endSymbol, endSymbol: startSymbol)
}

reverse(s: str, startSymbol: "(", endSymbol: ")")

