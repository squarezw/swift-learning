//: Playground - noun: a place where people can play

import UIKit

/**
 Find the first repeating element in an array of integers
 
 Input:  arr[] = {10, 5, 3, 4, 3, 5, 6}
 Output: 5 [5 is the first element that repeats]
 
 Input:  arr[] = {6, 10, 5, 4, 9, 120, 4, 6, 10}
 Output: 6 [6 is the first element that repeats]
 */


let arr1 = [10, 5, 3, 4, 3, 5, 6]
let arr2 = [6, 10, 5, 4, 9, 120, 4, 6, 10]

/**
 Incorrect func
 
 this way just using each tmp element compare with original latest element that is inserting to tmp array
 */
func findD(n: [Int]) -> Int {
    var tmpArr:[Int] = [n[0]]
    for i in 1..<n.count-1 {
        if tmpArr.contains(n[i]) {
            return n[i]
        }
        tmpArr.append(i)
    }
    return 0
}

// assert(findD(n: arr1) == 5) // incorrect result


var hash: [Int: Int] = [:]

/**
 Correct func
 */
func findDup(n: [Int]) -> Int {
    for i in 0..<n.count-1 {
        hash[n[i]] = (hash[n[i]] ?? 0) + 1
    }
    //    print(hash)
    
    return n.filter({ (k) -> Bool in
        if let value = hash[k] {
            return value > 1
        }
        return false
    }).first ?? 0
}

assert(findDup(n: arr1) == 5)
assert(findDup(n: arr2) == 6)

// represent all repeating elements
let repItems = hash.compactMap {$1 > 1 ? $0 : nil}
print("repeating items: ",repItems)
