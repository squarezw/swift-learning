//: Playground - noun: a place where people can play

import UIKit


/**
 扫雷游戏
 
 In the popular Minesweeper game you have a board with some mines and those cells that don't contain a mine have a number in it that indicates the total number of mines in the neighboring cells. Starting off with some arrangement of mines we want to create a Minesweeper game setup.
 
 Example
 
 For
 
 matrix = [[true, false, false],
            [false, true, false],
            [false, false, false]]
 the output should be
 
 minesweeper(matrix) = [[1, 2, 1],
                        [2, 1, 1],
                        [1, 1, 1]]
 
 Check out the image below for better understanding:
 
 */
func minesweeper(matrix: [[Bool]]) -> [[Int]] {
    var result = [[Int]] (repeating: [Int] (repeating: 0, count: matrix[0].count), count: matrix.count)
    
    for row in 0..<matrix.count {
        for column in 0..<matrix[0].count {
            var count = 0
            
            for k in -1...1 { // row
                for j in -1...1 { // column
                    if column + j == column && row + k == row {
                        continue
                    }
                    
                    if column + j >= 0 && column + j < matrix[0].count {
                        if row + k >= 0 && row + k < matrix.count {
                            print( row, column, k, j)
                            count += matrix[row+k][column+j] == true ? 1 : 0
                        }
                    }
                }
            }
            
            result[row][column] = count
        }
    }
    
    return result
}


let matrix = [[true, false, false],
              [false, true, false],
              [false, false, false]]

minesweeper(matrix: matrix)