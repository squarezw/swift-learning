//: Playground - noun: a place where people can play

import UIKit

struct Stack<Element>  {
    fileprivate var array: [Element] = []
    
    mutating func push(_ element: Element) {
        array.append(element)
    }
    
    mutating func pop() -> Element? {
        return array.popLast()
    }
    
    func peek() -> Element? {
        return array.last
    }
    
    var isEmpty: Bool {
        return array.isEmpty
    }
    
    var count: Int {
        return array.count
    }
    
    func description() {
        print(array)
    }
}

let map: [[Character]] = [["{", "}"], ["[", "]"], ["(", ")"]]
var stack: Stack<Character> = Stack(array: [])

func isOpenTerm(_ c: Character) -> Bool {
    for array in map {
        if array[0] == c {
            return true
        }
    }
    return false
}

func matches(_ top: Character, _ c: Character) -> Bool {
    for array in map {
        if array[0] == top {
            return array[1] == c
        }
    }
    return false
}

func solution(_ chars: String) -> Bool {
    
    for c in chars {
        if isOpenTerm(c) {
            stack.push(c)
        } else {
            if stack.isEmpty || !matches(stack.pop()!, c) {
                return false
            }
        }
    }
    
    return stack.isEmpty
}

let str = "{{[()()[]]}}"
solution(str)
