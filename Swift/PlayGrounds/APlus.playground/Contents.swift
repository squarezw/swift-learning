import UIKit


// API
protocol LogAPI {
    func printLog(msg: String)
}

protocol NetWorkAPI {
    func request()
}

protocol API: LogAPI, NetWorkAPI {}



// API Provider Manager

class AManager {
    static let shared = AManager()

    var logProvider: LogAPI?
    var networkProvider: NetWorkAPI?

    init() {
        register(provider: LogAdaptor())
        register(provider: NetworkAdaptor())
    }

    func register(provider: LogAPI) {
        logProvider = provider
    }

    func register(provider: NetWorkAPI) {
        networkProvider = provider
    }
}

// Adaptor

class LogAdaptor: LogAPI {
    let log = Log() // TODO: Reflection

    func printLog(msg: String) {
        log.execute(msg: msg)
    }
}

class NetworkAdaptor: NetWorkAPI {
    let network = OKHttp() // TODO: Reflection

    func request() {
        network.perform()
    }
}


// 3rd / 2rd party library

class Log {
    func execute(msg: String) {
        print("[log]: \(msg)")
    }
}

class OKHttp {
    func perform() {
        LoggerWrapper.e(msg: "OKHTTP Perform!")
        print("Processing a real network request!")
    }
}


// Custom Adaptor

class CustomHttp: NetWorkAPI {
    func request() {
        LoggerWrapper.e(msg: "New Network!")
    }
}

class CustomLog: LogAPI {
    func printLog(msg: String) {
        print("[New Log]: " + msg)
    }
}

// Wrapper

class LoggerWrapper {
    static func e(msg: String) {
        AManager.shared.logProvider?.printLog(msg: msg)
    }
}

class NetWorkWrapper {
    static func request() {
        AManager.shared.networkProvider?.request()
    }
}

// Client

AManager.shared


LoggerWrapper.e(msg: "Hello")
NetWorkWrapper.request()

// dynamic replacment
AManager.shared.register(provider: CustomHttp())
NetWorkWrapper.request()

AManager.shared.register(provider: CustomLog())
LoggerWrapper.e(msg: "Hello~")


protocol State {
    func doThis()
    func doThat()
}

class Context: State {
    private var state: State
    
    init(state: State) {
        self.state = state
    }
    
    func doThis() {
        state.doThis()
    }
    
    func doThat() {
        state.doThat()
    }
    
    func changeState(state: State) {
        self.state = state
    }
}

class ConcreteStates: State {
    weak var context: Context?
    
    func doThis() {
        print("do this")
    }
    
    func doThat() {
        print("do that")
        cs.context?.changeState(state: OtherStates())
    }
}

class OtherStates: State {
    func doThis() {
        print("do another this")
    }
    
    func doThat() {
        print("do another!")
    }
}

let cs = ConcreteStates()
let context = Context(state: cs)
cs.context = context

context.doThis()
context.doThat()
context.doThis()
